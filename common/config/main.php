<?php
return [
	
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
   // require(__DIR__ . '/../../vendor/semantics3/semantics3php/lib/Semantics3.php'),
	'components' => [
		 'db' => [
            'class' => 'yiidbConnection',
            'dsn' => 'mysql:host=localhost;dbname=schae_yii',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
