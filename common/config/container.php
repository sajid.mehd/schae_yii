<?php
use yii\di\Container;

$container = new Container();
\Yii::$container->set('yii\grid\GridView', [
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover'
    ],
    'layout' => "{items}\n{summary}\n{pager}",
	'summary' => "<div class='summary'>Showing <i>{begin} - {end}</i> of <b>{totalCount}</b> items</div>",
	'summaryOptions' => ['class'=>'summary']
]);
