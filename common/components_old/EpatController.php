<?php

namespace app\components;

use Yii;
use yii\web\Controller;

//use app\models\hr\HrDefjobcategory;

class EpatController extends Controller {

    public $pageHeading = "Dashboard";
    public $pageCaption = " overview &amp; stats ";
    public $pageAddUrl = "";
    public $breadcrumbs = array();
    
    
    public $contentHeading = "";
    public $contentCaption = "";

    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
    }

    public static function pre_r($data = array()) {

        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    public function modelSave($model, $modelArr) {
        if ($modelArr != NULL) {
            $model->attributes = $modelArr;
            $model->save(false);
        }
        return;
    }

    /**
     * 
     * @param undefined $model
     * @param undefined $attribute
     * @param undefined $id
     * 
     */
    public function findModelByAttribute($model, $attribute, $id) {
        $mod = $model::find()->where($attribute . '=:psr', [':psr' => $id])->one();
        /* EpatController::pre_r($model);
          die(); */
        return ($mod != NULL) ? $mod : $model;
    }

    /**
     * e.g: Finds the HrDefjobcategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $modelname
     * @param integer $id
     * @return modelname the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($modelName, $id) {
        if (($model = $modelName::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function findModelByEmpsr($modelName, $id) {
        if (($model = $modelName::findOne(['empofficialsr=:empsr', ':empsr' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function findModelByEmpid($modelName, $id) {
        if (($model = $modelName::findOne(['empofficialid=:empid', ':empid' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public static function mysqldate($dateString) {
        if ($dateString !== null) {
            $parts = explode('/', $dateString);
            $Y = $parts[2];
            $m = $parts[1];
            $d = $parts[0];
            return ( $Y . '-' . $m . '-' . $d);
        } else {
            return null;
        }
    }

    public static function normaldate($dateString) {
        if ($dateString !== null) {
            $parts = explode('-', $dateString);
            $Y = $parts[0];
            $m = $parts[1];
            $d = substr($parts[2], 0, 2);
            return ($d . '/' . $m . '/' . $Y);
        } else {
            return null;
        }
    }

    public static function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d') {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
        while ($current <= $last) {
            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }
        return $dates;
    }

    public static function getDayFromDate($date) {
        $return = "";
        $date = strtotime($date);
        $day = date('l', $date);
        switch ($day) {
            case 'Monday':
                $return = '1';
                break;
            case 'Tuesday':
                $return = '2';
                break;
            case 'Wednesday':
                $return = '3';
                break;
            case 'Thursday':
                $return = '4';
                break;
            case 'Friday':
                $return = '5';
                break;
            case 'Saturday':
                $return = '6';
                break;
            case 'Sunday':
                $return = '7';
                break;
        }
        return $return;
    }

    public static function getDuty($type) {

        $return = 1;
        switch ($type) {
            case 'leave':
                $return = '0';
                break;
            case 'Off':
                $return = '0';
                break;
        }
        return $return;
    }

    public function current_department($sr = null) {
        $sr = ($sr == null) ? Yii::app()->user->id : $sr;
        $model = Tusers::model()->findByPk($sr);
        if ($model != null) {
            $personalModel = Tpersonalinfo::model()->findByPk($model->personalinfosr);
            $officialModel = $personalModel->official;
            return $officialModel->defdepartmentsr;
        }

        return NULL;
    }
    
    public static function getClassName($model){
        return \yii\helpers\StringHelper::basename(get_class($model));
    }


}
