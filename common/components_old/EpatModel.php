<?php

namespace common\components;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\components\EpatController;
use app\models\application\AppRunningBalance;

/**
 * Epatronus Custom Model
 * Common Function Will be performed inside thid model
 * every Model Should be extended from EpatModel
 */
class EpatModel extends ActiveRecord {

    /**
     * Get List Of Avaliable Table Details as well as for Dropdown List
     * @param type array() i.e., list of attributes or Table id
     * @param type $pk i.e., primary key of the table
     * @return type no. of records
     */
    public function beforeSave($insert = TRUE) {

        if ($this->isNewRecord) {
            if ($this->hasAttribute('let'))
                $this->let = new \yii\db\Expression('NOW()');
            if ($this->hasAttribute('addedbyuserid'))
                $this->addedbyuserid = 1; //Yii::app()->user->id;
            if ($this->hasAttribute('dateadded'))
                $this->dateadded = new \yii\db\Expression('NOW()');
            if ($this->hasAttribute('recordstatus'))
                $this->recordstatus = 1;
            return parent::beforeSave($insert);
        }
        else {
            if ($this->hasAttribute('let'))
                $this->let = new \yii\db\Expression('NOW()');
            return parent::beforeSave($insert);
        }
    }

    public function MaxSr() {
        $connection = Yii::$app->db;
        $tableName = $this->tableName();
        $Max = $connection->createCommand(
                        "SELECT MAX(id)+1 id FROM $tableName "
                )->queryOne();

        if ($Max == null)
            $Max = 1;
        else
            $Max = $Max['id'];

        return $Max;
    }

    public static function getTableDetail($tableName, $dropDownList = [], $pk = NULL) {
        $query = (new Query)
                ->select('*')
                ->from($tableName)
                ->where('recordstatus=:rec')
                ->addParams([':rec' => '1']);

        if (sizeof($dropDownList) > 0) {
            list($id, $value) = $dropDownList;
            $query->orderBy("$value ASC");
        }

        $queryType = "queryAll";
        if ($pk != null) {
            $query->andWhere('[[id]] = :id')
                    ->addParams([
                        ':id' => $pk,
            ]);
            $queryType = "queryOne";
        }

        //EpatController::pre_r($query);

        $command = $query->createCommand();
        $rows = $command->$queryType();

        if (sizeof($dropDownList) > 0) {
            $retArray = array();
            if ($rows != NULL) {
                foreach ($rows as $row) {
                    $key = $row[$id];
                    $retArray[$key] = $row[$value];
                }
            }
            $rows = $retArray;
        }

        return $rows;
    }

    public static function getRosterType($key = NULL) {
        $rosterArr = ['0' => 'Monthly', '1' => 'Weekly'];
        if ($key == NULL)
            return $rosterArr;
        else
            return $rosterArr[$key];
    }

    public static function getCountry($key = NULL) {

        $countryArr = ['0' => 'Pakistan', '1' => 'India', '2' => 'dubai'];
        if ($key == NULL)
            return $countryArr;
        else
            return $countryArr[$key];
    }

    public static function getCity($key = NULL) {
        $cityArr = ['0' => 'Peshawar', '1' => 'Islamabad', '2' => 'Lahore'];
        if ($key == NULL)
            return $cityArr;
        else
            return $cityArr[$key];
    }

    public static function getDegreeType($key = NULL) {
        $degreeArr = ['1' => 'Degree', '0' => 'Diploma'];
        if ($key == NULL)
            return $degreeArr;
        else
            return $degreeArr[$key];
    }

    /**
     * Finde record in specific model by pk
     * @param id $id pk of table
     * @param object $model	requested  model
     * @return object $model requested model
     * @access public
     */
    public static function findModel($id, $refModel) {
        $model = new $refModel;
        $model = $model->findOne($id);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public static function dayInMonth($month, $year, $step = '+1 day', $format = 'Y-m-d') {
        $days = [];
        $now = $year . '-' . $month . '-01';
        $month = date("m", strtotime($now));
        $year = date("Y", strtotime($now));
        $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
        $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));

        $thisTime = strtotime($first);
        $endTime = strtotime($last);
        while ($thisTime <= $endTime) {
            $days[] = date('Y-m-d', $thisTime) . "<br />";
            $thisTime = strtotime('+1 day', $thisTime); // increment for loop
        }
        return $days;
    }

    /**
     * Delete from DB
     * @param id $id pk of the record which should be delted
     * @param object $model	requested  model from which you want to delete datat
     * @access public
     */
    public static function deleteData($id, $model) {

        $refModel = get_class($model);
        if ($model != NULL)
            EpatModel::findModel($id, $refModel)->delete();
    }

    /**
     * update/Add data to db
     * @param array $data array of postedData
     * @param object $model object of model
     * @access public
     * 
     */
    public static function setData($data, $model) {
        //EpatController::pre_r($data);die();
        $model->load($data);
        if ($data != NULL)
            $model->save(false);
        return true;
    }

    /**
     * list of bloodgroup
     * @return array $bloodArr  all bloodgroupes
     * @access public
     * 
     */
    public static function getBloodGroup($key = NULL) {
        $bloodArr = ['A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'O+' => 'O+', 'O-' => 'O-', 'Ab+' => 'Ab+', 'Ab-' => 'Ab-'];
        if ($key == NULL)
            return $bloodArr;
        else
            return $bloodArr[$key];
    }

    /**
     * list of EyeColor
     * @return  $eyeColor  all Eyecolors
     * @access public
     * 
     */
    public static function getEyeColor($key = NULL) {
        $eyeColor = ['Brown' => 'Brown', 'Black' => 'Black', 'Green' => 'Green', 'Hazel' => 'Hazel', 'Blue+' => 'Blue', 'Silver' => 'Silver', 'Amber' => 'Amber'];
        if ($key == NULL)
            return $eyeColor;
        else
            return $eyeColor[$key];
    }

    /**
     * list of Complexion
     * @return $complArr  all Complexion
     * @access public
     * 
     */
    public static function getComplexion($key = NULL) {

        $complArr = ['Dark' => 'Dark', 'Medium' => 'Medium', 'Olive' => 'Olive', 'Brown' => 'Brown', 'Fair' => 'Fair'];
        if ($key == NULL)
            return $complArr;
        else
            return $complArr[$key];
    }

    public static function getStockOut($key = NULL) {

        $stockOutArr = ['0' => 'On Batch #', '1' => 'On Expiry Date'];
        if ($key == NULL)
            return $stockOutArr;
        else
            return $stockOutArr[$key];
    }

    public static function MaxDocAll($table = 'doc_all', $type = NULL) {
        $connection = Yii::$app->db;
        //$tableName = $this->tableName();
        $tableName = $table;
        $sql = <<<EOD
			SELECT MAX(docid)+1 as doc FROM {$tableName}
			WHERE doc_type = {$type} AND recordstatus = 1
EOD;
        $Max = $connection->createCommand($sql)->queryOne();

        if (sizeof($Max['doc']) < 1)
            $Max = 1;
        else
            $Max = $Max['doc'];


        return $Max;
    }

    /**
     * Find AppRunning balance whether exists or not
     * @param integer $productid
     * @param integer $productdetailid productdetailid
     * @param integer $batchno batchnumber
     * @param integer $doctype doctype
     * 
     */
    public static function checkAppRunningBalance($productid = NULL, $productdetailid = NULL, $batchno = NULL, $doctype = NULL) {
        $stockBalance = AppRunningBalance::find()
                        ->where('productid=:pid	and productdetailid=:detid and batch_num=:batchno and doc_type=:type and recordstatus=:status', [
                            ':pid' => $productid,
                            ':detid' => $productdetailid,
                            ':batchno' => $batchno,
                            ':type' => $doctype,
                            ':status' => 1,
                                ]
                        )->count();
        return $stockBalance;
    }
    
    
    public  static function setReferenceNo($docType, $docid){
        switch($docType){
            
            case 1: // Purchase Order
                $refno = "PO/".sprintf("%04d",$docid);
                break;
            
            case 2: // Purcahse Invoice
                $refno = "PI/".sprintf("%04d",$docid);
                break;
				
			case 3: // Quotation
				$refno = "QU/".sprintf("%04d",$docid);
                break;
            
			case 4: //Sales Invoice
				$refno = "SI/".sprintf("%04d",$docid);
                break;
				
			case 5:
				$refno = "Prt/".sprintf("%04d",$docid);
                break;
			
            default:
                $refno = "Reference#".sprintf("%04d",$docid);
            
        }
        return $refno;
    }
    
    
}
