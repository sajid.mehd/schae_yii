<?php

namespace common\components;

use Yii;
use yii\web\Controller;
use backend\models\UploadForm;
use backend\models\AppAttachments;

//use app\models\hr\HrDefjobcategory;

class EpatController extends Controller {

    public $pageHeading = "Dashboard";
    public $pageCaption = " overview &amp; stats ";
    public $pageAddUrl = "";
    public $headerButton = "";
    public $breadcrumbs = array();
    public $contentHeading = "";
    public $contentCaption = "";

    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
    }

    public static function pre_r($data = array()) {

        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    public function modelSave($model, $modelArr) {
        if ($modelArr != NULL) {
            $model->attributes = $modelArr;
//             	pre($_REQUEST);
//              pre($model->attributes);
//              die(); 
//            pre($model);
//            die();
            $model->save(false);
        }
        return;
    }

    /**
     * 
     * @param undefined $model
     * @param undefined $attribute
     * @param undefined $id
     * 
     */
    public function findModelByAttribute($model, $attribute, $id) {
        $mod = $model::find()->where($attribute . '=:psr', [':psr' => $id])->one();
        /* EpatController::pre_r($model);
          die(); */
        return ($mod != NULL) ? $mod : $model;
    }

    /**
     * e.g: Finds the HrDefjobcategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $modelname
     * @param integer $id
     * @return modelname the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected static function findModel($modelName, $id) {
        if (($model = $modelName::findOne($id)) !== null) {
            return $model;
        } else {
            //$model = $modelName;
            return $modelName;
            // throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function findModelByEmpsr($modelName, $id) {
        if (($model = $modelName::findOne(['empofficialsr=:empsr', ':empsr' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function findModelByEmpid($modelName, $id) {
        if (($model = $modelName::findOne(['empofficialid=:empid', ':empid' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public static function mysqldate($dateString) {
        if ($dateString !== null) {
            $parts = explode('/', $dateString);
            $Y = $parts[2];
            $m = $parts[1];
            $d = $parts[0];
            return ( $Y . '-' . $m . '-' . $d);
        } else {
            return null;
        }
    }

    public static function normaldate($dateString) {
        if ($dateString !== null) {
            $parts = explode('-', $dateString);
            $Y = $parts[0];
            $m = $parts[1];
            $d = substr($parts[2], 0, 2);
            return ($d . '/' . $m . '/' . $Y);
        } else {
            return null;
        }
    }

    public static function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d') {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
        while ($current <= $last) {
            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }
        return $dates;
    }

    public static function getDayFromDate($date) {
        $return = "";
        $date = strtotime($date);
        $day = date('l', $date);
        switch ($day) {
            case 'Monday':
                $return = '1';
                break;
            case 'Tuesday':
                $return = '2';
                break;
            case 'Wednesday':
                $return = '3';
                break;
            case 'Thursday':
                $return = '4';
                break;
            case 'Friday':
                $return = '5';
                break;
            case 'Saturday':
                $return = '6';
                break;
            case 'Sunday':
                $return = '7';
                break;
        }
        return $return;
    }

    public static function getDuty($type) {

        $return = 1;
        switch ($type) {
            case 'leave':
                $return = '0';
                break;
            case 'Off':
                $return = '0';
                break;
        }
        return $return;
    }

    public function current_department($sr = null) {
        $sr = ($sr == null) ? Yii::app()->user->id : $sr;
        $model = Tusers::model()->findByPk($sr);
        if ($model != null) {
            $personalModel = Tpersonalinfo::model()->findByPk($model->personalinfosr);
            $officialModel = $personalModel->official;
            return $officialModel->defdepartmentsr;
        }

        return NULL;
    }

    public static function getClassName($model) {
        return \yii\helpers\StringHelper::basename(get_class($model));
    }

    public static function currencyFormat($amount = 0) {
        return number_format($amount, 2, '.', ',');
    }

    public static function addAttachments($attachModel, $model, $folder = null, $chk = 1) {
        // pre($attachModel);echo "<hr />";
        // echo $folder;die();

        // dd($folder);
        if ($attachModel->validate()) {
            $rootPath = Yii::$app->getBasePath() . '/../frontend/web/';
           
            $filePath = 'attachments/' . $folder . '/';
            // echo $filePath;die();
            foreach ($attachModel->imageFiles as $key => $file) {
                $random_num = sprintf('%04d', rand(4, 999));
                $filename = $file->name;

                if (file_exists($rootPath . $filePath . $filename)) {
                    $filename = $random_num . $file->name;
                }
                $file->saveAs($rootPath . $filePath . $filename, true);

                $model->setIsNewRecord(TRUE);
                $model->id = NULL;

                $model->file_name = $attachModel['imageTitle'][$key];
                $model->file_path = $filePath . $filename;

                $model->save(false);
            }
            return true;
        } else {
            return false;
        }
    }

    public static function getAttachments($docID, $hint = NULL, $type) {
        $attachArray = [];
        if ($hint == 'projects') {

            if ($type == 1) {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec ', [
                                    ':id' => $docID,
                                    ':img' => 1,
                                    ':rec' => 1
                                ])->asArray()->all();
            } else {


                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec ', [
                                    ':id' => $docID,
                                    ':img' => 0,
                                    ':rec' => 1
                                ])->asArray()->all();
            }
        } elseif ($hint == 'cv') {

            if ($type == 1) {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 1,
                                    ':rec' => 1
                                ])->asArray()->all();
            } else {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 0,
                                    ':rec' => 1
                                ])->asArray()->all();
            }
        }
        elseif ($hint == 'applications') {

            if ($type == 1) {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 1,
                                    ':rec' => 1
                                ])->asArray()->all();
            } else {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 0,
                                    ':rec' => 1
                                ])->asArray()->all();
            }
        }

        elseif ($hint == 'solarsystems') {

            if ($type == 1) {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 1,
                                    ':rec' => 1
                                ])->asArray()->all();
            } else {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 0,
                                    ':rec' => 1
                                ])->asArray()->all();
            }
        } elseif ($hint == 'products') {

            if ($type == 1) {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 1,
                                    ':rec' => 1
                                ])->asArray()->all();
            } else {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 0,
                                    ':rec' => 1
                                ])->asArray()->all();
            }
        }
        elseif ($hint == 'news_events') {

            if ($type == 1) {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 1,
                                    ':rec' => 1
                                ])->asArray()->all();
            } else {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 0,
                                    ':rec' => 1
                                ])->asArray()->all();
            }
        } elseif ($hint == 'febrications') {

            if ($type == 1) {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 1,
                                    ':rec' => 1
                                ])->asArray()->all();
            } else {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 0,
                                    ':rec' => 1
                                ])->asArray()->all();
            }
        } elseif ($hint == 'clients') {

            if ($type == 1) {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 1,
                                    ':rec' => 1
                                ])->asArray()->all();
            } else {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 0,
                                    ':rec' => 1
                                ])->asArray()->all();
            }
        }
        elseif ($hint == 'downloads') {

            if ($type == 1) {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 1,
                                    ':rec' => 1
                                ])->asArray()->all();
            } else {

                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 0,
                                    ':rec' => 1
                                ])->asArray()->all();
            }
        }

        elseif ($hint == 'distributor') {

            if ($type == 1) {
                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 1,
                                    ':rec' => 1
                                ])->asArray()->all();
            } else {

                $attachArray = AppAttachments::find()
                                ->where('resourceid=:id AND is_image=:img AND recordstatus=:rec', [
                                    ':id' => $docID,
                                    ':img' => 0,
                                    ':rec' => 1
                                ])->asArray()->all();
            }
        }

        if (sizeof($attachArray) > 0) {
            return $attachArray;
        }
        return [];
    }

}
