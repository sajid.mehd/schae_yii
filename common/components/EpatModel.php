<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use common\components\EpatController;

use app\models\application\AppRunningBalance;
use app\models\account\AcChartOfAccounts;
use app\models\account\AcDynamicCoa;
use app\models\customer\CsCustomers;
use app\models\hr\HrPersonalinfo;
use mPDF;
use app\models\account\AcDynamicWorkflow;

/**
 * Epatronus Custom Model
 * Common Function Will be performed inside thid model
 * every Model Should be extended from EpatModel
 */
class EpatModel extends ActiveRecord {

    const SUPPLIER_TYPE = 1;
    const CUSTOMER_TYPE = 2;
    const BANK_TYPE = 3;
    const AGAINST_DOCUMENT = 1;
    const AGAINST_VOUCHER = 2;
    const AGANIST_PRODUCT = 3;
    const AGAINST_CUSTOMER = 4;
    const AGAINST_EMPLOYEE = 5;
    const AGAINST_ACADEMIC = 6;
    const AGAINST_CERTIFICATE = 7;
    const AGAINST_WORK_EXP = 8;
    const AGAINST_INVOICE = 9;
    const AGAINST_EMERG = 10;
    const AGAINST_SIGNATURE = 11;
    const FOLDER_DOCUMENT = 'documents';
    const FOLDER_VOUCHER = 'vouchers';
    const FOLDER_PRODUCT = 'products';
    const FOLDER_CUSTOMER = 'customers';
    const FOLDER_EMPLOYEE = 'employees';
    const FOLDER_ACADEMIC = 'academic';
    const FOLDER_CERTIFICATE = 'certificates';
    const FOLDER_WORK_EXP = 'workexp';
    const FOLDER_INVOICE = 'invoice';
    const FOLDER_EMERG = 'emergency';
    const FOLDER_SIGNATURE = 'signature';
    //--- Chart of Accounts ID
    const SUPPLIER_COA_ID = 15; // Sundry Creditors/Payable
    const CUSTOMER_COA_ID = 8; // Sundry Debitors/Receiveable
    //const BANK_COA_ID =  40; //Editable According to DB
    const CASH_IN_HAND = 28; // Editable According to DB
    //---- Vouchers IDs
    const VC_RECEIVED = 0;
    const VC_PAYMENT = 1;
    const VC_EXPENSE = 2;
    const VC_CASH_WD = 3;
    //--- Stock Status
    const STOCK_PENDING = 0;
    const STOCK_RECEIVED = 1;
    //---- WorkFlow
    const WF_FOR_PURCHASE = 2;
    const WF_FOR_SALES = 4;
    const WF_FOR_PURCHASE_RETURN = 5;
    const WF_FOR_SALES_RETURN = 6;
    const WF_FOR_EXPENSE = 7;
    const WF_FOR_CASH_WITHDRAW = 8;
    //---- Documents
    const DOC_PORDER = 1;
    const DOC_PINVOICE = 2;
    const DOC_QUOTATION = 3;
    const DOC_SALES = 4;
    const DOC_PURCHASE_RETURN = 5;
    const DOC_SALES_RETURN = 6;
    //---- PDF Views
    const VIEW_QUOTATION = 1;
    const VIEW_SALES = 2;
    const VIEW_SALES_RETURN = 3;
    const VIEW_RECEIVED_VOUCHER = 4;
    const VIEW_DELIVERY_CHALLAN = 5;
    const VIEW_PURCHASE_INVOICE = 1;
    const VIEW_PURCHASE_ORDER = 2;
    const VIEW_PURCHASE_RETURN = 3;
    const VIEW_PAYMENT_VOUCHER = 4;
    //---Opening balance
    const OC_AGAINST_COA = 1;
    const OB_AGAINST_PRODUCTS = 2;
    const ON_LEAVE = 'Leave';
    const DAY_WORKING = 'Regular';
    const DAY_OFF = 'Off';
    //----- Journal Against
    const JR_AGAINST_DOCUMENT = 1;
    const JR_AGAINST_FINANACE = 2;
    const JR_AGAINST_MANUAL = 3;
    const JR_AGAINST_OPENING = 4;
    const IS_DEBIT = 1;
    const IS_CREDIT = 0;
    //----- DOCUMENT TAX DETAILS
    const TAX_AGAINST_DOCUMENT = 1;
    const TAX_AGAINST_FINANCE = 2;
    const TAX_TYPE_GST = 1;
    const TAX_TYPE_WHT = 2;
    const WHT_NA = 0;
    const WHT_FILER = 1;
    const WHT_NON_FILER = 2;
    //-------- DYNAMIC FLOW AGAINST ACCOUNTS i.e., Customers, Supplier etc
    const FLOW_AGAINST_CUSTOMER = 1;
    const FLOW_AGAINST_SUPPLIER = 2;
    const FLOW_AGAINST_BANK = 3;
    const FLOW_AGAINST_PRODUCT_CATEGORY = 4;
    const FLOW_AGAINST_PRODUCT = 5;
    const FLOW_AGAINST_DIRECT_EXPENSE = 6;
    const FLOW_AGAINST_INDIRECT_EXPENSE = 7;
    const OPENING_BALANCE = 9;
    //---- Accounts Hard Coded Editable according to COA_ID
    const ON_CREDIT = 0;
    const ON_CASH = 1;
    const ON_BANK = 2;
    const AC_INVENTORY_ASSETS = 7;
    const AC_SALES_INCOME = 22;
    const AC_SALES_RETURN = 23;
    const AC_PURCHASE_DISCOUNT = 38;
    const AC_FREIGHT_IN = 41;
    const AC_OB_INCOME = 46; // For opening Balances in Assets
    const AC_OB_EXPENSE = 56; // For opening Balances in Liabilities
    const AC_CASH_IN_HAND = 9;
    const AC_SALES_TAX = 50;
    const AC_WHT = 51;
    const AC_ADDITIONAL_TAX = 52;
    const AC_COGS = 30;
    const AC_PAYABLE = 12;
    const AC_RECEIVABLE = 5;

    /**
     * Get List Of Avaliable Table Details as well as for Dropdown List
     * @param type array() i.e., list of attributes or Table id
     * @param type $pk i.e., primary key of the table
     * @return type no. of records
     */
    public function beforeSave($insert = TRUE) {

        if ($this->isNewRecord) {
            if ($this->hasAttribute('let'))
                $this->let = new \yii\db\Expression('NOW()');
            if ($this->hasAttribute('addedbyuserid'))
                $this->addedbyuserid = 1; //Yii::app()->user->id;
            if ($this->hasAttribute('dateadded'))
                $this->dateadded = new \yii\db\Expression('NOW()');
            if ($this->hasAttribute('recordstatus'))
                $this->recordstatus = 1;
            return parent::beforeSave($insert);
        }
        else {
            if ($this->hasAttribute('let'))
                $this->let = new \yii\db\Expression('NOW()');
            return parent::beforeSave($insert);
        }
    }

    public static function MaxSr($model) {
        $connection = Yii::$app->db;
        $tableName = $model->tableName();
        $Max = $connection->createCommand(
                        "SELECT MAX(id)+1 as id FROM $tableName "
                )->queryOne();

        if ($Max['id'] == NULL)
            $Max = 1;
        else
            $Max = $Max['id'];

        return $Max;
    }

    public static function MaxID($model, $attrib = 'id', $condition = array()) {
        $connection = Yii::$app->db;
        $tableName = $model->tableName();

        /* $command = $connection->createCommand(
          "SELECT MAX({$attrib})+1 as id FROM $tableName WHERE recordstatus=1"
          ); */

        $query = (new Query)
                ->select("MAX({$attrib})+1 as id")
                ->from($tableName)
                ->where('recordstatus=:rec')
                ->addParams([':rec' => '1']);

        if (sizeof($condition) > 0) {
            $query = $query->andWhere($condition);
        }

        $Max = $query->createCommand()->queryOne();

        if ($Max['id'] == NULL)
            $Max = 1;
        else
            $Max = $Max['id'];

        return $Max;
    }

    /*
      public static function MaxID($model, $attrib = 'id') {

      $connection = Yii::$app->db;
      $tableName = $model->tableName();
      $Max = $connection->createCommand(
      "SELECT MAX({$attrib})+1 as id FROM $tableName WHERE recordstatus=1"
      )->queryOne();

      if ($Max['id'] == NULL)
      $Max = 1;
      else
      $Max = $Max['id'];

      return $Max;
      } */

    public static function getTableDetail($tableName, $dropDownList = [], $pk = NULL) {
        $query = (new Query)
                ->select('*')
                ->from($tableName)
                ->where('recordstatus=:rec')
                ->addParams([':rec' => '1']);

        if (sizeof($dropDownList) > 0) {
            list($id, $value) = $dropDownList;
            $query->orderBy("$value ASC");
        }

        $queryType = "queryAll";
        if ($pk != null) {
            $query->andWhere('[[id]] = :id')
                    ->addParams([
                        ':id' => $pk,
            ]);
            $queryType = "queryOne";
        }

        //EpatController::pre_r($query);

        $command = $query->createCommand();
        $rows = $command->$queryType();

        if (sizeof($dropDownList) > 0) {
            $retArray = array();
            if ($rows != NULL) {
                foreach ($rows as $row) {
                    $key = $row[$id];
                    $retArray[$key] = $row[$value];
                }
            }
            $rows = $retArray;
        }

        return $rows;
    }

    public static function getRosterType($key = NULL) {
        $rosterArr = ['0' => 'Monthly', '1' => 'Weekly'];
        if ($key == NULL)
            return $rosterArr;
        else
            return $rosterArr[$key];
    }

    public static function getCountry($key = NULL) {

        $countryArr = ['0' => 'Pakistan', '1' => 'India', '2' => 'dubai'];
        if ($key == NULL)
            return $countryArr;
        else
            return $countryArr[$key];
    }

    public static function getCity($key = NULL) {
        $cityArr = ['0' => 'Peshawar', '1' => 'Islamabad', '2' => 'Lahore'];
        if ($key == NULL)
            return $cityArr;
        else
            return $cityArr[$key];
    }

    public static function getDegreeType($key = NULL) {
        $degreeArr = ['1' => 'Degree', '0' => 'Diploma'];
        if ($key == NULL)
            return $degreeArr;
        else
            return $degreeArr[$key];
    }

    /**
     * Finde record in specific model by pk
     * @param id $id pk of table
     * @param object $model	requested  model
     * @return object $model requested model
     * @access public
     */
    public static function findModel($id, $refModel) {
        $model = new $refModel;
        $model = $model->findOne($id);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public static function dayInMonth($month, $year, $step = '+1 day', $format = 'Y-m-d') {
        $days = [];
        $now = $year . '-' . $month . '-01';
        $month = date("m", strtotime($now));
        $year = date("Y", strtotime($now));
        $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
        $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));

        $thisTime = strtotime($first);
        $endTime = strtotime($last);
        while ($thisTime <= $endTime) {
            $days[] = date('Y-m-d', $thisTime) . "<br />";
            $thisTime = strtotime('+1 day', $thisTime); // increment for loop
        }
        return $days;
    }

    /**
     * Delete from DB
     * @param id $id pk of the record which should be delted
     * @param object $model	requested  model from which you want to delete datat
     * @access public
     */
    public static function deleteData($id, $model) {

        $refModel = get_class($model);
        if ($model != NULL)
            EpatModel::findModel($id, $refModel)->delete();
    }

    /**
     * update/Add data to db
     * @param array $data array of postedData
     * @param object $model object of model
     * @access public
     * 
     */
    public static function setData($requestData, $model) {
        
        pre($requestData);
        pre($model);
        die();

        if ($requestData == NULL)
            $model->load($requestData);

        $model->save(false);

        return $model;
    }
    
    /**
     * Set/Get Dynamic Account Product Categories, Products, Customers, Suppliers etc
     * @param type $model i.e, cs_customer model, product model etc
     * @param type $against
     * @param type $parentid
     * @return int
     */

    public static function checkDynamicAccount($model, $against, $parentid = null) {


        if (isset($model->id) && $model->id != NULL && $against != NULL) {


            //Get DynamicFlow
            $dynamicFlow = AcDynamicWorkflow::find()
                    ->where('against=:ag AND recordstatus=:rec')
                    ->params([':ag' => $against, ':rec' => 1])
                    ->all();

            if (sizeof($dynamicFlow) > 0) {

                $refTable = $model::tableName();
                $dynamicID = EpatModel::MaxID(new AcDynamicCoa(), 'dynamic_id', ['against' => $against]);

                foreach ($dynamicFlow as $key => $dynamic) {

                    $parentid = $dynamic->coa_id;

                    $exist = AcDynamicCoa::find()
                                    ->where([
                                        'against' => $against,
                                        'ref_id' => $model->id,
                                        'parent_coa_id' => $parentid,
                                        'recordstatus' => 1
                                    ])->limit(1)->one();


                    //pre($exist);
                    //pre($model->attributes);
                    //die();
                    //continue; 

                    if (sizeof($exist) > 0) {
                        return 0;
                        //pre($exist->attributes);
                    } else {
                        $returnModel = array();
                        switch ($against) {

                            case EpatModel::FLOW_AGAINST_CUSTOMER:
                                $custModel = new AcChartOfAccounts();
                                $custModel->title = $model->name;
                                $custModel->display_title = $model->name;
                                $custModel->parent_coa_id = $parentid;
                                $accountModel = AcChartOfAccounts::setAccounts($custModel, $_REQUEST);
                                break;

                            case EpatModel::FLOW_AGAINST_SUPPLIER:
                                $supModel = new AcChartOfAccounts();
                                $supModel->title = $model->name;
                                $supModel->display_title = $model->name;
                                $supModel->parent_coa_id = $parentid;
                                $accountModel = AcChartOfAccounts::setAccounts($supModel, $_REQUEST);
                                break;

                            case EpatModel::FLOW_AGAINST_BANK:
                                $words = explode(" ", $model->bank_name);
                                $title = "";
                                foreach ($words as $w) {
                                    $title .= $w[0];
                                }
                                $supModel = new AcChartOfAccounts();
                                $supModel->title = $title . ' - ' . $model->account_title;
                                $supModel->display_title = $title . ' - ' . $model->account_title;
                                $supModel->parent_coa_id = $parentid;
                                $accountModel = AcChartOfAccounts::setAccounts($supModel, $_REQUEST);
                                break;

                            case EpatModel::FLOW_AGAINST_PRODUCT_CATEGORY:
                                $supModel = new AcChartOfAccounts();
                                $supModel->title = $model->categorytitle;
                                $supModel->display_title = $model->categorytitle;
                                $supModel->parent_coa_id = $parentid;
                                $accountModel = AcChartOfAccounts::setAccounts($supModel, $_REQUEST);
                                break;

                            default:
                                setError("Check The Dynamic Account Entries");
                                return 0;
                        }

                        $dynamicModel = new AcDynamicCoa();
                        $dynamicModel->dynamic_id = $dynamicID;
                        $dynamicModel->ref_table_name = $refTable;
                        $dynamicModel->against = $against;
                        $dynamicModel->ref_id = $model->id;
                        $dynamicModel->parent_coa_id = $parentid;
                        $dynamicModel->coa_id = isset($accountModel) ? $accountModel->id : NULL;
                        $dynamicModel->save(false);

                        //----- Insert only once
                        if ($key == 0) {
                            $model->dynamic_coa_id = $dynamicID;
                            $model->save(false);
                        }
                    }//--- END of Else-if
                } //--- END of $dynamicFlow foreach
                
            } else if ($against == EpatModel::FLOW_AGAINST_PRODUCT) {
                //pre('Products');
                $refTable = $model::tableName();
                $dynamicID = EpatModel::MaxID(new AcDynamicCoa(), 'dynamic_id', ['against' => $against]);
                $categories = AcDynamicCoa::find()
                                ->where([
                                    'against' => EpatModel::FLOW_AGAINST_PRODUCT_CATEGORY,
                                    'ref_id' => $model->category_id,
                                    'recordstatus' => 1
                                ])->all();

                if (sizeof($categories) > 0) {

                    foreach ($categories as $key => $category) {

                        $parentid = $category->coa_id;
                        $exist = AcDynamicCoa::find()
                                        ->where([
                                            'against' => $against,
                                            'ref_id' => $model->id,
                                            'parent_coa_id' => $parentid,
                                            'recordstatus' => 1
                                        ])->limit(1)->one();

                        if ($exist) {
                            continue;
                        } else {
                            //pre($category->attributes);

                            switch ($against) {

                                case EpatModel::FLOW_AGAINST_PRODUCT:
                                    $prodModel = new AcChartOfAccounts();
                                    $prodModel->title = $model->title;
                                    $prodModel->display_title = $model->title;
                                    $prodModel->parent_coa_id = $parentid;
                                    $accountModel = AcChartOfAccounts::setAccounts($prodModel, $_REQUEST);
                                    break;

                                default:
                                    setError('Check Dynamic Account Entry');
                                    return 0;
                            }
                        }

                        $dynamicModel = new AcDynamicCoa();
                        $dynamicModel->dynamic_id = $dynamicID;
                        $dynamicModel->ref_table_name = $refTable;
                        $dynamicModel->against = $against;
                        $dynamicModel->ref_id = $model->id;
                        $dynamicModel->parent_coa_id = $parentid;
                        $dynamicModel->coa_id = isset($accountModel) ? $accountModel->id : NULL;
                        $dynamicModel->save(false);

                        //----- Insert only once
                        if ($key == 0) {
                            $model->dynamic_coa_id = $dynamicID;
                            $model->save(false);
                        }
                    }//---- END of foreach statement
                }//--- END of Products
            }
        }
        return 0;
    }

    /**
     * 
     * @param type $refid i.e., Model in which we get CsCustomer id, Supplier id, Bank id
     * @param type $against i.e, Supplier / Customer / Bank
     * @param type $parentcoa i.e, Parent Chart of Account Sundry Reciveable, Payable
     * @return int
     */
    public static function checkDynamicAccountOld($model, $against, $parentid) {

        if (isset($model->id) && $model->id != NULL && $against != NULL) {
            $exist = AcDynamicCoa::find()
                            ->where([
                                'coa_against' => $against,
                                'against_refid' => $model->id,
                                'parent_coa_id' => $parentid,
                                'recordstatus' => 1
                            ])->limit(1)->one();

            if (sizeof($exist) > 0) {
                //pre($exist->attributes);
            } else {
                $returnModel = array();
                switch ($against) {
                    case EpatModel::SUPPLIER_TYPE:
                        $supModel = new AcChartOfAccounts();
                        $supModel->title = $model->name;
                        $supModel->display_title = $model->name;
                        $supModel->parent_coa_id = $parentid;
                        $accountModel = AcChartOfAccounts::setAccounts($supModel, $_REQUEST);
                        break;

                    case EpatModel::CUSTOMER_TYPE:
                        $supModel = new AcChartOfAccounts();
                        $supModel->title = $model->name;
                        $supModel->display_title = $model->name;
                        $supModel->parent_coa_id = $parentid;
                        $accountModel = AcChartOfAccounts::setAccounts($supModel, $_REQUEST);
                        break;

                    default:
                        setError("Check The Dynamic Account Entries");
                        return 0;
                }

                $dynamicModel = new AcDynamicCoa();
                $dynamicModel->coa_against = $against;
                $dynamicModel->against_refid = $model->id;
                $dynamicModel->level4_coa_id = isset($accountModel) ? $accountModel->id : NULL;
                $dynamicModel->parent_coa_id = $parentid;
                $dynamicModel->save(false);
            }
        }
        return 0;
    }

    /**
     * list of bloodgroup
     * @return array $bloodArr  all bloodgroupes
     * @access public
     * 
     */
    public static function getBloodGroup($key = NULL) {
        $bloodArr = ['A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'O+' => 'O+', 'O-' => 'O-', 'Ab+' => 'Ab+', 'Ab-' => 'Ab-'];
        if ($key == NULL)
            return $bloodArr;
        else
            return $bloodArr[$key];
    }

    /**
     * list of EyeColor
     * @return  $eyeColor  all Eyecolors
     * @access public
     * 
     */
    public static function getEyeColor($key = NULL) {
        $eyeColor = ['Brown' => 'Brown', 'Black' => 'Black', 'Green' => 'Green', 'Hazel' => 'Hazel', 'Blue+' => 'Blue', 'Silver' => 'Silver', 'Amber' => 'Amber'];
        if ($key == NULL)
            return $eyeColor;
        else
            return $eyeColor[$key];
    }

    /**
     * list of Complexion
     * @return $complArr  all Complexion
     * @access public
     * 
     */
    public static function getComplexion($key = NULL) {

        $complArr = ['Dark' => 'Dark', 'Medium' => 'Medium', 'Olive' => 'Olive', 'Brown' => 'Brown', 'Fair' => 'Fair'];
        if ($key == NULL)
            return $complArr;
        else
            return $complArr[$key];
    }

    public static function getStockOut($key = NULL) {

        $stockOutArr = ['0' => 'On Batch #', '1' => 'On Expiry Date'];
        if ($key == NULL)
            return $stockOutArr;
        else
            return $stockOutArr[$key];
    }

    public static function MaxDocAll($table = 'doc_all', $type = NULL) {
        $connection = Yii::$app->db;
        //$tableName = $this->tableName();
        $tableName = $table;
        $sql = <<<EOD
			SELECT MAX(docid)+1 as doc FROM {$tableName}
			WHERE doc_type = {$type} AND recordstatus = 1
EOD;
        $Max = $connection->createCommand($sql)->queryOne();

        if (sizeof($Max['doc']) < 1)
            $Max = 1;
        else
            $Max = $Max['doc'];

        return $Max;
    }

    public static function QuotationId($table = 'doc_all', $type = NULL, $q_id = NULL) {
        //$tableName = $this->tableName();
        $tableName = $table;
        
        $sql = <<<EOD
			SELECT docid as doc FROM {$tableName}
                        WHERE id="{$q_id}" AND doc_type ="{$type}" AND recordstatus = 1
EOD;
                        
        
        $Max = Yii::$app->db->createCommand($sql)->queryOne();

        if (sizeof($Max['doc']) < 1)
            $Max = 1;
        else
            $Max = $Max['doc'];

        return $Max;
    }

    public static function MaxDocVersion($table = 'doc_all', $docid, $type = NULL) {
        $connection = Yii::$app->db;
        //$tableName = $this->tableName();
        $tableName = $table;
        $sql = <<<EOD
			SELECT MAX(version)+1 as doc FROM {$tableName}
			WHERE doc_type = {$type} AND docid={$docid} AND recordstatus = 1
EOD;
        $Max = $connection->createCommand($sql)->queryOne();

        if (sizeof($Max['doc']) < 1)
            $Max = 1;
        else
            $Max = $Max['doc'];


        return intval($Max);
    }

    public static function MaxDocDss($table = 'dss_doc', $type = NULL) {
        $connection = Yii::$app->db;
        //$tableName = $this->tableName();
        $tableName = $table;
        $sql = <<<EOD
			SELECT MAX(id)+1 as doc FROM {$tableName}
			WHERE recordstatus = 1
EOD;
        $Max = $connection->createCommand($sql)->queryOne();

        if (sizeof($Max['doc']) < 1)
            $Max = 1;
        else
            $Max = $Max['doc'];
        return $Max;
    }

    public static function MaxDocVoucher($table = 'doc_voucher', $type = 0) {
        $connection = Yii::$app->db;
        //$tableName = $this->tableName();
        $tableName = $table;
        $sql = <<<EOD
			SELECT MAX(voucher_id)+1 as doc FROM {$tableName}
			WHERE doc_type = {$type} AND recordstatus = 1
EOD;
        $Max = $connection->createCommand($sql)->queryOne();

        if (sizeof($Max['doc']) < 1)
            $Max = 1;
        else
            $Max = $Max['doc'];


        return $Max;
    }

    //---- Find the Max Chart of Accounts Level
    public static function MaxCOA($pid = null, $lvl = null, $frm = 'model') {
        if ($lvl > 1)
            $inc = 0.1;
        else
            $inc = 1;

        $tableName = ($frm == 'model') ? 'ac_chart_of_accounts' : $this->tableName();

        $query = (new Query)
                ->select("count(account_code)+1 account_code")
                ->from($tableName)
                ->where('parent_coa_id=:pid')
                ->addParams([':pid' => $pid]);

        $Max = $query->createCommand()->queryScalar();
        /* $Max = Yii::$app->db->createCommand()
          ->select("count(account_code)+1 account_code")
          ->from($tableName)
          ->where("parent_coa_id=$pid")
          ->queryScalar(); */
        //EpatController::pre_r($Max);

        if ($Max == NULL)
            $Max = 1;

        return $Max;
    }

    //-------- END of Account Type and its LEVEL
    //Get List of Chart Accounts with Group
    public static function getAccountList($lvl = NULL, $caption = null, $addExpenses = false) {

        if ($lvl != null) {
            $condition = 'recordstatus =:rec AND level<:level';
            $params = array(':rec' => 1, ':level' => $lvl);
        } else {
            $condition = 'recordstatus =:rec';
            $params = array(':rec' => 1);
        }

        if ($caption != null) {
            $condition = 'recordstatus =:rec AND level=:level AND title=:tit';
            $params = array(':rec' => 1, ':level' => $lvl, ':tit' => $caption);
        }

        $accModel = AcChartOfAccounts::find()->where($condition, $params)->all();

        if ($caption != null) {
            $list = "";
            foreach ($accModel as $val) {
                if ($addExpenses == false) {
                    $accModel = isset($val->childs) ? $val->childs : "";

                    if ($lvl < 3)
                        $list = ArrayHelper::map($accModel, 'id', 'title', 'parent.display_title');
                    else
                        $list = ArrayHelper::map($accModel, 'id', 'title');
                }
                else {
                    if ($val->childs != null) {
                        //EpatController::pre_r($val->childs);
                        foreach ($val->childs as $child) {
                            //EpatController::pre_r($child->childs);
                            isset($child->childs) ?
                                            $option = $child->childs : $option = "";

                            if (sizeof($child->childs) > 0)
                                $accModel = $child->childs;

                            $list[$child['display_title']] = ArrayHelper::map($accModel, 'id', 'title');
                            //, 'title','parent.display_title');
                        }
                    }
                }
            }
            //EpatController::pre_r($list);
            //die();
            return $list;
        }
        //EpatController::pre_r($accModel);
        //die('Account List');
        return ArrayHelper::map($accModel, 'id', 'title', 'parent.display_title');
    }

    /**
     * Find AppRunning balance whether exists or not
     * @param integer $productid
     * @param integer $productdetailid productdetailid
     * @param integer $batchno batchnumber
     * @param integer $doctype doctype
     * 
     */
    public static function checkAppRunningBalance($productid = NULL, $productdetailid = NULL, $batchno = NULL, $doctype = NULL) {
        $stockBalance = AppRunningBalance::find()
                        ->where('productid=:pid	and productdetailid=:detid and batch_num=:batchno and doc_type=:type and recordstatus=:status', [
                            ':pid' => $productid,
                            ':detid' => $productdetailid,
                            ':batchno' => $batchno,
                            ':type' => $doctype,
                            ':status' => 1,
                                ]
                        )->count();
        return $stockBalance;
    }

    /**
     * SET Reference# against Documents
     * @param type $docType 0-8
     * @param type $docid maximum docid
     * @return string referenceno
     */
    public static function setReferenceNo($docType, $docid, $version = false) {
        $refno = "";
        $sesiondate = date('y') . '~' . date("y", strtotime("+1 year"));
        switch ($docType) {

            case 1: // Purchase Order
                $refno = "ATSpo/" . $sesiondate . "/" . sprintf("%03d", $docid);
                break;

            case 2: // Purcahse Invoice
                $refno = "ATSpi/" . $sesiondate . "/" . sprintf("%03d", $docid);
                break;

            case EpatModel::DOC_QUOTATION:
                $refno = "ATSe/" . $sesiondate . "/" . sprintf("%03d", $docid);
                if ($version != false)
                    $refno = "ATSe/" . $sesiondate . "/" . sprintf("%03d", $docid) . "-V" . $version;
                break;

            case 4: //Sales Invoice
                $refno = "ATSi/" . $sesiondate . "/" . sprintf("%03d", $docid);
                break;

            case 5:
                $refno = "ATSprt/" . $sesiondate . "/" . sprintf("%03d", $docid);
                break;

            case 6:
                $refno = "ATSsrt/" . $sesiondate . "/" . sprintf("%03d", $docid);
                break;

            default:
                $refno = "Reference#" . sprintf("%03d", $docid);
        }
        return $refno;
    }

    /**
     * Set Voucher Reference No
     * @param type $docid maximum id
     * @param type $type 0: Received 1: Payment
     */
    public static function setVoucherReferenceNo($docid, $type) {
        $refno = "";
        switch ($type) {

            case EpatModel::VC_RECEIVED:
                $refno = "RV/" . sprintf("%04d", $docid);
                break;

            case EpatModel::VC_PAYMENT:
                $refno = "PV/" . sprintf("%04d", $docid);
                break;

            case EpatModel::VC_EXPENSE:
                $refno = "EXP/" . sprintf("%04d", $docid);
                break;

            case EpatModel::VC_CASH_WD:
                $refno = "CWD/" . sprintf("%04d", $docid);
                break;

            default:
                $refno = "Reference#" . sprintf("%04d", $docid);
        }
        return $refno;
    }

    /**
     * Set Dss Reference No
     * @param type $docid maximum id
     * @param type $type 0: Received 1: Payment
     */
    public static function setDssReferenceNo($docid, $type = 0) {
        $refno = "";
        switch ($type) {

            case 0: // generat dss
                $refno = "DSS/" . sprintf("%04d", $docid);
                break;

            case 1: // Purcahse Invoice
                $refno = "PV/" . sprintf("%04d", $docid);
                break;

            default:
                $refno = "Reference#" . sprintf("%04d", $docid);
        }
        return $refno;
    }

    public static function Employeeslist() {
        $personal = HrPersonalinfo::find('recordstatus=:rec', [':rec' => 1])->with('officialInfo')->all();
        $empList = array();
        if (sizeof($personal) != 0) {
            foreach ($personal as $key => $val) {
                $name = $val->firstName . " " . $val->middleName . " " . $val->lastName;
                $emp_id = $val->officialInfo->id;

                $empList[$emp_id] = $name;
            }
        }
        return $empList;
    }

    public static function m2h($mins) {
        $return = "";
        if ($mins < 0) {
            $min = Abs($mins);
        } else {
            $min = $mins;
        }
        if ($mins > 60) {

            $H = Floor($min / 60);
            $M = ($min - ($H * 60)) / 100;
            $hours = $H + $M;
            if ($mins < 0) {
                $hours = $hours * (-1);
            }

            $expl = explode(".", $hours);
            $H = $expl[0];
            if (empty($expl[1])) {
                $expl[1] = 00;
            }

            $M = $expl[1];
            if (strlen($M) < 2) {
                $M = $M . 0;
            }

            $hours = $H . ":" . $M;
            $return = $hours;
        } else {
            $return = $min;
        }
        return $return;
    }

    public function genPdf($html, $options = []) {
        $theme = Yii::$app->view->theme->baseUrl . '/images/header';
        $pdfHeader = Html::img($theme . '/' . Yii::$app->params['application']['documentlogo'], array(
                    'class' => 'company-logo',
                    'title' => Yii::$app->params['application']['name'],
                        //'style'=>"width: 210mm; height: 297mm; margin: 0;"
                        )
        );
        ini_set('memory_limit', '-1');
        date_default_timezone_set('Asia/Karachi');

        $mpdf = new mPDF;

        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->SetDisplayMode(isset($options['zoom']) ? $options['zoom'] : 100);
        $mpdf->SetCompression(isset($options['compression']) ? $options['zoom'] : 100);

        if (isset($options['print']) && $options['print'] == 1)
            $mpdf->SetJS('this.print();');


        $mpdf->pagenumPrefix = 'Page : ';
        $mpdf->nbpgPrefix = ' of ';
        $mpdf->nbpgSuffix = '';
        if (isset($options['headless']) && $options['headless'] == 1) {
            $mpdf->autoMarginPadding = 20;
            $mpdf->setHTMLFooter('<hr><div width="100%"><div style="width:30%; text-align:left; float:left; font-size:10px;">{PAGENO}{nbpg}</div><div style="width:40%; text-align:center; float:left; font-size:10px;">&nbsp;</div><div width="30%" style="text-align:right; float:left; font-size:10px;">{DATE j/m/Y h:i A}</div></div>');
        } else {
            $mpdf->SetHTMLHeader($pdfHeader);
            $footer = '<div width="100%"><div style="width:48%; text-align:left; float:left; font-size:9px;">{nbpg}</div>';
            $footer .= '<div width="48%" style="text-align:right; float:right; font-size:9px;">{DATE j/m/Y h:i A}</div></div>';
            $footer .= '<hr>';
            $footer .= '<div width="100%">';
            $footer .= '<div style="width:100%; text-align:center; float:left; font-size:9px;">Plot# 69, Street #3, Main Street #1, Industrial area I-10/3, Islamabad. Pakistan<br/>
Phone: 92-51-4440973, Fax: 92-51-442436, Email:info@atsengg.com, www.atsengg.com</div>';
            $footer .= '</div>';

            $mpdf->setHTMLFooter($footer);
        }
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }


    public static function changeStatus($model, $id) {
        $checked = "";
        // pre($model);die();
        
        if (isset($model['is_active']) && $model['is_active'] == 1) {
            $checked = 'checked="checked"';
        }

        $html = "<div class='col-xs-12 col-sm-3'><label>";
        $html .= "<input type='checkbox' $checked name='confirm' class='ace ace-switch ace-switch-5 btn-empty change_status_clx'  data-id=$id>";
        $html .= "<span class='lbl'></span></label></div>";

        return $html;
    }

}
