<?php

namespace app\components\multiupload;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadImage extends Model {

    public $imageFiles;
    public $imageTitle;

    public function rules() {
        return [
            [['imageTitle'], 'safe'],
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 10,'maxSize' => 1024 * 1024 * 100],
        ];
    }

    public function upload() {
        if ($this->validate()) {

            $rootPath = Yii::$app->getBasePath(). '/';
            $filePath = 'attachments' . '/';
            
            //pre($_REQUEST);
            //die();
            foreach ($this->imageFiles as $file) {
                $random_num = sprintf('%04d', rand(4, 999));
                $filename = $file->name;
                
                if (file_exists($rootPath . $filePath . $filename)) {
                    $filename = $random_num . $file->name;
                }

                $file->saveAs($rootPath . $filePath . $filename, true);
                //$file->saveAs('uploads/' . $file->baseName . '.' . $file->extension);
                /*
                $model->title = $filename;
                $model->path = $filePath;
                $model->save(false);
                */
            }
            return true;
        } else {
            return false;
        }
    }

}
