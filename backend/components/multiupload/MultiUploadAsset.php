<?php

namespace app\components\tree;

use yii\web\AssetBundle;

/**
 * Class TreeAsset
 * @package yii2mod\tree
 */
class MultiUploadAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/components/multiupload';

    /**
     * @var array
     */
    public $css = [
        'css/multiupload.css'
    ];

    /**
     * @var array
     */
    public $js = [
        'jss/custom-multiupload.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\AppAsset',
    ];
}