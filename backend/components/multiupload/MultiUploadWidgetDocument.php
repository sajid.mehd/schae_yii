<?php

namespace backend\components\multiupload;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;
use backend\models\UploadDocument;
use app\components\tree\MultiUploadAsset;
use backend\models\AppAttachments;
use common\components\EpatController;

class MultiUploadWidgetDocument extends Widget {

    public $form = '';
    public $options = []; // UploadForm Model
    public $list = ""; // List of Titles
    public $single = false;

    public function init() {
        parent::init();
        if (!isset($this->options['headerTitle']))
            $this->options['headerTitle'] = 'Document Attachments';

        if (!isset($this->options['headerColor']))
            $this->options['headerColor'] = 'widget-color-blue3';
    }

    public function run() {
        //return Html::encode($this->message);
        return $this->attachmentBody();
    }

    protected function attachmentBody() {

        $uploadForm = new UploadDocument();
        ?>

        <div class="widget-box <?= $this->options['headerColor'] ?>">
            <div class="widget-header">
                <h5 class="widget-title">
                    <?= $this->options['headerTitle'] ?>
                </h5>
                <div class="widget-toolbar padding-top-bottom padding-left-right">
                    <?php if ($this->single == false): ?>
                        <span class="btn btn-success btn-xs btn-flat addmore-attachment-doc no-padding" data-toggle="tooltip" data-placement="top" title="Add">
                            <i class="fa fa-fw fa-plus-circle"></i> 
                            <span class="padding-right">Add More</span>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="widget-body" style="min-height:12em">
                <div class="widget-main row padding-left-right padding-top-bottom">
                    <div class="col-xs-12">

                        <?php
                        $prevTRs = '';
                        if ($this->list != "" && sizeof($this->list) > 0) {
                            $fileTitle = $this->form->field($uploadForm, 'imageTitle[]')
                                            ->dropDownList($this->list, [
                                                'class' => 'form-control chosen-select-deselect',
                                                'prompt' => 'Select',
                                                'style' => 'width:100%'
                                            ])->label(false);
                        } else {
                            $fileTitle = $this->form->field($uploadForm, 'imageTitle[]')->textInput([
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter Title...'
                                    ])->label(false);
                        }

                        $browseFile = $this->form->field($uploadForm, 'imageFiles[]')->fileInput([
                                    'multiple' => true, 'accept' => 'image/*',
                                    'style' => 'overflow:hidden; width:100%'
                                ])->label(false);


                        if ($this->single == false):
                            $actionsList = '<i class="fa fa-fw fa-trash fa-lg red del-attach-row"></i>';
                        endif;
                        $newTR = <<<EOD
                                    <li class="item-orange clearfix ui-sortable-handle" style="position: relative; opacity: 1; left: 0px; top: 2px; z-index: auto;">
                                        <label class="inline col-xs-4 no-padding">
                                            <span class="lbl col-sm-12">{$fileTitle}</span>
                                        </label>
                                        <div class="col-xs-7">
                                            {$browseFile}
                                        </div>
                                           

                                        <div class="col-xs-1 text-right right margin-top" style="height: 29px; line-height: 28px;">
                                            {$actionsList}
                                        </div>
                                    </li>
EOD;


                        if (isset($this->options['attachmentArray']) && sizeof($this->options['attachmentArray']) > 0) {

                            $rootPath = Yii::$app->urlManager->baseUrl . '/../../frontend/';

                            foreach ($this->options['attachmentArray'] as $key => $attachment) {
                                
                                //pre($attachment['file_name']);die();
//                                pre($key);
//                                pre($this->options['attachmentArray']['file_name']);
//                                continue;
                                //pre($attachment['file_name']);die();
                                $filepath = ($attachment['file_path'] != NULL) ? $attachment['file_path'] : '';
                                $fileTitle = $this->form->field($uploadForm, 'imageTitle[]')->textInput([
                                            'class' => 'form-control',
                                            'readonly' => TRUE,
                                            'placeholder' => 'Enter Title...',
                                            'value' => (isset($attachment['file_name']) && $attachment['file_name'] != NULL) ? $attachment['file_name'] : ''
                                        ])->label(false);

                                $pieces = explode('/', (isset($attachment) && $attachment['file_path'] != NULL) ? $attachment['file_path'] : '');
                                $browseFileText = array_pop($pieces);
                                $path = $rootPath . $filepath;
                                $extension = $this->get_file_extension($browseFileText);
                                $browseFileImg = '';

                                if (in_array($extension, ['jpg', 'png', 'gif', 'jpeg'])) {
                                    $browseFileImg = Html::img($path, [
                                                'target' => '_blank',
                                                'title' => (isset($attachment) && $attachment['file_path'] != NULL) ? $attachment['file_path'] : '',
                                                'height' => '50',
                                                'width' => '50'
                                    ]);
                                }
                                $hindenImage = Html::hiddenInput('image', (isset($attachment) && $attachment['file_path'] != NULL) ? $attachment['file_path'] : '', ['class' => 'form-control hidden_image_path']);

                                $link = Html::a($browseFileImg . $browseFileText, $path, [
                                            'target' => '_blank',
                                            'class' => 'cboxElement',
                                            'data-rel' => 'colorbox'
                                ]);



                                $prevTR = <<<EOD
                                    <li class="item-orange clearfix ui-sortable-handle" style="position: relative; opacity: 1; left: 0px; top: 2px; z-index: auto;" data-id="">
                                        <label class="inline col-xs-4 no-padding">
                                            <span class="lbl col-sm-12">{$fileTitle}</span>
                                        </label>
                                        <div class="col-xs-7" style="overflow:hidden; text-overflow: ellipsis;">
                                            {$link}{$hindenImage}
                                        </div>

                                        <div class="col-xs-1 text-right right margin-top" style="height: 29px; line-height: 28px;">
                                            <i class="fa fa-fw fa-trash fa-lg red del-attach-row" data-id="{$attachment['id']}"></i>
                                        </div>
                                    </li>
EOD;

                                $prevTRs .= $prevTR;
                            }
//                            die();
                        }
                        ?>



                        <ul class="item-list ui-sortable attachment-list-doc col-sm-12" id="attachments" style="margin-left:0">
        <?= $newTR ?>
                            <?php
                            if (isset($attachment['file_path']))
                                echo $prevTRs;
                            ?>
                        </ul>

                            <?php if (isset($this->options['button']) && $this->options['button'] == true): ?>
                            <div class="col-sm-2 pull-right">
                                <button class="btn btn-success btn-xs">Submit</button>
                            </div>
        <?php endif; ?>

                    </div>

                </div>
            </div><!-- /.widget-main -->
        </div><!-- /.widget-box -->

        <?php
        $this->registerAssets($newTR);
    }

    /**
     * Register client assets
     */
    protected function registerAssets($newTR) {
        $view = $this->getView();
        $remID = '';
        //MultiUploadAsset::register($view);
        //  $jsTR = Json::htmlEncode($newTR);
        $jsTR = Json::encode($newTR);
        $url = Url::to(['/site/delete-attachment']);
        $js = <<<EOD
            $('body').on('click', 'span.addmore-attachment-doc', function(){
                $('ul.attachment-list-doc').append({$jsTR});
                $('ul.attachment-list-doc select').chosen();
            });

            $('#attachments').sortable({
                opacity: 0.8,
                revert: true,
                forceHelperSize: true,
                placeholder: 'draggable-placeholder',
                forcePlaceholderSize: true,
                tolerance: 'pointer',
                stop: function (event, ui) {
                    //just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
                    $(ui.item).css('z-index', 'auto');
                },
                update: function(event, ui){ groupUpdate() }
            });

            function groupUpdate() { 
                console.log("sortupdate")
                itemList = [];
                $('ul#attachments li').each(function(i, val){
                  ths = $(this);
                  id = ths.data('id');
                  itemList.push(id);
                });
                console.log(itemList);
                if(itemList!=""){
                    $.ajax({
                        data:{groups: itemList, company: 1 },
                        dataType:'html',
                        type:'POST',
                        url:'',
                        success:function(data){
                            console.log('Groups Updated');
                        }
                    });
                }
            }
            
            $('body').on('click', '.del-attach-row', function(){
                table = $('#hint').val();
                par = $(this).parents('li');
                if( $(this).data('id')!=undefined ){
                    id = $(this).data('id');
                    filePath = $(par).find('.hidden_image_path').val();
                    cn = confirm('Are you Sure');
                    if(cn==true){
                        $.ajax({
                            'type': 'GET',
                            'data' : {id:id,table:table,filePath:filePath},
                            'dataType' : 'html',
                            'cache' : false,
                            'url' : 'delete-attachment',
                            'success' : function(data){
                                par.fadeOut("slow");
                            },
                            'error': function(){
                                console.log('Error Occur...');
                            }
                        });
                    }
                }
                else{
                    par.remove();
                }
            });
EOD;
        $view->registerJs($js, $view::POS_END);



        $css = <<<EOD
            ul#attachments {
                max-height: 16.5em;
                min-height : 12em;
                overflow: hidden;
                overflow-y: scroll;
            }
			.item-orange .fa.fa-trash:hover{
				 cursor: pointer;
			}
EOD;
        $view->registerCss($css);
    }

    /**
     * Get plugin options
     * @return string
     */
    public function getOptions() {
        return Json::encode($this->options);
    }

    public function get_file_extension($file_name) {
        $extension = substr(strrchr($file_name, '.'), 1);
        return strtolower($extension);
    }

// END of attachmentBody()
}
?>
