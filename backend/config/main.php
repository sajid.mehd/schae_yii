<?php
use \yii\web\Request;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
  //  require(__DIR__ . '/../../vendor/semantics3/semantics3php/lib/Semantics3.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
	//require(__DIR__ . '/../../vendor')
);
require(__DIR__ . '/../../common/config/container.php');

$backEndBaseUrl = str_replace('/backend/web', '/frontend/web', (new Request)->getBaseUrl());
   
return [
    'id' => 'app-backend',
	'name'=>'ATS::',
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	//'layout' => '@app/themes/backend-theme/layouts/main',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
	//'extensions' => require(__DIR__ . '/../../vendor/semantics3/semantics3php/lib/Semantics3.php'),
    'modules' => [],


    'components' => [
    	'urlManagerFrontend' => [
                'class' => 'yii\web\urlManager',
                'baseUrl' => $backEndBaseUrl,
                'enablePrettyUrl' => false,
                'showScriptName' => false,
        ],
        
        'urlManagerBackend' => [
                'class' => 'yii\web\urlManager',
                'baseUrl' => '/frontend/',
                'enablePrettyUrl' => true,
                'showScriptName' => false,
        ],
        
    	'urlManager' => [
			'enablePrettyUrl' => true, // Cancel passing route in get 'r' paramater
			'showScriptName' => false, // Remove index.php from url
			//'suffix' => '.php', // Add suffix to all routes (globally)
		],
		/*'user' => [
		        'identityClass'   => 'app\models\User',
		        'enableAutoLogin' => true,
		    ],*/
        'user' => [
            'identityClass' => 'common\models\Users',
            'enableAutoLogin' => true,
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.atsengg.com',
                'username' => 'icpapemails@epatronus.com.pk',
                'password' => 'C)Ey,ovwDM.[',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
		
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'view'=> [
			'theme'=>[
				'pathMap' => ['@app/views' => '@app/themes/backend-theme'],
            	 'baseUrl' => '',
			],
		],
    ],
    'params' => $params,
];
