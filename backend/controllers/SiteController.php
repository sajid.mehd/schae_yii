<?php
 
namespace backend\controllers;

use common\components\EpatController;
use common\components\EpatModel;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Session;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use yii\filters\VerbFilter;
use backend\models\PsPosts;
use backend\models\PostDetails;
use backend\models\UploadForm;
use backend\models\search\PsPostsSearch;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\validators\FileValidator;
use backend\models\UploadImage;
use backend\models\UploadDocument;
use backend\models\AppAttachments;
use backend\models\DefLocation;
use backend\models\search\DefLocationSearch;
use backend\models\Members;
use backend\models\search\MembersSearch;
use backend\models\Countries;
use backend\models\Cities;
use backend\models\ProtectedPagesUsers;
use backend\models\search\ProtectedPagesUsersSearch;


//use yii\vendor\semantics3\lib\Semantics3\products;
/**
 * Site controller
 */
class SiteController extends EpatController {

    public $pageHeading = null;
    public $pageCaption = null;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['delete', 'logout', 'index', 'faq', 'manage-faq', 'testimonial', 'manage-testimonial', 'p-category', 'manage-p-category', 'product', 'manage-product', 'welcome-note', 'aboutus', 'manage-aboutus', 'vision', 'manage-vision', 'board-director', 'manage-board-director', 'team-management', 'manage-team-management', 'contactus', 'manage-contactus', 'project', 'manage-project', 'add-person-contact', 'client', 'manage-client', 'services', 'manage-services', 'products', 'manage-products', 'projects', 'manage-projects', 'clients', 'manage-clients', 'affiliation', 'manage-affiliation', 'brands', 'manage-brands', 'partner', 'manage-partner', 'events-gallery', 'manage-events-gallery', 'news', 'manage-news', 'download', 'manage-download', 'ctestimonial', 'manage-ctestimonial', 'test-library', 'service', 'manage-defservice', 'error'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

// ======================================================= //
// ======================= Index  ======================== //
// ======================================================= //

    public function actionIndex() {
        return $this->render('index');
    }

// ======================================================= //
// ======================= About Us  ======================== //
// ======================================================= //
    public function actionAboutus() {
        $model = new PsPosts();
        
        if (isset($_REQUEST['id'])) {
            $model = $model::findOne($_REQUEST['id']);
            
        }

        if (isset($_POST['detail'])) {

            $content['title']=$_REQUEST['posted']['title'];
            $content['detail'] = $_REQUEST['detail'];
            $title =$_REQUEST['posted']['title'];
            $type = 'about-us';

            savePosts($model, $content, $title, $type);

            return $this->redirect(['manage-aboutus']);
        }

        return $this->render('aboutus', ['model' => $model]);
    }

    public function actionManageAboutus() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'about-us');
        return $this->render('manage-aboutus', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }

    public function actionVideo() {
        $model = new PsPosts();

        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($id);
        }

        if (isset($_POST['posted'])) {

            $content = $_REQUEST['posted'];
            $title = '';
            $type = 'video';

            $modelSave = savePosts($model, $content, $title, $type);

            return $this->redirect(['manage-video']);
        }

        return $this->render('video', [
                    'model' => $model
        ]);
    }

    public function actionManageVideo() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'video');
        return $this->render('manage-video', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel
                        ]
        );
    }

// ======================================================= //
// ==================== Applications  ==================== //
// ======================================================= //

    public function actionApplications() {
        $model = new PsPosts();
        $applcAttachment = new AppAttachments();
        $table = NULL;
        $attachmentModel = new UploadImage();

        $attachmentArray = [];
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($_REQUEST['id']);
            $attachmentArray = EpatController::getAttachments($id, 'applications', '1');
            $table = 'app_attachments';
        }

        if (isset($_POST['posted'])) {
            // dd($_REQUEST);
            $content = $_REQUEST['posted'];
            $title = $content['title'];
            $content['detail'] = $_REQUEST['detail'];
            $type = 'applications';

            $model = savePosts($model, $content, $title, $type);
            if (isset($_POST['UploadImage'])) {
                $applcAttachment->resourceid = $model->id;
                $applcAttachment->is_image = 1;
                $attachmentModel->imageFiles = UploadedFile::getInstances($attachmentModel, 'imageFiles');
                $attachmentModel->imageTitle = isset($_POST['UploadImage']) ? $_POST['UploadImage']['imageTitle'] : [];
                EpatController::addAttachments($attachmentModel, $applcAttachment, 'applications');
            }

            return $this->redirect(['manage-applications']);
        }
        return $this->render('applications', ['model' => $model,
                    'attachmentArray' => $attachmentArray,
                    'table' => $table]);
    }

    public function actionManageApplications() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'applications');
        return $this->render('manage-applications', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }

    public function actionFebrications() {
        // dd($_REQUEST);
        $model = new PsPosts();
        $applcAttachment = new AppAttachments();
        $table = NULL;
        $attachmentModel = new UploadImage();

        $attachmentArray = [];
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($_REQUEST['id']);
            $attachmentArray = EpatController::getAttachments($id, 'febrications', '1');
            $table = 'app_attachments';
        }

        if (isset($_POST['posted'])) {
            // dd($_REQUEST);
            $content = $_REQUEST['posted'];
            $title = $content['title'];
            $content['detail'] = $_REQUEST['detail'];
            $type = 'febrications';

            $model = savePosts($model, $content, $title, $type);
            if (isset($_POST['UploadImage'])) {
                $applcAttachment->resourceid = $model->id;
                $applcAttachment->is_image = 1;
                $attachmentModel->imageFiles = UploadedFile::getInstances($attachmentModel, 'imageFiles');
                $attachmentModel->imageTitle = isset($_POST['UploadImage']) ? $_POST['UploadImage']['imageTitle'] : [];
                EpatController::addAttachments($attachmentModel, $applcAttachment, 'febrications');
            }

            return $this->redirect(['manage-febrications']);
        }
        return $this->render('febrications', ['model' => $model,
                    'attachmentArray' => $attachmentArray,
                    'table' => $table]);
    }

    public function actionManageFebrications() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'febrications');
        return $this->render('manage-febrications', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }


    public function actionSolarSystems() {
        // dd($_REQUEST);
        $model = new PsPosts();
        $applcAttachment = new AppAttachments();
        $table = NULL;
        $attachmentModel = new UploadImage();

        $attachmentArray = [];
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($_REQUEST['id']);
            $attachmentArray = EpatController::getAttachments($id, 'solarsystems', '1');
            $table = 'app_attachments';
        }

        if (isset($_POST['posted'])) {
            // dd($_REQUEST);
            $content = $_REQUEST['posted'];
            $title = $content['title'];
            $content['detail'] = $_REQUEST['detail'];
            $type = 'solarsystems';

            $model = savePosts($model, $content, $title, $type);
            if (isset($_POST['UploadImage'])) {
                $applcAttachment->resourceid = $model->id;
                $applcAttachment->is_image = 1;
                $attachmentModel->imageFiles = UploadedFile::getInstances($attachmentModel, 'imageFiles');
                $attachmentModel->imageTitle = isset($_POST['UploadImage']) ? $_POST['UploadImage']['imageTitle'] : [];
                EpatController::addAttachments($attachmentModel, $applcAttachment, 'solarsystems');
            }

            return $this->redirect(['manage-solar-systems']);
        }
        return $this->render('solar-systems', ['model' => $model,
                    'attachmentArray' => $attachmentArray,
                    'table' => $table]);
    }

    public function actionManageSolarSystems() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'solarsystems');
        return $this->render('manage-solar-systems', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }


    public function actionFeaturedImgDelete($id = Null){
        $model = new PsPosts();

        $id = $_REQUEST['id'];
        $model = $model::findOne($id);

        $rootPath = Yii::$app->getBasePath() . '/../frontend/web/';
        $filePath = $model['file_attachment'];
        // pre($rootPath . $filePath);
        // die();
        $filename = explode("/", $filePath);
        // pre($filename);
        // die();
        if (file_exists($rootPath . $filePath)) {
            unlink($rootPath . $filePath);
        }

        $model->file_attachment = '';
        $model->save(false);

        // ===================


        // return;

        // ===================

        $message = 'Record has been deleted successfully.';
        Yii::$app->session->setFlash('error', $message);
        if (Yii::$app->request->referrer) {
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->goHome();
        }
    }

    public function actionFaq() {
        $model = new PsPosts();

        if (isset($_REQUEST['id'])) {
            $model = $model::findOne($_REQUEST['id']);
        }

        if (isset($_POST['posted'])) {
            $content = $_REQUEST['posted'];
            $title = $content['question'];
            $type = 'faq';
            savePosts($model, $content, $title, $type);

            return $this->redirect(['manage-faq']);
        }
        return $this->render('faq', ['model' => $model]);
    }

    public function actionManageFaq() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, faq);
        return $this->render('manage-faq', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }

// ======================================================= //
// ====================== Projects  ====================== //
// ======================================================= //

    public function actionMainAreas() {
        $model = new PsPosts();
        
        if (isset($_REQUEST['id'])) {
            $model = $model::findOne($_REQUEST['id']);
        }

        if (isset($_POST['posted'])) {
            $content['main_area']=$_REQUEST['posted']['main_area'];
            $title =$_REQUEST['posted']['main_area'];
            $type = 'main_areas';

            savePosts($model, $content, $title, $type);
            return $this->redirect(['manage-main-areas']);
        }
        return $this->render('main-areas', ['model' => $model]);
    }

    public function actionManageMainAreas(){
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'main_areas');
        return $this->render('manage-main-areas', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }

    public function actionCountries() {
        $model = new PsPosts();
        
        if (isset($_REQUEST['id'])) {
            $model = $model::findOne($_REQUEST['id']);
        }

        if (isset($_POST['posted'])) {
            $content['main_area']=$_REQUEST['posted']['main_area'];
            $content['country'] = $_REQUEST['posted']['country'];
            $title =$_REQUEST['posted']['country'];

            $type = 'countries';
            $main_area = $_REQUEST['posted']['main_area'];
            $model->main_area = $main_area;
            $model->save(false);

            savePosts($model, $content, $title, $type);
            return $this->redirect(['manage-countries']);
        }
        return $this->render('countries', ['model' => $model]);
    }

    public function actionManageCountries(){
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'countries');
        return $this->render('manage-countries', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }

    public function actionProvinces() {
        $model = new PsPosts();
        
        if (isset($_REQUEST['id'])) {
            $model = $model::findOne($_REQUEST['id']);
        }

        if (isset($_POST['posted'])) {
            $content['main_area']=$_REQUEST['posted']['main_area'];
            $content['country'] = $_REQUEST['posted']['country'];
            $content['province'] = $_REQUEST['posted']['province'];
            $title =$_REQUEST['posted']['province'];

            $type = 'provinces';
            $main_area = $_REQUEST['posted']['main_area'];
            $country = $_REQUEST['posted']['country'];
            $model->main_area = $main_area;
            $model->country = $country;
            $model->save(false);

            savePosts($model, $content, $title, $type);
            return $this->redirect(['manage-provinces']);
        }
        return $this->render('provinces', ['model' => $model]);
    }

    public function actionManageProvinces(){
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'provinces');
        return $this->render('manage-provinces', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }

    public function actionCities() {
        $model = new PsPosts();
        
        if (isset($_REQUEST['id'])) {
            $model = $model::findOne($_REQUEST['id']);
        }

        if (isset($_POST['posted'])) {
            $content['country'] = $_REQUEST['posted']['country'];
            $content['province'] = $_REQUEST['posted']['province'];
            $content['city'] = $_REQUEST['posted']['city'];
            $title =$_REQUEST['posted']['city'];

            $type = 'cities';
            $country = $_REQUEST['posted']['country'];
            $province = $_REQUEST['posted']['province'];
            $model->country = $country;
            $model->province = $province;
            $model->save(false);

            savePosts($model, $content, $title, $type);
            return $this->redirect(['manage-cities']);
        }
        return $this->render('cities', ['model' => $model]);
    }

    public function actionManageCities(){
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'cities');
        return $this->render('manage-cities', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }

    public function actionAddProjects() {
        $model = new PsPosts();
        $applcAttachment = new AppAttachments();
        $table = NULL;
        $attachmentModel = new UploadImage();

        $attachmentArray = [];
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($_REQUEST['id']);
            $attachmentArray = EpatController::getAttachments($id, 'projects', '1');
            $table = 'app_attachments';
        }

        if (isset($_POST['posted'])) {
            dd($_REQUEST);
            $content = $_REQUEST['posted'];


            $title = $content['title'];
            $content['detail'] = $_REQUEST['detail'];
            $type = 'projects';

            $main_area = $content['main_area'];
            $country = $content['country'];
            $province = $content['province'];
            $city = $content['city'];

            $model->main_area = $main_area;
            $model->country = $country;
            $model->province = $province;
            $model->city = $city;

            $model->save(false);

            $model = savePosts($model, $content, $title, $type);
        // pre($model->id);
        // die();
            if (isset($_POST['UploadImage'])) {
                $applcAttachment->resourceid = $model->id;
                $applcAttachment->is_image = 1;
                $attachmentModel->imageFiles = UploadedFile::getInstances($attachmentModel, 'imageFiles');
                $attachmentModel->imageTitle = isset($_POST['UploadImage']) ? $_POST['UploadImage']['imageTitle'] : [];
                EpatController::addAttachments($attachmentModel, $applcAttachment, 'projects');
            }

            return $this->redirect(['manage-add-projects']);
        }
        return $this->render('add-projects', ['model' => $model,
                    'attachmentArray' => $attachmentArray,
                    'attachmentArrayDoc' => $attachmentArrayDoc,
                    'table' => $table]);
    }

    public function actionManageAddProjects() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'projects');
        return $this->render('manage-add-projects', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }


// ======================================================= //
// ====================== Products  ====================== //
// ======================================================= //

    public function actionProductCategories() {
        $model = new PsPosts();
        
        if (isset($_REQUEST['id'])) {
            $model = $model::findOne($_REQUEST['id']);
        }

        if (isset($_POST['posted'])) {

           // dd($_REQUEST);
            $content = $_REQUEST['posted'];
            $title =$_REQUEST['posted']['category'];
            $type = 'product_categories';
            // $parent_cat = $content['']

            savePosts($model, $content, $title, $type);
            return $this->redirect(['manage-product-categories']);
        }
        return $this->render('product-categories', ['model' => $model]);
    }

    public function actionManageProductCategories(){
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'product_categories');
        return $this->render('manage-product-categories', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }

    public function actionAddProducts() {
        $model = new PsPosts();
        $applcAttachment = new AppAttachments();
        $productAttachment = new AppAttachments();
        $table = NULL;
        $attachmentModel = new UploadImage();
        $attachmentDocumentModel = new UploadDocument();

        $attachmentArray = [];
        $attachmentArrayDoc = [];
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($_REQUEST['id']);
            $attachmentArray = EpatController::getAttachments($id, 'products', '1');
            $attachmentArrayDoc = EpatController::getAttachments($id, 'products', 0);
            $table = 'app_attachments';
        }

        if (isset($_POST['posted'])) {
            // dd($_REQUEST);
            $content = $_REQUEST['posted'];


            $title = $content['title'];
            $content['detail'] = $_REQUEST['detail'];
            $type = 'products';

            $category = $content['category'];
            $model->post_parent = $category;

            $model = savePosts($model, $content, $title, $type);       
            if (isset($_POST['UploadImage'])) {
                $applcAttachment->resourceid = $model->id;
                $applcAttachment->is_image = 1;
                $attachmentModel->imageFiles = UploadedFile::getInstances($attachmentModel, 'imageFiles');
                $attachmentModel->imageTitle = isset($_POST['UploadImage']) ? $_POST['UploadImage']['imageTitle'] : [];
                // dd($attachmentModel->imageTitle);
                EpatController::addAttachments($attachmentModel, $applcAttachment, 'products');
            }
            if (isset($_POST['UploadDocument'])) {
                $productAttachment->resourceid = $model->id;
                $productAttachment->is_image = 0;
                $attachmentDocumentModel->imageFiles = UploadedFile::getInstances($attachmentDocumentModel, 'imageFiles');
                $attachmentDocumentModel->imageTitle = isset($_POST['UploadDocument']) ? $_POST['UploadDocument']['imageTitle'] : [];
                // dd($attachmentDocumentModel->imageFiles);
                EpatController::addAttachments($attachmentDocumentModel, $productAttachment, 'products');
            }


            return $this->redirect(['manage-add-products']);
        }
        return $this->render('add-products', ['model' => $model,
                    'attachmentArray' => $attachmentArray,
                    'attachmentArrayDoc' => $attachmentArrayDoc,
                    'table' => $table]);
    }

    public function actionManageAddProducts() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'products');
        return $this->render('manage-add-products', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }





    public function actionDownloads() {
        $model = new PsPosts();
        $productAttachment = new AppAttachments();
        $table = NULL;
        $attachmentDocumentModel = new UploadDocument();

        $attachmentArrayDoc = [];
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($_REQUEST['id']);
            $attachmentArrayDoc = EpatController::getAttachments($id, 'downloads', 0);
            $table = 'app_attachments';
        }

        if (isset($_POST['posted'])) {
            // dd($_REQUEST);
            $content = $_REQUEST['posted'];


            $title = $content['title'];
            $type = 'downloads';

            $model = savePosts($model, $content, $title, $type);       
            
            if (isset($_POST['UploadDocument'])) {
                $productAttachment->resourceid = $model->id;
                $productAttachment->is_image = 0;
                $attachmentDocumentModel->imageFiles = UploadedFile::getInstances($attachmentDocumentModel, 'imageFiles');
                $attachmentDocumentModel->imageTitle = isset($_POST['UploadDocument']) ? $_POST['UploadDocument']['imageTitle'] : [];
                EpatController::addAttachments($attachmentDocumentModel, $productAttachment, 'downloads');
            }


            return $this->redirect(['manage-downloads']);
        }
        return $this->render('downloads', ['model' => $model,
                    'attachmentArrayDoc' => $attachmentArrayDoc,
                    'table' => $table]);
    }

    public function actionManageDownloads() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'downloads');
        return $this->render('manage-downloads', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }




// ======================================================= //
// ==================== News & Events  =================== //
// ======================================================= //


    public function actionNewsEvents() {
        $model = new PsPosts();
        $applcAttachment = new AppAttachments();
        $table = NULL;
        $attachmentModel = new UploadImage();

        $attachmentArray = [];
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($_REQUEST['id']);
            $attachmentArray = EpatController::getAttachments($id, 'news_events', '1');
            $table = 'app_attachments';
        }

        if (isset($_POST['posted'])) {
            // dd($_REQUEST);
            $content = $_REQUEST['posted'];
            
            $title = $content['title'];
            $content['detail'] = strip_tags($_REQUEST['detail'],'<br><ul><li><b>');
            $type = 'news_events';

            $date = $_REQUEST['posted']['date'];
            $time = strtotime($date);
            $newformat = date('Y-m-d',$time);
            $model->event_date = $newformat;
            $model->save(false);

            $model = savePosts($model, $content, $title, $type);
            if (isset($_POST['UploadImage'])) {
                $applcAttachment->resourceid = $model->id;
                $applcAttachment->is_image = 1;
                $attachmentModel->imageFiles = UploadedFile::getInstances($attachmentModel, 'imageFiles');
                $attachmentModel->imageTitle = isset($_POST['UploadImage']) ? $_POST['UploadImage']['imageTitle'] : [];
                EpatController::addAttachments($attachmentModel, $applcAttachment, 'news_events');
            }

            return $this->redirect(['manage-news-events']);
        }
        return $this->render('news-events', ['model' => $model,
                    'attachmentArray' => $attachmentArray,
                    'attachmentArrayDoc' => $attachmentArrayDoc,
                    'table' => $table]);
    }

    public function actionManageNewsEvents() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'news_events');
        return $this->render('manage-news-events', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }


// ======================================================= //
// ===================== Contact Us  ===================== //
// ======================================================= //


    public function actionContactus() {
        $model = new PsPosts();

        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($id);
        }

        if (isset($_POST['posted'])) {
            // dd($_REQUEST);
            $content = $_REQUEST['posted'];
            $title = $_REQUEST['posted']['area'];
            $type = 'contactus';
            $modelSave = savePosts($model, $content, $title, $type);

            return $this->redirect(['manage-contactus']);
        }

        return $this->render('contactus', [
                    'model' => $model
        ]);
    }

    public function actionManageContactus() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'contactus');
        return $this->render('manage-contactus', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel
                        ]
        );
    }

// ======================================================= //
// ======================= Clients  ====================== //
// ======================================================= //

    public function actionClients() {
        $model = new PsPosts();
        $clientAttachment = new AppAttachments();
        $table = NULL;
        $chk = 2;
        $attachmentModel = new UploadImage();
        $attachmentArray = [];
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($_REQUEST['id']);
            $attachmentArray = EpatController::getAttachments($id, 'clients', '1');
            $table = 'app_attachments';
        }

        if (isset($_POST['posted'])) {
            // dd($_REQUEST);
            $content = $_REQUEST['posted'];

            $title = $content['name'];
            $content['detail'] = $_REQUEST['detail'];
            $type = 'clients';

            $model = savePosts($model, $content, $title, $type);

            if (isset($_POST['UploadImage'])) {
                $clientAttachment->resourceid = $model->id;
                $clientAttachment->is_image = 1;
                $attachmentModel->imageFiles = UploadedFile::getInstances($attachmentModel, 'imageFiles');
                $attachmentModel->imageTitle = isset($_POST['UploadImage']) ? $_POST['UploadImage']['imageTitle'] : [];
                EpatController::addAttachments($attachmentModel, $clientAttachment, 'clients', $chk);
            }

            return $this->redirect(['manage-clients']);
        }

        return $this->render('clients', ['model' => $model,
                    'attachmentArray' => $attachmentArray,
                    'table' => $table]);
    }

    public function actionManageClients() {
        $searchModel = new PsPostsSearch();
        // dd($searchModel);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'clients');
        // die('hello');
        return $this->render('manage-clients', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }

// ======================================================= //
// ================= Brands We Deal With  ================ //
// ======================================================= //

    public function actionDistributors() {
        $model = new PsPosts();
        $clientAttachment = new AppAttachments();
        $table = NULL;
        $chk = 2;
        $attachmentModel = new UploadImage();
        $attachmentArray = [];
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = $model::findOne($_REQUEST['id']);
            $attachmentArray = EpatController::getAttachments($id, 'distributor', '1');
            $table = 'app_attachments';
        }

        if (isset($_POST['posted'])) {
            // dd($_REQUEST);
            $content = $_REQUEST['posted'];

            $title = $content['name'];
            $content['detail'] = $_REQUEST['detail'];
            $type = 'distributors';

            $model = savePosts($model, $content, $title, $type);

            if (isset($_POST['UploadImage'])) {
                $clientAttachment->resourceid = $model->id;
                $clientAttachment->is_image = 1;
                $attachmentModel->imageFiles = UploadedFile::getInstances($attachmentModel, 'imageFiles');
                $attachmentModel->imageTitle = isset($_POST['UploadImage']) ? $_POST['UploadImage']['imageTitle'] : [];
                EpatController::addAttachments($attachmentModel, $clientAttachment, 'distributor', $chk);
            }

            return $this->redirect(['manage-distributors']);
        }

        return $this->render('distributors', ['model' => $model,
                    'attachmentArray' => $attachmentArray,
                    'table' => $table]);
    }

    public function actionManageDistributors() {
        $searchModel = new PsPostsSearch();
        // dd($searchModel);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'distributors');
        // die('hello');
        return $this->render('manage-distributors', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }
// ======================================================= //
// ==================== Ajax Requests  =================== //
// ======================================================= //

    public function actionAjaxRequests() {
        $html = '';
        
        if (isset($_REQUEST['main_areas'])) {
            $main_areas = $_REQUEST['main_areas'];
                //dd($main_areas);
            if ($main_areas != '' || $main_areas != NULL) {
               $countries_list = PsPosts::find('post_type,main_area,recordstatus')->where('post_type=:ptype AND main_area=:marea AND recordstatus=:rec',[':ptype'=>'countries', ':marea' => $main_areas ,':rec' => 1])->all();
                $countries = [];
                foreach ($countries_list as $key => $val) {
                    $countries[$val['id']] = $val['post_title'];
                }
                $html = Html::dropDownList('posted[country]', '', $countries, [
                    'class' => 'form-control chosen-select-deselect',
                    'prompt' => "Select Country",
                ]);
                echo $html;
            }
        }
        else if (isset($_REQUEST['province'])) {
            $province = $_REQUEST['province'];
                //dd($main_areas);
            if ($province != '' || $province != NULL) {
               $city_list = PsPosts::find('post_type,province,recordstatus')->where('post_type=:ptype AND province=:prov AND recordstatus=:rec',[':ptype'=>'cities', ':prov' => $province ,':rec' => 1])->all();
                $cities = [];
                foreach ($city_list as $key => $val) {
                    $cities[$val['id']] = $val['post_title'];
                }
                $html = Html::dropDownList('posted[city]', '', $cities, [
                    'class' => 'form-control chosen-select-deselect',
                    'prompt' => "Select City",
                ]);
                echo $html;
            }
        }
    }

// ===================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================//





    public function actionDelete($id = NULL) {
        $model = new PsPosts();
        $id = $_REQUEST['id'];
        $model = $model::findOne($id);
      
        $model->recordstatus = '0';

        $model->save(false);

        $message = 'Record has been deleted successfully.';
        Yii::$app->session->setFlash('error', $message);
        if (Yii::$app->request->referrer) {
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->goHome();
        }
    }


     public function actionDeleteArea($id = NULL) {
        $model = new DefLocation();
        $id = $_REQUEST['id'];
        $model = $model::findOne($id);
       
        $model->recordstatus = '0';


        $model->save(false);

        $message = 'Record has been deleted successfully.';
        Yii::$app->session->setFlash('error', $message);
        if (Yii::$app->request->referrer) {
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->goHome();
        }
    }

    public function actionLogin() {
        $this->layout = 'login';

        if (!\Yii::$app->user->isGuest) {

            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    } 

    public function actionTestimonial() {
        $model = new PsPosts();
        if (isset($_REQUEST['id'])) {
            $model = $model::findOne($_REQUEST['id']);
        }

        if (isset($_POST['posted'])) {
            // dd($_REQUEST);
            $content = $_REQUEST['posted'];
           
            $title = $content['title'];
            $content['detail'] = $_REQUEST['detail'];
            $type = 'testimonial';

            savePosts($model, $content, $title, $type);

            return $this->redirect(['manage-testimonial']);
        }

        return $this->render('testimonial', ['model' => $model]);
    }

    public function actionManageTestimonial() {
        $searchModel = new PsPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'testimonial');
        return $this->render('manage-testimonial', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel]
        );
    }


public static function actionDeleteAttachment($id = NULL, $table = NULL, $filePath = NULL) {

        $table = 'app_attachments';
        $rootPath = Yii::$app->getBasePath() . '/../frontend/web/';
        unlink($rootPath.$filePath);
        $cmd = new \yii\db\Query();

        $cmd->createCommand()->delete($table, 'id=:id', [':id' => $id])->execute();
        return;
    }
    

}
