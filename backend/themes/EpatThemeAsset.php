<?php
namespace backend\themes;

use yii\web\AssetBundle;

/**
 * AdminLte AssetBundle
 * @since 0.1
 */
class EpatThemeAsset extends AssetBundle
{
    public $sourcePath = '@bower/backend-theme/assets';
    public $css = [
        'css/ace.css',
        'css/chosen.css',
        'css/ace-fonts.css',
        'css/font-awesome.css',
        'css/ace.onpage-help.css',
        'css/switch/bootstrap3/bootstrap-switch.css',
        'css/datepicker.css',
        'css/bootstrap-multiselect.css',
        //'../docs/assets/js/themes/sunburst.css'
    ];
    public $js = [
        'js/ace.js',
        'js/ace/elements.scroller.js',
        'js/chosen.jquery.js',
        'js/date-time/bootstrap-datepicker.js',
        'js/ace/ace.settings.js',
        'js/ace/ace.settings-rtl.js',
        'js/ace/ace.settings-skin.js',
        'js/switch/bootstrap-switch.js',

        'js/ace/elements.fileinput.js',
        'js/ace/elements.wysiwyg.js',
        'js/ace/elements.spinner.js',
        'js/ace/elements.treeview.js',
        'js/ace/elements.wizard.js',
        'js/ace/elements.aside.js',
        'js/ace/ace.sidebar.js',
        'js/ace/ace.sidebar-scroll-1.js',
        'js/ace/ace.submenu-hover.js',
        'js/ace/ace.widget-box.js',
        'js/ace/ace.settings.js',
        'js/ace/ace.settings-rtl.js',
        'js/ace/ace.settings-skin.js',
        'js/ace/elements.typeahead.js',
        'js/ace/ace.ajax-content.js',
        'js/ace/ace.touch-drag.js',
        'js/jquery-ui.custom.js',
        'js/jquery.ui.touch-punch.js',
        'js/jquery.easypiechart.js',
        'js/jquery.sparkline.js',
        'js/flot/jquery.flot.js',
        'js/flot/jquery.flot.pie.js',
        'js/ace/ace.widget-on-reload.js',
        'js/ace/ace.searchbox-autocomplete.js',
        'js/ace/elements.onpage-help.js',
        'js/ace/ace.onpage-help.js',
        'js/flot/jquery.flot.resize.js',
		'js/bootstrap-multiselect.js',
		'js/jquery.maskedinput.js',
       	'js/bootstrap-wysiwyg.js',
	/*	'../docs/assets/js/rainbow.js',
		'../docs/assets/js/language/generic.js',
		'../docs/assets/js/language/html.js',
		'../docs/assets/js/language/css.js',
		'../docs/assets/js/language/javascript.js',*/
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
