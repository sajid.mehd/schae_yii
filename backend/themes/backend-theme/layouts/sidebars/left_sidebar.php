<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<li class="">
    <a href="#" class="dropdown-toggle">
        <i class="menu-icon fa fa-desktop"></i>
        <span class="menu-text">
            CMS
        </span>
        <b class="arrow fa fa-angle-down"></b>
    </a>
    <b class="arrow"></b>
    <!-- ====================   ABOUT US   =================== -->
    <li class="">
        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>About Us', Url::to(['/site/manage-aboutus']))
        ?>
        <b class="arrow"></b>
    </li>
    <li class="">
        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>Applications', Url::to(['/site/manage-applications']))
        ?>
        <b class="arrow"></b>
    </li>
    <li class="">
        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>Solar Systems', Url::to(['/site/manage-solar-systems']))
        ?>
        <b class="arrow"></b>
    </li>
    
    <!-- ********************************************* -->
    <!-- ***************  Our Projects *************** -->
    <!-- ********************************************* -->
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-caret-right"></i>
            <span class="menu-text"> Projects </span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            <!-- <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Main Areas', Url::to(['/site/manage-main-areas']))
            ?>
                <b class="arrow"></b>
            </li>
            <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Countries', Url::to(['/site/manage-countries']))
            ?>
                <b class="arrow"></b>
            </li> -->
            <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Provinces', Url::to(['/site/manage-provinces']))
            ?>
                <b class="arrow"></b>
            </li>
            <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Cities', Url::to(['/site/manage-cities']))
            ?>
                <b class="arrow"></b>
            </li>
             <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Add Project', Url::to(['/site/manage-add-projects']))
            ?>
                <b class="arrow"></b>
            </li>
        </ul>
    </li>

    <!-- ********************************************* -->
    <!-- ***************  Our Products *************** -->
    <!-- ********************************************* -->

    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-caret-right"></i>
            <span class="menu-text"> Products </span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Product Categories', Url::to(['/site/manage-product-categories']))
            ?>
                <b class="arrow"></b>
            </li>
            <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Add Product', Url::to(['/site/manage-add-products']))
            ?>
                <b class="arrow"></b>
            </li>
        </ul>
    </li>

    <!-- ********************************************* -->
    <!-- ****************** Downloads **************** -->
    <!-- ********************************************* -->
    <li class="">
        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>Downloads', Url::to(['/site/manage-downloads']))
        ?>
        <b class="arrow"></b>
    </li>

    <!-- ********************************************* -->
    <!-- ***************  News & Events ************** -->
    <!-- ********************************************* -->

    <li class="">
        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>News & Events', Url::to(['/site/manage-news-events']))
        ?>
        <b class="arrow"></b>
    </li>

    <!-- ********************************************* -->
    <!-- ******************  Clients ***************** -->
    <!-- ********************************************* -->
    <li class="">
        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>Videos', Url::to(['/site/manage-video']))
        ?>
        <b class="arrow"></b>
    </li>
    <li class="">
        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>Clients', Url::to(['/site/manage-clients']))
        ?>
        <b class="arrow"></b>
    </li>

    <li class="">
        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>FAQs', Url::to(['/site/manage-faq']))
        ?>
        <b class="arrow"></b>
    </li>

    <li class="">
        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>Testimonials', Url::to(['/site/manage-testimonial']))
        ?>
        <b class="arrow"></b>
    </li>

    <!-- ********************************************* -->
    <!-- ******************  Distributors ***************** -->
    <!-- ********************************************* -->
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-caret-right"></i>
            <span class="menu-text"> Febrications </span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            <li class="">
                <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Ferrotek Febrication ', Url::to(['/site/manage-distributors']))
                ?>
                <b class="arrow"></b>
            </li>
            <li class="">
                <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Febrication ', Url::to(['/site/manage-febrications']))
                ?>
                <b class="arrow"></b>
            </li>
        </ul>
    </li>
    

    <!-- ********************************************* -->
    <!-- ***************  Contact Us ************** -->
    <!-- ********************************************* -->

    <li class="">
        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>Contact Us', Url::to(['/site/manage-contactus']))
        ?>
        <b class="arrow"></b>
    </li>
</li>
</li>
