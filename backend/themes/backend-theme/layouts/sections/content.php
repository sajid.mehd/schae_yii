<?php

use yii\widgets\Breadcrumbs;

use yii\bootstrap\Alert;

use app\components\EpatController;



?>



<div class="main-content">

    <div class="main-content-inner">

        <!-- #section:basics/content.breadcrumbs -->

        <div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">

            <script type="text/javascript">

                try {

                    ace.settings.check('breadcrumbs', 'fixed')

                } catch (e) {

                }

            </script>



            <ul class="breadcrumb">

                <li>

                    <i class="ace-icon fa fa-home home-icon"></i>

                    <a href="#">Home</a>

                </li>

                <li class="active">Dashboard</li>

            </ul><!-- /.breadcrumb -->



            <!-- #section:basics/content.searchbox

            <div class="nav-search" id="nav-search">

                <form class="form-search">

                    <span class="input-icon">

                        <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />

                        <i class="ace-icon fa fa-search nav-search-icon"></i>

                    </span>

                </form>

            </div><!-- /.nav-search -->



            <!-- /section:basics/content.searchbox -->

        </div>

        

        <div class="page-content">

            

            <div class="page-header">

                <h1>

                    <?= isset($this->context)? $this->context->pageHeading:""; ?>

                    <small>

                        <i class="ace-icon fa fa-angle-double-right"></i>

                        <?= isset($this->context)? $this->context->pageCaption:""; ?>

                    </small>

                </h1>

            </div><!-- /.page-header -->

            <!-- Flash Messages -->
            <div class="row flash-message">
                <?php
                if (Yii::$app->session->hasFlash('success')) {
                    $caption = "<strong>";
                    $caption .= "<i class='ace-icon fa fa-check'></i> ";
                    $caption .= "Success! ";
                    $caption .= "</strong>";
                    $bg = "alert alert-success";
                    $flashMessage = '<small>'.Yii::$app->session->getFlash('success').'</small>';
                } elseif (Yii::$app->session->hasFlash('error')) {
                    $caption = "<strong>";
                    $caption .= "<i class='ace-icon fa fa-times'></i> ";
                    $caption .= "Alert! ";
                    $caption .= "</strong>";
                    $bg = "alert alert-danger";
                    $flashMessage = '<small>'.Yii::$app->session->getFlash('error').'</small>';
                } elseif (Yii::$app->session->hasFlash('warning')) {
                    $caption = "<strong>";
                    //$caption .= "<i class='ace-icon fa fa-times'></i> ";
                    $caption .= "Warning! ";
                    $caption .= "</strong>";
                    $bg = "alert alert-warning";
                    $flashMessage = '<small>'.Yii::$app->session->getFlash('warning').'</small>';
                } elseif (Yii::$app->session->hasFlash('info')) {
                    $caption = "<strong>";
                    //$caption .= "<i class='ace-icon fa fa-times'></i> ";
                    $caption .= "Information! ";
                    $caption .= "</strong>";
                    $bg = "alert alert-info";
                    $flashMessage = '<small>'.Yii::$app->session->getFlash('info').'</small>';
                }
                if (isset($flashMessage) && $flashMessage != "") {
                    ?>
                    <div class="col-xs-12">
                        <div class="<?= $bg ?>">
                            <button data-dismiss="alert" class="close" type="button">
                                <i class="ace-icon fa fa-times"></i>
                            </button>
                            <?= $caption ?>
                            <?= $flashMessage ?>  
                            <br>
                        </div>
                    </div>

                    <?php
                }
                ?>
            </div>
            <!-- ./ END of Flash Messages -->

            <div class="row">

                <div class="col-xs-12">

                    <?= $content ?>

                </div>

            </div>



        </div>





    </div>

</div>



