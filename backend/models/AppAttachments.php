<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "app_attachments".
 *
 * @property integer $id
 * @property integer $resourceid
 * @property string $file_name
 * @property string $file_path
 * @property string $dateadded
 * @property string $addedbyuserid
 * @property integer $recordstatus
 * @property string $let
 */
class AppAttachments extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'app_attachments';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['resourceid', 'recordstatus','is_image'], 'integer'],
            [['file_name', 'file_path'], 'string'],
            [['dateadded', 'let'], 'safe'],
            [['addedbyuserid'], 'required'],
            [['addedbyuserid'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'resourceid' => 'Resourceid',
            'file_name' => 'File Name',
            'file_path' => 'File Path',
            'is_image' => 'Is Image',
            'dateadded' => 'Dateadded',
            'addedbyuserid' => 'Addedbyuserid',
            'recordstatus' => 'Recordstatus',
            'let' => 'Let',
        ];
    }

    public static function attachment($docAgainst, $doc_id, $doc_type) {
        $model = AppAttachments::find()->where('doc_against=:against AND doc_id=:did AND title=:dtitle', [
                    ':against' => $docAgainst,
                    ':did' => $doc_id,
                    'dtitle' => $doc_type,
                ])->one();
        return $model;
    }

    public static function getAllAttachments($rid){
        $all_images = AppAttachments::find()
                ->where('resourceid=:rid AND is_image=:img AND recordstatus=:rec', [':rid' => $rid , ':img' => 1 , ':rec' => 1])
                ->asArray()->all();
        return $all_images;
    }

    public static function getAllDocuments($rid){
        $all_images = AppAttachments::find()
                ->where('resourceid=:rid AND is_image=:img AND recordstatus=:rec', [':rid' => $rid , ':img' => 0 , ':rec' => 1])
                ->asArray()->all();
        return $all_images;
    }

}
