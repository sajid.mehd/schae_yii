<?php

namespace backend\models;

use \yii\web\IdentityInterface;
use common\components\EpatModel;
use app\components\EpatController;

//class Users extends \yii\base\Object implements IdentityInterface
class Users extends EpatModel implements IdentityInterface
{
	
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    /*
    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];*/
    
    private static $users;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        self::getAllUsers();
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        self::getAllUsers();
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        self::getAllUsers();
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
    
    
    public static function getAllUsers(){
        $userModel = new Users();
        $userModel = $userModel->find()->where(['recordstatus'=>1])->asArray()->all();
        if(sizeof($userModel)>0){
            foreach($userModel as $user){
                $index = $user['id'];
                $newusers[$index] = $user;
            }
            self::$users = $newusers;
        }else{
            self::$users = array();
        }
        
        return self::$users;
    }
    
}
