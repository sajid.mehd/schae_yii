<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TempShowResultWinter2017;

/**
 * TempShowResultWinter2017Search represents the model behind the search form about `backend\models\TempShowResultWinter2017`.
 */
class TempShowResultWinter2017Search extends TempShowResultWinter2017
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'regno', 'paper_title', 'result_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TempShowResultWinter2017::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'regno', $this->regno])
            ->andFilterWhere(['like', 'paper_title', $this->paper_title])
            ->andFilterWhere(['like', 'result_status', $this->result_status]);

        return $dataProvider;
    }
}