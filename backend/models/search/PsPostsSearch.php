<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PsPosts;

/**
 * PsPostsSearch represents the model behind the search form about `backend\models\PsPosts`.
 */
class PsPostsSearch extends PsPosts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'main_area', 'city', 'country', 'province', 'sorting', 'post_parent', 'addedbyusersr'], 'integer'],
            [['post_content', 'post_title', 'event_date', 'file_attachment', 'product_category', 'product_sub_category', 'post_name', 'post_modified', 'post_type', 'dateadded', 'recordstatus', 'let'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$type = NULL)
    {
        //dd($type);
        $query = PsPosts::find()->where(['recordstatus'=>1])->orderBy('id DESC');
        // $query = PsPosts::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'event_date' => $this->event_date,
            'main_area' => $this->main_area,
            'city' => $this->city,
            'country' => $this->country,
            'province' => $this->province,
            'sorting' => $this->sorting,
            'post_modified' => $this->post_modified,
            'post_parent' => $this->post_parent,
            'dateadded' => $this->dateadded,
            'addedbyusersr' => $this->addedbyusersr,
            'let' => $this->let,
        ]);

        $query->andFilterWhere(['like', 'post_content', $this->post_content])
            ->andFilterWhere(['like', 'post_title', $this->post_title])
            ->andFilterWhere(['like', 'file_attachment', $this->file_attachment])
            ->andFilterWhere(['like', 'product_category', $this->product_category])
            ->andFilterWhere(['like', 'product_sub_category', $this->product_sub_category])
            ->andFilterWhere(['like', 'post_name', $this->post_name])
            ->andFilterWhere(['like', 'post_type', $this->post_type])
            ->andFilterWhere(['like', 'recordstatus', $this->recordstatus]);
            if(isset($type) && $type != NULL)   
            $query->andFilterWhere(['post_type'=>$type]);

        return $dataProvider;
    }
}