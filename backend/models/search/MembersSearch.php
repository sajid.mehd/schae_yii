<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Members;

/**
 * PsPostsSearch represents the model behind the search form about `app\models\PsPosts`.
 */
class MembersSearch extends Members
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Sr'], 'integer'],
            [['membershipnumber', 'membershipname', 'status', 'country'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$type = NULL)
    {
        $actionid =  Yii::$app->controller->action->id;
        if($actionid=='members')
           $query = Members::find()->where(['is_active'=>1])->orderBy('Sr DESC');    
        else
        $query = Members::find()->orderBy('Sr DESC');
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            // 'Sr' => $this->Sr,
            // 'membershipnumber' => $this->membershipnumber,
            // 'membershipname' => $this->membershipname,
            // 'status' => $this->status,
            // 'country' => $this->country,
        ]);
         $query->andFilterWhere(['like','membershipname', $this->membershipname,])
               ->andFilterWhere(['like','membershipnumber', $this->membershipnumber])
               ->andFilterWhere(['like','status', $this->status])
               ->andFilterWhere(['like','country', $this->country]);


  //       $query->andFilterWhere(['like', 'post_content', $this->post_content])
  //           ->andFilterWhere(['like', 'post_title', $this->post_title])
  //           ->andFilterWhere(['like', 'post_summary', $this->post_summary])
  //           ->andFilterWhere(['like', 'post_status', $this->post_status])
  //           ->andFilterWhere(['like', 'post_name', $this->post_name])
  //           ->andFilterWhere(['like', 'post_type', $this->post_type])
  //           ->andFilterWhere(['like', 'recordstatus', $this->recordstatus]);
		// if(isset($type) && $type != NULL)	
		// 	$query->andFilterWhere(['post_type'=>$type]);	
		

        // pre($_REQUEST);
        // pre($dataProvider);
        // die();
        return $dataProvider;
    }
}