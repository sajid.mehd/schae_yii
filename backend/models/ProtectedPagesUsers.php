<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protected_pages_users".
 *
 * @property integer $id
 * @property string $user
 * @property string $password
 * @property string $status
 * @property string $date_created
 */
class ProtectedPagesUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protected_pages_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'password', 'status', 'date_created'], 'required'],
            [['date_created'], 'safe'],
            [['user', 'password', 'status'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'password' => 'Password',
            'status' => 'Status',
            'date_created' => 'Date Created',
        ];
    }
}
