<?php
namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;
	public $photo;
	public $logo;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['file'], 'file','maxFiles' => 10],
        ];
    }
}
 
?>