<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ps_posts".
 *
 * @property integer $id
 * @property string $post_content
 * @property string $post_title
 * @property string $event_date
 * @property string $file_attachment
 * @property integer $main_area
 * @property integer $city
 * @property integer $country
 * @property integer $province
 * @property integer $sorting
 * @property string $product_category
 * @property string $product_sub_category
 * @property string $post_name
 * @property string $post_modified
 * @property integer $post_parent
 * @property string $post_type
 * @property string $dateadded
 * @property integer $addedbyusersr
 * @property string $recordstatus
 * @property string $let
 */
class PsPosts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_content', 'post_title', 'event_date', 'main_area', 'city', 'country', 'province', 'product_category', 'product_sub_category', 'post_name', 'post_modified', 'post_parent', 'post_type', 'dateadded', 'addedbyusersr', 'recordstatus'], 'required'],
            [['post_content', 'post_title'], 'string'],
            [['event_date', 'post_modified', 'dateadded', 'let'], 'safe'],
            [['main_area', 'city', 'country', 'province', 'sorting', 'post_parent', 'addedbyusersr'], 'integer'],
            [['file_attachment'], 'string', 'max' => 500],
            [['product_category', 'product_sub_category'], 'string', 'max' => 100],
            [['post_name', 'post_type'], 'string', 'max' => 200],
            [['recordstatus'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_content' => 'Post Content',
            'post_title' => 'Post Title',
            'event_date' => 'Event Date',
            'file_attachment' => 'File Attachment',
            'main_area' => 'Main Area',
            'city' => 'City',
            'country' => 'Country',
            'province' => 'Province',
            'sorting' => 'Sorting',
            'product_category' => 'Product Category',
            'product_sub_category' => 'Product Sub Category',
            'post_name' => 'Post Name',
            'post_modified' => 'Post Modified',
            'post_parent' => 'Post Parent',
            'post_type' => 'Post Type',
            'dateadded' => 'Dateadded',
            'addedbyusersr' => 'Addedbyusersr',
            'recordstatus' => 'Recordstatus',
            'let' => 'Let',
        ];
    }


    public static function getPostWithParent($type, $parent_post = NULL){
        $model = NULL;
        if($parent_post != NULL){
             $model = PsPosts::find()
                ->select('id,post_content,post_title, post_summary, post_type, post_parent, recordstatus')
                        ->where('post_type=:ptype AND post_parent=:p_parent AND recordstatus=:rec', [
                                ':ptype' => $type,
                                 ':p_parent'=>$parent_post,
                                 ':rec' => 1]
                                 )
                        ->asArray()
                        ->orderBy('sorting')
                        ->all();


        }
        // pre($model);
        // die();

        return $model;
    }

    // public static function getPostWithParentOrder($type, $parent_post = NULL){
    //     $model = NULL;
    //     // pre($model);
    //     // die();
    //     if($parent_post != NULL){
    //          $model = PsPosts::find()
    //             ->select('id,post_content,post_title, post_summary, post_type, post_parent, recordstatus')
    //                     ->where('post_type=:ptype AND post_parent=:p_parent AND recordstatus=:rec', [
    //                             ':ptype' => $type,
    //                              ':p_parent'=>$parent_post,
    //                              ':rec' => 1]
    //                              )
    //                     ->orderBy('CAST(`post_summary` AS INT)')
    //                     ->asArray()
    //                     ->all();
    //                     // pre($model);
    //                     // die();


    //     }

    //     return $model;
    // }

    public static function getPostWithParentOrder($type, $main_area = NULL, $limit= NULL){
        $model = NULL;
        // pre($model);
        // die();
        if($main_area != NULL){
             $model = PsPosts::find()
                ->select('id,post_content,post_title, post_type, main_area, recordstatus')
                        ->where('post_type=:ptype AND main_area=:m_area AND recordstatus=:rec', [
                                ':ptype' => $type,
                                 ':m_area'=>$main_area,
                                 ':rec' => 1]
                                 )
                        ->limit($limit)
                        ->asArray()
                        ->all();
                        // pre($model);
                        // die();


        }

        return $model;
    }

    public static function getPostsAgainstMainAreas($type, $proj_id = NULL){
        $model = NULL;
        // dd($proj_id);
        if($proj_id != NULL){
             $model = PsPosts::find()
                ->select('id,post_content,post_title, post_type, main_area, recordstatus')
                ->where('post_type=:ptype AND main_area=:m_area AND recordstatus=:rec', 
                    [':ptype' => $type, ':m_area'=>$proj_id, ':rec' => 1])
                ->asArray()
                ->all();
        }
        // dd($model);
        return $model;
    }

    public static function getCityAgainstProvince($type, $province = NULL){
        $model = NULL;
        // dd($province);
        if($province != NULL){
            $model = PsPosts::find()
            ->select('id,post_content,post_title, post_type, province')
            ->where('post_type=:ptype AND province=:prov AND recordstatus=:rec', [
                ':ptype' => $type,
                ':prov'=>$province,
                ':rec' => 1]
            )
            ->asArray()
            ->all();
        }
        return $model;
    }
    public static function getProjectsAgainstCity($type, $city = NULL){
        $model = NULL;
        // dd($province);
        if($city != NULL){
            $model = PsPosts::find()
            ->select('id,post_content,post_title, post_type, city')
            ->where('post_type=:ptype AND city=:cty AND recordstatus=:rec', [
                ':ptype' => $type,
                ':cty'=>$city,
                ':rec' => 1]
            )
            ->asArray()
            ->all();
        }
        return $model;
    }


    /**
    ** Get Post Data with limit / Order i.e., 'ASC' or 'DESC'
    **/

    public static function getPost($type, $is_limit=NULL, $order=NULL) {
        $return = '';
        if(isset($order) && $order!=NULL){
            $model = PsPosts::find()
                ->select('id,post_content,post_title,event_date,post_parent, file_attachment, post_type, recordstatus')
                ->where('post_type=:ptype AND recordstatus=:rec', [':ptype' => $type, ':rec' => 1])
                ->orderBy("id $order")
                ->asArray()
                ->all();
            $return = $model;
        }
        else if($is_limit==NULL){
            $model = PsPosts::find()
                ->select('id,post_content,post_title,event_date,post_parent, file_attachment, post_type, recordstatus')
                        ->where('post_type=:ptype AND recordstatus=:rec', [':ptype' => $type, ':rec' => 1])
                        ->asArray()
                        ->all();
                        $return = $model;
        }
        else if(is_numeric($is_limit)){
        $model = PsPosts::find()
                ->select('id,post_content,post_title,event_date,post_parent, file_attachment, post_type, recordstatus')
                        ->where('post_type=:ptype AND recordstatus=:rec', [':ptype' => $type, ':rec' => 1])
                        ->asArray()->limit($is_limit)->all();   
                        $return = $model;
        }

        return $return;
    }

    public static function getPostWithOrderLimit($type, $is_limit=NULL, $order=NULL) {
        $model = PsPosts::find()
                ->select('id,post_content,event_date,post_title, post_type, recordstatus')
                ->where('post_type=:ptype AND recordstatus=:rec', [':ptype' => $type, ':rec' => 1])
                ->orderBy("event_date $order")
                ->limit($is_limit)
                ->asArray()
                ->all();
        return $model;
    }



    public static function getProduct($type, $productType) {
        $model = PsPosts::find()->where('post_type=:ptype AND post_content LIKE :pcont', [':ptype' => $type, ':pcont' => '%' . $productType . '%'])->all();
        return $model;
    }

    public static function getCategoryById($id) {
        $model = PsPosts::findOne($id);
        return ($model != NULL) ? $model->post_title : '-';
    }
    public static function getCountryById($id) {
        // dd($id);
        $model = PsPosts::findOne($id);
        return ($model != NULL) ? $model->post_title : '-';
    }

    public static function getOnePost($id, $title=FALSE) {
        $model = PsPosts::find()
                ->where('id=:pid AND recordstatus=:rec', [':pid' => $id, ':rec' => 1])
                ->asArray()
                ->one();

        if($title==TRUE)
            return $model['post_title'];
        return $model;
    }

    public static function getProjectsList($list = []) {
        $model = PsPosts::find()
                ->where('recordstatus=:rec', [':rec' => 1])
                ->andWhere(['IN', 'id', $list])
                ->asArray()
                ->all();

        return $model;
    }

    public static function getMainAreas($type = NULL){
        $areas = PsPosts::find()
        ->select('id,post_title, recordstatus')
        ->where('post_type=:ptype AND recordstatus=:rec', [':ptype' => $type, ':rec' => 1])
        ->asArray()->all();
        foreach ($areas as $key => $value) {
            $all_areas[] = $value['post_title'];
        }
        return $all_areas;
    }

    public static function getcity($type = NULL){
        if($type == NULL){
            $cities = PsPosts::find()
            ->where("post_type = 'city'")
            ->orWhere("post_type = 'country'")
            ->asArray()->all();
            foreach ($cities as $key => $value) {
                $all_cities[] = $value['post_title'];
            }
        }else{
            $cities = PsPosts::find()
            ->where("post_type = 'city'")
            ->orWhere("post_type = 'country'")
            ->asArray()->all();
            foreach ($cities as $key => $value) {
                $all_cities[$value['post_title']] = $value['post_title'];
            }
        }
        return $all_cities;
    }
}