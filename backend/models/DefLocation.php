<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "def_location".
 *
 * @property integer $id
 * @property string $title
 * @property integer $parent_id
 * @property integer $level
 * @property string $dateadded
 * @property integer $recordstatus
 * @property integer $addedbyuserid
 * @property string $let
 */
class DefLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'def_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string'],
            [['parent_id', 'level', 'recordstatus', 'addedbyuserid'], 'integer'],
            [['dateadded', 'let'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'parent_id' => 'Parent ID',
            'level' => 'Level',
            'dateadded' => 'Dateadded',
            'recordstatus' => 'Recordstatus',
            'addedbyuserid' => 'Addedbyuserid',
            'let' => 'Let',
        ];
    }
}
