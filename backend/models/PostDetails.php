<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "post_details".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $details_type
 * @property string $Details
 */
class PostDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'details_type', 'Details'], 'required'],
            [['post_id'], 'integer'],
            [['details_type', 'Details'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'details_type' => 'Details Type',
            'Details' => 'Details',
        ];
    }
}
