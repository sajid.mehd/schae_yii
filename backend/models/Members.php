<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "members".
 *
 * @property integer $Sr
 * @property string $membershipnumber
 * @property string $membershipname
 * @property string $status
 * @property string $country
 */
class Members extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'members';
    }
    /**
     * @inheritdoc
     */
    const SCENARIO_INSERT = 'Insertion';
    const SCENARIO_UPDATE = 'Updation';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_INSERT] = ['Sr', 'membershipnumber','status','country','membershipname'];
        $scenarios[self::SCENARIO_UPDATE] = ['Sr', 'membershipnumber','status','country','membershipname'];
        return $scenarios;
    }

    public function rules()
    {
        return [
            [['Sr', 'membershipnumber', 'membershipname', 'status', 'country'], 'required'],
            [['Sr'], 'integer'],
            ['membershipnumber','filter','filter'=>'trim'],
            ['membershipnumber','checkUnique','on' => self::SCENARIO_INSERT],
            [['membershipnumber', 'membershipname', 'status', 'country'], 'string']
        ];
    }

    /**
     * @inheritdoc 
     */
    public function attributeLabels()
    {
        return [
            'Sr' => 'Sr',
            'membershipnumber' => 'Membershipnumber',
            'membershipname' => 'Membershipname',
            'status' => 'Status',
            'country' => 'Country',
        ];
    }

    public function checkUnique($attribute, $params){
        $attr = $this->membershipnumber;
        $count = Members::find()->where(['membershipnumber' =>$this->membershipnumber])->count();
        if($count > 0){
            $this->addError($attribute,'Must Enter a Unique Membership Number');
        }
    }
    public static function CountryList(){
        $countries = array();
        $list = Members::find()->select('country')->distinct()->asArray()->all();


        if(sizeof($list) > 0){
            foreach ($list as $key => $value) {
                $name = $value['country'];

                $countries[$name] = $name;

            }

        }


        return $countries;
    }

    public static function saveDirectory($membership_no , $member_name , $status , $country,$model){
     // $model = new Members;
       
        $model->membershipnumber = $membership_no;
        $model->membershipname = $member_name;
        $model->status = $status;
        $model->country = $country;

        $model->save(false);
        // pre($model->attributes);
        // die('trigger');
    }
}
