<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadImage extends Model {

    /**
     * @var UploadedFile
     */
    /*
      public $imageFile;

      public function rules() {
      return [
      [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
      ];
      }

      public function upload() {
      if ($this->validate()) {
      $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
      return true;
      } else {
      return false;
      }
      }*
     * 
     */

    public $imageFiles;
    public $imageTitle;

    public function rules() {
        return [
            [['imageTitle'], 'safe'],
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, JPG', 'maxFiles' => 10],
        ];
    }

    public function upload() {
        if ($this->validate()) {

            $rootPath = Yii::$app->getBasePath() . '/';
            $filePath = 'attachments' . '/';

            //pre($_REQUEST);
            //die();
            foreach ($this->imageFiles as $file) {
                $random_num = sprintf('%04d', rand(4, 999));
                $filename = $file->name;

                if (file_exists($rootPath . $filePath . $filename)) {
                    $filename = $random_num . $file->name;
                }

                $file->saveAs($rootPath . $filePath . $filename, true);
                //$file->saveAs('uploads/' . $file->baseName . '.' . $file->extension);
                /*
                  $model->title = $filename;
                  $model->path = $filePath;
                  $model->save(false);
                 */
            }
            return true;
        } else {
            return false;
        }
    }

}
