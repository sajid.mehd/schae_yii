
jQuery(function ($) {
	
	$('._ajaxCall').on('click', function(event){
    	event.preventDefault();
    	var data = {};
		url = $(this).attr('href');
	//	console.log(url);
		shifts(url);
	});
	
	function shifts(ajaxUrl=""){
	
		$('body').on('click','#delshift', function(){
			val = $(this).attr('sr');
		//	console.log(val);
			var conf = confirm('Are You Sure to delete Shift');		
		
			if(conf==true && val!=undefined && val!=''){
				par = $(this).parents('tr'); 
			//	console.log(par);
			//	par.remove();
				$.ajax({  
					type: "POST",  
					url: ajaxUrl,
					data: {shiftdel:val},
					cache: true,
					dataType: 'json',
					success: function(data){
						par.remove();
					}
				});
			}
		});
		
		
		$('body').on('click','#addShift', function() {	
		returnValue = checkValues();
		if(returnValue=='ok'){
		defalt = $('span[id^="def"]');
		defalt.parents('tr').remove();
		
		//var itemId = $("#item_name").attr('value');
		shiftSr = $('#shifts_idx').val() ;
		shiftTitle = $("#shifts_idx option:selected").text();
		asFrom = $("#assignedFrom_idx").val();
		asTo = $("#assignedTo_idx").val();		
		//asType =$('input:checked').val(); 
		asType ='Regular'; 
		asRemarks = $("#assigningRemarks_idx").val();		
		
		shifts = shiftTitle.split(')'); 
		shiftTiming = '';
		
		for(i in shifts){
			if(i==0){					
				shiftTitle = shifts[0]+')';	
			}
			if(i==1){
				shiftTiming = shifts[1];	
			}
		}
				
		shiftListDisplay = '<tr><td>'+shiftTitle+'<br/>'+shiftTiming+'<input type="hidden" name="TempShifts[shiftSr][] id="TempShifts_shiftSr" value="'+shiftSr+'" /></td><td>'+asFrom+'<input type="hidden" name="TempShifts[assignedFromDate][] id="TempShifts_assigned_from_date" value="'+asFrom+'" /></td><td>'+asTo+'<input type="hidden" name="TempShifts[assignedToDate][] id="TempShifts_assigned_to_date" value="'+asTo+'" /></td><td>'+asType+'<input type="hidden" name="TempShifts[assigningType][] id="TempShifts_assigned_type" value="'+asType+'" /><input type="hidden" name="TempShifts[assigningRemarks][] id="TempShifts_assigning_remarks" value="'+asRemarks+'" /></td><td><span id="remItem"><span class="ace-icon glyphicon glyphicon-trash del_image_clx"></span></span></td></tr>';			
//			alert(shiftListDisplay);
	    	$('#myShifts tr:last').after(shiftListDisplay);
			
			$("#assignedFrom_idx").val('');
			$("#assignedTo_idx").val('');
			$("#assigningRemarks_idx").val('');
		}
		
		});		
	
		$('body').on('click','#remItem', function() { 
	                $(this).parents('tr').remove();
	                return false;
	      });
	
		function checkValues(){
			shiftSr = $('#shifts_idx').val() ;	
			if(shiftSr=='' || shiftSr==null){
				return 'empty';
			}else{
				return 'ok';
			}
		}        
		
	}
	
	
	
    if (!ace.vars['touch']) {
        $('.chosen-select, .chosen-select-deselect').chosen({allow_single_deselect: true});
        //resize the chosen on window resize

        $(window)
                .off('resize.chosen')
                .on('resize.chosen', function () {
                    $('.chosen-select').each(function () {
                        var $this = $(this);
                        $this.next().css({'width': $this.parent().width()});
                    })
                }).trigger('resize.chosen');
        //resize chosen on sidebar collapse/expand
        $(document).on('settings.ace.chosen', function (e, event_name, event_val) {
            if (event_name != 'sidebar_collapsed')
                return;
            $('.chosen-select').each(function () {
                var $this = $(this);
                $this.next().css({'width': $this.parent().width()});
            })
        });


        $('#chosen-multiple-style .btn').on('click', function (e) {
            var target = $(this).find('input[type=radio]');
            var which = parseInt(target.val());
            if (which == 2)
                $('#form-field-select-4').addClass('tag-input-style');
            else
                $('#form-field-select-4').removeClass('tag-input-style');
        });
    }
    
    // scrollables
    $('.scrollable').each(function () {
        var $this = $(this);
        $(this).ace_scroll({
            size: $this.attr('data-size') || 100,
            //styleClass: 'scroll-left scroll-margin scroll-thin scroll-dark scroll-light no-track scroll-visible'
        });
    });
    $('.scrollable-horizontal').each(function () {
        var $this = $(this);
        $(this).ace_scroll(
                {
                    horizontal: true,
                    styleClass: 'scroll-top', //show the scrollbars on top(default is bottom)
                    size: $this.attr('data-size') || 500,
                    mouseWheelLock: true
                }
        ).css({'padding-top': 12});
    });

    $(window).on('resize.scroll_reset', function () {
        $('.scrollable-horizontal').ace_scroll('reset');
    });


    $('#id-checkbox-vertical').prop('checked', false).on('click', function () {
        $('#widget-toolbox-1').toggleClass('toolbox-vertical')
                .find('.btn-group').toggleClass('btn-group-vertical')
                .filter(':first').toggleClass('hidden')
                .parent().toggleClass('btn-toolbar')
    });

    /**
     //or use slimScroll plugin
     $('.slim-scrollable').each(function () {
     var $this = $(this);
     $this.slimScroll({
     height: $this.data('height') || 100,
     railVisible:true
     });
     });
     */


    /**$('.widget-box').on('setting.ace.widget' , function(e) {
     e.preventDefault();
     });*/

    /**
     $('.widget-box').on('show.ace.widget', function(e) {
     //e.preventDefault();
     //this = the widget-box
     });
     $('.widget-box').on('reload.ace.widget', function(e) {
     //this = the widget-box
     });
     */

    //$('#my-widget-box').widget_box('hide');



    // widget boxes
    // widget box drag & drop example
    $('.widget-container-col').sortable({
        connectWith: '.widget-container-col',
        items: '> .widget-box',
        handle: ace.vars['touch'] ? '.widget-header' : false,
        cancel: '.fullscreen',
        opacity: 0.8,
        revert: true,
        forceHelperSize: true,
        placeholder: 'widget-placeholder',
        forcePlaceholderSize: true,
        tolerance: 'pointer',
        start: function (event, ui) {
            //when an element is moved, it's parent becomes empty with almost zero height.
            //we set a min-height for it to be large enough so that later we can easily drop elements back onto it
            ui.item.parent().css({'min-height': ui.item.height()})
            //ui.sender.css({'min-height':ui.item.height() , 'background-color' : '#F5F5F5'})
        },
        update: function (event, ui) {
            ui.item.parent({'min-height': ''})
            //p.style.removeProperty('background-color');
        }
    });
    
    $("[class='bootstrap-checkbox']").bootstrapSwitch();

    //datepicker plugin
    //link
    $('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true
    })
    //show datepicker when clicking on the icon
    .next().on(ace.click_event, function(){
        $(this).prev().focus();
    });
	
	
	$('.multiselect').multiselect({
		 enableFiltering: true,
		 buttonClass: 'btn btn-white btn-primary',
		 templates: {
			button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
			ul: '<ul class="multiselect-container dropdown-menu"></ul>',
			filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
			filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
			li: '<li><a href="javascript:void(0);"><label></label></a></li>',
			divider: '<li class="multiselect-item divider"></li>',
			liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
		 }
		});



//in ajax mode, remove remaining elements before leaving page
	$(document).one('ajaxloadstart.page', function(e) {
		$('[class*=select2]').remove();
		$('select[name="duallistbox_demo1[]"]').bootstrapDualListbox('destroy');
		$('.rating').raty('destroy');
		$('.multiselect').multiselect('destroy');
	});
	
	
	// Shifts js
	
	$('.input-mask-time').mask('99:99');
	
	$('#editor1').ace_wysiwyg({
		toolbar:
		[
			{
				name:'font',
				title:'Custom tooltip',
				values:['Some Font!','Arial','Verdana','Comic Sans MS','Custom Font!']
			},
			null,
			{
				name:'fontSize',
				title:'Custom tooltip',
				values:{1 : 'Size#1 Text' , 2 : 'Size#1 Text' , 3 : 'Size#3 Text' , 4 : 'Size#4 Text' , 5 : 'Size#5 Text'} 
			},
			null,
			{name:'bold', title:'Custom tooltip'},
			{name:'italic', title:'Custom tooltip'},
			{name:'strikethrough', title:'Custom tooltip'},
			{name:'underline', title:'Custom tooltip'},
			null,
			'insertunorderedlist',
			'insertorderedlist',
			'outdent',
			'indent',
			null,
			{name:'justifyleft'},
			{name:'justifycenter'},
			{name:'justifyright'},
			{name:'justifyfull'},
			null,
			{
				name:'createLink',
				placeholder:'Custom PlaceHolder Text',
				button_class:'btn-purple',
				button_text:'Custom TEXT'
			},
			{name:'unlink'},
			null,
			{
				name:'insertImage',
				placeholder:'Custom PlaceHolder Text',
				button_class:'btn-inverse',
				//choose_file:false,//hide choose file button
				button_text:'Set choose_file:false to hide this',
				button_insert_class:'btn-pink',
				button_insert:'Insert Image'
			},
			null,
			{
				name:'foreColor',
				title:'Custom Colors',
				values:['red','green','blue','navy','orange'],
				/**
					You change colors as well
				*/
			},
			/**null,
			{
				name:'backColor'
			},*/
			null,
			{name:'undo'},
			{name:'redo'},
			null,
			'viewSource'
		],
		//speech_button:false,//hide speech button on chrome
		
		'wysiwyg': {
			hotKeys : {} //disable hotkeys
		}
		
	}).prev().addClass('wysiwyg-style2');

	if($('.detail_clx').val() != ''){
		hid = $('.detail_clx').val();
		$('#editor1').html(hid);
	}
	
		//handle form onsubmit event to send the wysiwyg's content to server
	$('.myform').on('submit', function(){
		editor = $('#editor1').html();
		/*alert(editor);
		alert($('input[name=wysiwyg-value]').val());*/
		//put the editor's html content inside the hidden input to be sent to server
		$('input[name=detail]' , this).val(editor);
		
		//but for now we will show it inside a modal box

		//$('#modal-wysiwyg-editor').modal('show');
		//$('#wysiwyg-editor-value').css({'width':'99%', 'height':'200px'}).val($('#editor').html());
		
		//return false;
	});
	
	$('#myform').on('reset', function() {
			$('#editor').empty();
	});
	
});
	