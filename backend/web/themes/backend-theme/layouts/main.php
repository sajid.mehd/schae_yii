<?php
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\components\EpatController;

//use backend\assets\AppAsset;
use backend\themes;


//@backend\themes\EpatThemeAsset::register($this);
@backend\themes\EpatThemeAsset::register($this);
@backend\assets\AppAsset::register($this);



$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@bower') . '/epat-theme';
?>
<?php $this->beginPage(); ?>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="ePatronus Enterprise IT Solutions">
        <meta name="description" content="">
        <meta name="robots" content="all">*
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link rel="shortcut icon" href="<?php echo $this->theme->baseUrl ?>/img/favicon.png"/>-->
        <link rel="shortcut icon" href="<?php echo Yii::$app->urlManagerBackend->baseUrl; ?>/img/favicon.png"/>
        
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
	
<style>
.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
    padding: 0 !important;
}
</style>

    <body class="no-skin">
        <?php $this->beginBody() ?>

        <?=
        $this->render(
                'sections/header.php', ['directoryAsset' => $directoryAsset]
        )
        ?>

        <div class="main-container" id="main-container">

            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>

            <?=
            $this->render(
                    'sections/left.php', ['directoryAsset' => $directoryAsset]
            )
            ?>

            <?=
            $this->render(
                    'sections/content.php', ['content' => $content, 'directoryAsset' => $directoryAsset]
            )
            ?>

            <?=
            $this->render(
                    'sections/footer.php', ['directoryAsset' => $directoryAsset]
            )
            ?>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>

        </div>
        <!--<script type="text/javascript"> ace.vars['base'] = '..'; </script>-->
        <?php 
		$this->registerJsFile(Yii::$app->request->baseUrl.'/js/app.js', 
                [
                    'depends'=>[
                        'yii\web\YiiAsset',
                        'yii\bootstrap\BootstrapPluginAsset'
                    ], $this::POS_END
                ] ) ?>
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>
