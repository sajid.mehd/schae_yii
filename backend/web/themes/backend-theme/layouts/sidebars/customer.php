<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>



<li class="">
    <a href="#" class="dropdown-toggle">
        <i class="menu-icon fa fa-desktop"></i>
        <span class="menu-text">
            Customers
        </span>

        <b class="arrow fa fa-angle-down"></b>
    </a>

    <b class="arrow"></b>
    <ul class="submenu">
        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-caret-right"></i>
                Customers
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>
            <ul class="submenu">
                <li class="">
                    <?=
                    Html::a('<i class="menu-icon fa fa-caret-right"></i>Manage Customers', Url::to(['/customer/manage/']), [])
                    ?>
                    <b class="arrow"></b>
                </li>

                <li class="">
                    <?=
                    Html::a('<i class="menu-icon fa fa-caret-right"></i>New Customer', Url::to(['/customer/manage/create']), [])
                    ?>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-caret-right"></i>
                Settings
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>
            <ul class="submenu">
                <li class="">
                    <?=
                    Html::a('<i class="menu-icon fa fa-caret-right"></i>Csutomer Categories', Url::to(['/customer/setting/category']), [])
                    ?>
                    <b class="arrow"></b>
                </li>

                <li class="">
                    <?=
                    Html::a('<i class="menu-icon fa fa-caret-right"></i>Customer Area', Url::to(['/customer/setting/area']), [])
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?=
                    Html::a('<i class="menu-icon fa fa-caret-right"></i>Customer City', Url::to(['/customer/setting/city']), [])
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?=
                    Html::a('<i class="menu-icon fa fa-caret-right"></i>Credit Status', Url::to(['/customer/setting/credit-status']), [])
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?=
                    Html::a('<i class="menu-icon fa fa-caret-right"></i>Default Payment Terms', Url::to(['/customer/setting/default-payment']), [])
                    ?>
                    <b class="arrow"></b>
                </li>

            </ul>
        </li>


    </ul>
</li>