<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>



<li class="">
    <a href="#" class="dropdown-toggle">
        <i class="menu-icon fa fa-desktop"></i>
        <span class="menu-text">
            HR
        </span>

        <b class="arrow fa fa-angle-down"></b>
    </a>

    <b class="arrow"></b>
    <ul class="submenu">
       
        
        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-caret-right"></i>
                Settings
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>
            <ul class="submenu">
                <li class="">
                     <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Manage Departments', Url::to(['/hr/settings/department']), [])
                ?>
                    <b class="arrow"></b>
                </li>
                
                <li class="">
                    <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Manage Designation', Url::to(['/hr/settings/designation']), [])
                ?>
                    <b class="arrow"></b>
                </li>
				<li class="">
                     <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Manage Dependents', Url::to(['/hr/settings/dependent']), [])
                ?>
                    <b class="arrow"></b>
					</li>
					<li class="">
                     <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Manage Branches', Url::to(['/hr/settings/branch']), [])
                ?>
                    <b class="arrow"></b>	
            </li>
			
			<li class="">
                     <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Manage Qaulification', Url::to(['/hr/settings/qualification']), [])
                ?>
                    <b class="arrow"></b>	
            </li>
			<li class="">
                     <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Manage Skills', Url::to(['/hr/settings/skills']), [])
                ?>
                    <b class="arrow"></b>	
            </li>
			<li class="">
                     <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Manage Language', Url::to(['/hr/settings/language']), [])
                ?>
                    <b class="arrow"></b>	
            </li>
			<li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Allowance', Url::to(['/hr/settings/allowance']))
		 			?>
                    <b class="arrow"></b>
                </li>
				<li class="">
                   <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Deduction', Url::to(['/hr/settings/deduction']))
		 			?>
                    <b class="arrow"></b>
                </li>
				<li class="">
                   <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Bonus', Url::to(['/hr/settings/bonus']))
		 			?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Job Title', Url::to(['/hr/settings/job-title']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Employee Status', Url::to(['/hr/settings/emp-status']))
                    ?>
                    <b class="arrow"></b>
                </li>

                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Job Category', Url::to(['/hr/settings/job-category']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Pay Scale', Url::to(['/hr/settings/pay-scale']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Probation', Url::to(['/hr/settings/probation']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Process', Url::to(['/hr/settings/process']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Tax Slots', Url::to(['/hr/settings/taxslots']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Time Margins', Url::to(['/hr/settings/time-margin']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Time Relaxation', Url::to(['/hr/settings/time-relaxation']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Contacts', Url::to(['/hr/settings/contact']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Group', Url::to(['/hr/settings/group']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Job Type', Url::to(['/hr/settings/job-time']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Overtime', Url::to(['/hr/settings/overtime']))
                    ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?= Html::a('<i class="menu-icon fa fa-caret-right"></i>Leaves', Url::to(['/hr/settings/leaves']))
                    ?>
                    <b class="arrow"></b>
                </li>
			</ul>
		</li>
		<li class="">
        	<a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-caret-right"></i>
                Attendance
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>
            <ul class="submenu">
				<li class="">
                     <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>All Attendances', Url::to(['/hr/attendance/view-attendance']), [])
                ?>
                    <b class="arrow"></b>
                </li>
				
			</ul>
		</li>
		<li class="">
        	<a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-caret-right"></i>
                Attendance
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>
            <ul class="submenu">
				<li class="">
                     <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>All Attendances', Url::to(['/hr/attendance/view-attendance']), [])
                ?>
                    <b class="arrow"></b>
                </li>
				<li class="">
                     <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Half Days Attendances', Url::to(['/hr/attendance/half-days-att']), [])
                ?>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Mark Attendance', Url::to(['/hr/attendance/manual-attendance']), [])
                ?>
                    <b class="arrow"></b>
                </li>
				<li class="">
                    <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Attendance History', Url::to(['/hr/attendance/att-history']), [])
                ?>
                    <b class="arrow"></b>
                </li>
				
			</ul>
		</li>
			
    </ul>
    </li>
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-caret-right"></i>
            Profile
            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>
        <ul class="submenu">
            <li class="">
                <?=
                Html::a('<i class="menu-icon fa fa-caret-right"></i>Manage Employee', Url::to(['/hr/profile/manage']), [])
                ?>
                <b class="arrow"></b>
            </li>
         </ul>
     </li>

</li>