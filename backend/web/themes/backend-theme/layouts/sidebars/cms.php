<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<ul class="sidebar-menu">
    <li>
        <a href="#" class="navbar-link">
            <i class="fa fa-angle-down"></i> <span class="text-bold text-gray">Inventory</span>
        </a>
    </li>
    <!--<li class="active">
        <a href="<?= $directoryAsset ?>/">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>-->
    <!-- Products -->
    <li class="treeview">
        <a href="<?= $directoryAsset ?>/#">
            <i class="fa fa-bar-chart-o"></i>
            <span>Products</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <?=
                Html::a('<i class="fa fa-angle-double-right"></i>Manage Products', Url::to(['/inventory/product/manage-product']))
                ?>
            </li>
            <li>
                <?=
                Html::a('<i class="fa fa-angle-double-right"></i>New Products', Url::to(['/inventory/product/create-product']))
                ?>
            </li>       
        </ul>
    </li>
    
    <!-- Categories -->
    <li class="treeview">
        <a href="<?= $directoryAsset ?>/#">
            <i class="fa fa-bar-chart-o"></i>
            <span>Categories</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <?=
                Html::a('<i class="fa fa-angle-double-right"></i>Manage Categories', Url::to(['/inventory/product/product_manage']), [])
                ?>
            </li>
            <li>
                <?=
                Html::a('<i class="fa fa-angle-double-right"></i>New Category', Url::to(['/inventory/product/product_manage']), [])
                ?>
            </li>       
        </ul>
    </li>
    
    <!-- Formations -->
    <li class="treeview">
        <a href="<?= $directoryAsset ?>/#">
            <i class="fa fa-bar-chart-o"></i>
            <span>Formations</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <?=
                Html::a('<i class="fa fa-angle-double-right"></i>Manage Formation', Url::to(['/inventory/product/product_manage']), [])
                ?>
            </li>
            <li>
                <?=
                Html::a('<i class="fa fa-angle-double-right"></i>New Formation', Url::to(['/inventory/product/product_manage']), [])
                ?>
            </li>       
        </ul>
    </li>
    
</ul>