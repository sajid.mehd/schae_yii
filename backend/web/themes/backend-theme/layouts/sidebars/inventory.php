<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>

<li class="">

    <a href="#" class="dropdown-toggle">

        <i class="menu-icon fa fa-desktop"></i>

        <span class="menu-text">

            CMS

        </span>



        <b class="arrow fa fa-angle-down"></b>

    </a>



    <b class="arrow"></b>
    <!--	 <ul class="submenu">-->

<li class="">
    <?=
    Html::a('<i class="menu-icon fa fa-caret-right"></i>JTE Batteries', Url::to(['/site/manage-jtebatteries']))
    ?>
    <b class="arrow"></b>
</li>

<li class="">
    <?=
    Html::a('<i class="menu-icon fa fa-caret-right"></i>Main Areas', Url::to(['/site/manage-mainareas']))
    ?>
    <b class="arrow"></b>
</li>
<li class="">
    <a href="#" class="dropdown-toggle">
        <i class="menu-icon fa fa-caret-right"></i>
        <span class="menu-text"> Products </span>
        <b class="arrow fa fa-angle-down"></b>
    </a>
    <b class="arrow"></b>

    <ul class="submenu">
        <li class="">

        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>Product Categories', Url::to(['/site/manage-p-category']))
        ?>

            <b class="arrow"></b>

        </li>
        <li class="">

        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>Sub Category', Url::to(['/site/manage-product']))
        ?>

            <b class="arrow"></b>

        </li>
        <li class="">

        <?=
        Html::a('<i class="menu-icon fa fa-caret-right"></i>Add Products', Url::to(['/site/manage-products']))
        ?>

            <b class="arrow"></b>

        </li>
    </ul>
</li>
<li class="">
    <a href="#" class="dropdown-toggle">
        <i class="menu-icon fa fa-caret-right"></i>
        <span class="menu-text"> Projects </span>
        <b class="arrow fa fa-angle-down"></b>
    </a>

    <b class="arrow"></b>

    <ul class="submenu">
        <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Project Categories', Url::to(['/site/manage-project-category']))
            ?>

            <b class="arrow"></b>
        </li>

        <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Add Projects', Url::to(['/site/manage-projects']))
            ?>

            <b class="arrow"></b>
        </li>

    </ul>
</li>
<li class="">
    <a href="#" class="dropdown-toggle">
        <i class="menu-icon fa fa-caret-right"></i>
        <span class="menu-text"> About </span>
        <b class="arrow fa fa-angle-down"></b>
    </a>

    <b class="arrow"></b>

    <ul class="submenu">
        <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>About Us', Url::to(['/site/manage-aboutus']))
            ?>

            <b class="arrow"></b>
        </li>

        <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Vision / Mission', Url::to(['/site/manage-vision']))
            ?>

            <b class="arrow"></b>
        </li>

        <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>History', Url::to(['/site/manage-history']))
            ?>

            <b class="arrow"></b>
        </li>

        
    </ul>
</li>


<li class="">
<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>Certificates', Url::to(['/site/manage-certificate']))
?>

    <b class="arrow"></b>
</li>

<li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>Why Choose Us', Url::to(['/site/manage-why-choose-us']))
?>

    <b class="arrow"></b>

</li>

<li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>How We Work', Url::to(['/site/manage-how-we-work']))
?>

    <b class="arrow"></b>

</li>

<li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>Clients', Url::to(['/site/manage-clients']))
?>

    <b class="arrow"></b>

</li>
<li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>Partners', Url::to(['/site/manage-distributors']))
?>

    <b class="arrow"></b>

</li>


<li class="">
    <a href="#" class="dropdown-toggle">
        <i class="menu-icon fa fa-caret-right"></i>
        <span class="menu-text"> Services </span>
        <b class="arrow fa fa-angle-down"></b>
    </a>

    <b class="arrow"></b>

    <ul class="submenu">
        <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Services', Url::to(['/site/manage-services']))
            ?>

            <b class="arrow"></b>
        </li>

        <li class="">
            <?=
            Html::a('<i class="menu-icon fa fa-caret-right"></i>Sub Services', Url::to(['/site/manage-sub-services']))
            ?>

            <b class="arrow"></b>
        </li>
    </ul>
</li>

<li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>News', Url::to(['/site/manage-news']))
?>

    <b class="arrow"></b>

</li>
<li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>Testimonial', Url::to(['/site/manage-testimonial']))
?>

    <b class="arrow"></b>

</li>




<!-- <li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i> Vision', Url::to(['/site/manage-vision']))
?>

    <b class="arrow"></b>

</li> -->

<!-- <li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i> Why ATS', Url::to(['/site/manage-responsibility']))
?>

    <b class="arrow"></b>

</li> -->
<!-- <li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>What is Ferrotek', Url::to(['/site/manage-ferrotek']))
?>

    <b class="arrow"></b>

</li> -->
<!-- <li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>Event', Url::to(['/site/manage-event']))
?>

    <b class="arrow"></b>

</li> -->
<!-- <li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>Certificate', Url::to(['/site/manage-certificate']))
?>

    <b class="arrow"></b>

</li> -->
<!--<li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>History', Url::to(['/site/manage-history']))
?>

    <b class="arrow"></b>

</li>-->




<li class="">

<?=
Html::a('<i class="menu-icon fa fa-caret-right"></i>Contact Us', Url::to(['/site/manage-contactus']))
?>

    <b class="arrow"></b>

</li>





<!-- <li class="">

<?php
echo Html::a('<i class="menu-icon fa fa-caret-right"></i>FAQ', Url::to(['/site/manage-faq']))
?>

    <b class="arrow"></b>

</li> -->




<!--            </ul>-->
<!-- <ul class="submenu">

     <li class="">

         <a href="#" class="dropdown-toggle">

             <i class="menu-icon fa fa-caret-right"></i>

             Manage

             <b class="arrow fa fa-angle-down"></b>

         </a>

                     
         <b class="arrow"></b>

     
     </li>

 </ul>-->

</li>

