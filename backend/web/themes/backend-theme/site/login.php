<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = Yii::$app->name . ' Login';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="login-container">
            <div class="center">
                <h1>
                    <i class="ace-icon fa fa-leaf green"></i>
                    <span class="red">Progressive Ventures</span>
                    <!--<span class="white" id="id-text2">Clicks</span>-->
                </h1>
            </div>

            <div class="space-6"></div>

            <div class="position-relative">
                <div id="login-box" class="login-box visible widget-box no-border">
                    <div class="widget-body">
                        <div class="widget-main">
                            <h4 class="header blue lighter bigger">
                                <i class="ace-icon fa fa-coffee green"></i>
                                Please Enter Your Information
                            </h4>

                            <div class="space-6"></div>

                            <?php
                            $form = ActiveForm::begin([
                                        'id' => 'login-form',
                            ]);
                            ?>
                            <fieldset>
                                <label class="block clearfix">
                                    <span class="block input-icon input-icon-right">
                                        <!--<input type="text" class="form-control" placeholder="Username" />-->
                                        <?=
                                        $form->field($model, 'username')->textInput([
                                            'class' => 'form-control',
                                            'placeholder' => 'Username'
                                        ])->label(false);
                                        ?>
                                        <i class="ace-icon fa fa-user"></i>
                                    </span>
                                </label>

                                <label class="block clearfix">
                                    <span class="block input-icon input-icon-right">
                                        <!--<input type="password" class="form-control" placeholder="Password" />-->
                                        <?=
                                        $form->field($model, 'password')->passwordInput([
                                            'class' => 'form-control',
                                            'placeholder' => 'Password'
                                        ])->label(false);
                                        ?>
                                        <i class="ace-icon fa fa-lock"></i>
                                    </span>
                                </label>

                                <div class="space"></div>

                                <div class="clearfix">
                                    <label class="inline">
                                        <input type="checkbox" class="ace" />
                                        <span class="lbl"> Remember Me</span>
                                    </label>

                                    <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                        <i class="ace-icon fa fa-key"></i>
                                        <span class="bigger-110">Login</span>
                                    </button>
                                </div>

                                <div class="space-4"></div>
                            </fieldset>
                            <?php ActiveForm::end(); ?>

                        </div><!-- /.widget-main -->

                        <div class="toolbar clearfix text-center">
                            <h5 class="white bolder" id="id-company-text">
                                <small class="white">Product of : </small>
                                <?= Html::a('ePatronus Enterprise IT Solutions', Url::to('http://www.epatronus.com'),[
                                    'target'=>'_blank',
                                    'class' => 'white'
                                ]) ?> 
                            </h5>
                        </div>
                    </div><!-- /.widget-body -->
                </div><!-- /.login-box -->
            </div><!-- /.position-relative -->

        </div>
    </div><!-- /.col -->
</div><!-- /.row -->

<?php
/*
  $form->field($model, 'rememberMe', [
  'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
  ])->checkbox() */
?>
