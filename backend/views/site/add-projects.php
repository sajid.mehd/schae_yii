<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;
use app\components\EpatController;
use yii\web\View;
use backend\components\multiupload\MultiUploadWidget;
use backend\components\multiupload\MultiUploadWidgetDocument;
//use backend\models\PsPosts;
/* @var $this yii\web\View */
/* @var $model app\models\hr\HrDefallowance */
/* @var $form yii\widgets\ActiveForm */
if (isset($this->context)) {
    $this->context->pageHeading = "CMS";
    $this->context->pageCaption = "Add Project";
}
?>
<div class="hr-basicinfo-form">
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => 'form-horizantal form-control myform',
                    'role' => 'from',
                    'style' => 'border:none'
                ]
    ]);
    ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title">Project</h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-sm-12" id="province_for_hide">
                                <div class="row form-group">
                                    <label class="col-xs-2">Province:</label>
                                    <div class="col-xs-5">
                                        <?php
                                        $province_list = pCategoryList('provinces');
                                        // dd($area_list);
                                        $province = (isset($_REQUEST['id'])) ? $model->province : NULL;
                                        // dd($_REQUEST['id']);

                                        echo Html::dropDownList('posted[province]', $province, $province_list, [
                                            'prompt' => 'Select Province', 'class' => ' col-sm-12 chosen-select', 'id' => 'provinces'
                                                ]
                                        );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" id="city_for_hide">
                                <div class="row form-group">
                                    <label class="col-xs-2">City:</label>
                                    <div class="col-xs-5" id="cities">
                                        <?php
                                        $cities_list = pCategoryList('cities');
                                        // dd($area_list);
                                        $city = (isset($_REQUEST['id'])) ? $model->city : NULL;
                                        // dd($_REQUEST['id']);

                                        echo Html::dropDownList('posted[city]', $city, $cities_list, [
                                            'prompt' => 'Select City', 'class' => ' col-sm-12 chosen-select'
                                                ]
                                        );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row form-group">
                                    <label class="col-xs-2">Title:</label>
                                    <div class="col-xs-5">
                                        <?php
                                        $product = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'title') :
                                                NULL;
                                        echo Html::textInput('posted[title]', $product, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>
                                    </div> 
                                </div>
                                <div class="row form-group">
                                    <label class="col-xs-2">Description:</label>
                                    <div class="col-xs-10">
                                        <div class="wysiwyg-editor" id="editor1"></div>
                                        <?php
                                        $detail = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'detail') :
                                                NULL;
                                        echo Html::hiddenInput('detail', $detail, ['class' => 'col-sm-12 detail_clx']
                                        );
                                        ?>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-12">
                                        <?php
                                        if (isset($attachmentArray)) {
                                            echo MultiUploadWidget::widget([
                                                'form' => $form,
                                                'options' => [
                                                    'attachmentArray' => $attachmentArray
                                                ]
                                            ]);
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>


<?php
    $script = <<<EOD
      $('body').on('change','#main_areas',function(){
          var main_area_id = $('#main_areas').val();
          console.log(main_area_id);
          if(main_area_id == 8)    //  World Wide
          {
            $('#country_for_hide').css('display','none');
            $('#province_for_hide, #city_for_hide').css('display','block');
          }
          if(main_area_id == 9)    // Nation Wide
          {
            $('#province_for_hide, #city_for_hide').css('display','none');
            $('#country_for_hide').css('display','block');
          }
  });

  // ======================== //

  $('body').on('change','#provinces',function(){
          var province_id = $('#provinces').val();
          console.log(province_id);
          $.ajax({
              'type' : 'GET',
              'dataType' : 'html',
              'url' : 'ajax-requests',
              'data' : {province:province_id},
              'success' : function(data){
                  $('#cities').html(data);
                  $('select').chosen();
            },
            'error' : function(){
                console.log('Error Occured...');
            }
        });
  });
  
        
EOD;
$this->registerJs($script, $this::POS_END)
?>