<?php



use yii\helpers\Html;

use yii\grid\GridView;



/* @var $this yii\web\View */

/* @var $searchModel app\models\hr\search\HrPersonalinfoSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = "Manage Video Link";

$this->params['breadcrumbs'][] = $this->title;





if (isset($this->context)) {

    $this->context->pageHeading = "CMS";

    $this->context->pageCaption = "Manage Video Link";

}

?>

<div class="hr-faq-index">



    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <p>

        <?= Html::a('Create', ['video'], ['class' => 'btn btn-success']) ?>

    </p>



    <?= GridView::widget([

        'dataProvider' => $dataProvider,

        'filterModel' => $searchModel,

        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],

			


            [

                'attribute' => 'title',

                'header' => 'Title',

                'value' => function($data) {

                  return (decodePost($data,'title') != NULL) ? decodePost($data,'title'): "-";

                 },

            ],
                          [

                'attribute' => 'description',

                'header' => 'Description',

                'value' => function($data) {

                  return (decodePost($data,'description') != NULL) ? decodePost($data,'description'): "-";

                 },

            ],
                          [

                'attribute' => 'link',

                'header' => 'Video Link',

                'value' => function($data) {

                  return (decodePost($data,'link') != NULL) ? decodePost($data,'link'): "-";

                 },

            ],
                        

			 /*[

                'attribute' => 'google-map',

                'header' => 'Google Map Address',

                'value' => function($data) {

                  return (decodePost($data,'google-map') != NULL) ? decodePost($data,'google-map'): "-";

                 },

            ],*/

			

            ['class' => 'yii\grid\ActionColumn',

				'template'=>'{update}{delete}',

				'buttons'=>[

					'update'=>function($url,$model,$key){

						$url = ['site/video','id'=>$model->id];

						return HTML::a('<span class="fa fa-fw fa-edit fa-lg"></span>',$url,[

							'title'=>Yii::t('yii','update contact'),

						]);

					},

					'delete'=>function($url,$model,$key){

						$url = ['site/delete','id'=>$key];

						return Html::a('<span class="fa fa-fw fa-remove text-red fa-lg"></span>',$url,[

							'title'=>Yii::t('yii','delete contact'),
                                                    'data-confirm' => Yii::t('yii', 'Are You Sure??')

						]);

					},

				],

			],

        ],

    ]); ?>



</div>