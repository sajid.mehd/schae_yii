<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;
use app\components\EpatController;
use app\models\PsPosts;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model app\models\hr\HrDefallowance */
/* @var $form yii\widgets\ActiveForm */
$this->title = "Add Main Area";
$this->params['breadcrumbs'][] = $this->title;
if (isset($this->context)) {
    $this->context->pageHeading = "CMS";
    $this->context->pageCaption = "Main Area";
}

?>
<div class="aboutus-form">
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => 'form-horizantal form-control myform',
                    'role' => 'from',
                    'style' => 'border:none'
                ]
    ]);
    ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title">Main Area</h4>
                </div>
                   
                <div class="widget-body">
                    <div class="widget-main"> 
                        <div class=row>
                            <div class="col-sm-12">
                                <div class="row form-group">
                                    <label class="col-xs-2">Area:</label>
                                    <div class="col-xs-5">
                                        <?php
                                        $main_area = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'main_area') :
                                                NULL;
                                        echo Html::textInput('posted[main_area]', $main_area, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
