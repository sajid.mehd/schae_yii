<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\hr\search\HrPersonalinfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = "Manage AboutUs";
$this->params['breadcrumbs'][] = $this->title;
if (isset($this->context)) {
    $this->context->pageHeading = "Dashboard";
    $this->context->pageCaption = "Manage AboutUs";
}
?>
<div class="hr-faq-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?php //Html::a('Create', ['aboutus'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'header' => 'Title',
                'value' => function($data) {
                  return (decodePost($data,'title') != NULL) ? decodePost($data,'title'): "-";
                 },
            ],
            [
                'attribute' => 'detail',
                'header' => 'Detail',
                'format' =>'html',
                'value' => function($data) {
                  return (decodePost($data,'detail') != NULL) ? (decodePost($data,'detail')): "-";
                 },
            ],
			
            ['class' => 'yii\grid\ActionColumn',
				'template'=>'{update}',
				'buttons'=>[
					'update'=>function($url,$model,$key){
						$url = ['site/aboutus','id'=>$model->id];
						return HTML::a('<span class="fa fa-fw fa-edit fa-lg"></span>',$url,[
							'title'=>Yii::t('yii','update AboutUs'),
						]);
					},
					'delete'=>function($url,$model,$key){
						$url = ['site/delete','id'=>$key];
						return Html::a('<span class="fa fa-fw fa-remove text-red fa-lg"></span>',$url,[
							'title'=>Yii::t('yii','delete about'),
                                                    'data-confirm' => Yii::t('yii', 'Are You Sure??')
						]);
					},
				],
			],
        ],
    ]); ?>
</div> 