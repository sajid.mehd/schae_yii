<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;
use app\components\EpatController;
use backend\models\PsPosts;
use yii\web\View;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\hr\HrDefallowance */
/* @var $form yii\widgets\ActiveForm */
$this->title = "Add Province";
$this->params['breadcrumbs'][] = $this->title;
if (isset($this->context)) {
    $this->context->pageHeading = "CMS";
    $this->context->pageCaption = "Province";
}
$countr = 1;
$prov = 2;
?>
<div class="aboutus-form">
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => 'form-horizantal form-control myform',
                    'role' => 'from',
                    'style' => 'border:none'
                ]
    ]);
    ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title">Province</h4>
                </div>
                   
                <div class="widget-body">
                    <div class="widget-main">
                        <div class=row>
                            <div class="col-sm-12 form-group">
                                <label class="col-xs-2">Main Area:</label>
                                <div class="col-xs-5" >
                                    <?php
                                    $area_list = pCategoryList('main_areas');

                                    $area = (isset($_REQUEST['id'])) ? $model->main_area : NULL;
                                    // dd($_REQUEST['id']);

                                    echo Html::dropDownList('posted[main_area]', $area, $area_list, [
                                        'prompt' => 'Select Main Area', 'class' => ' col-sm-12 chosen-select','id'=>'main_areas'
                                            ]
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-xs-2">Country:</label>
                                <div class="col-xs-5" id="countries">
                                    <?php
                                    $countries_list = pCategoryList('countries');

                                    $country = (isset($_REQUEST['id'])) ? $model->country : NULL;
                                    // dd($_REQUEST['id']);

                                    echo Html::dropDownList('posted[country]', $country, $countries_list, [
                                        'prompt' => 'Select Country', 'class' => ' col-sm-12 chosen-select'
                                            ]
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-xs-2">Province:</label>
                                <div class="col-xs-5">
                                    <?php
                                    $title = (isset($_REQUEST['id'])) ?
                                            decodePost($model, 'province') :
                                            NULL;
                                    echo Html::textInput('posted[province]', $title, ['class' => 'input-sm col-sm-12']
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>


 <?php
    $script = <<<EOD
      $('body').on('change','#main_areas',function(){
          var main_area_id = $('#main_areas').val();
          $.ajax({
              'type' : 'GET',
              'dataType' : 'html',
              'url' : 'ajax-requests',
              'data' : {main_areas:main_area_id},
              'success' : function(data){
                  $('#countries').html(data);
                  $('select').chosen();
            },
            'error' : function(){
                console.log('Error Occured...');
            }
        });
  });
  
        
EOD;
$this->registerJs($script, $this::POS_END)
?>