<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\hr\search\HrPersonalinfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = "Manage Distributors";
$this->params['breadcrumbs'][] = $this->title;
if (isset($this->context)) {
    $this->context->pageHeading = "CMS";
    $this->context->pageCaption = "Manage Distributors";
}
?>
<div class="hr-faq-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Create', ['distributors'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'header' => 'Name',
                'value' => function($data) {
                  return (decodePost($data,'name') != NULL) ? decodePost($data,'name'): "-";
                 },
            ],
			
			
            ['class' => 'yii\grid\ActionColumn',
				'template'=>'{update}{delete}',
				'buttons'=>[
					'update'=>function($url,$model,$key){
						$url = ['site/distributors','id'=>$model->id];
						return HTML::a('<span class="fa fa-fw fa-edit fa-lg"></span>',$url,[
							'title'=>Yii::t('yii','update Client'),
						]);
					},
					'delete'=>function($url,$model,$key){
						$url = ['site/delete','id'=>$key];
						return Html::a('<span class="fa fa-fw fa-remove text-red fa-lg"></span>',$url,[
							'title'=>Yii::t('yii','delete Client'),
                                                    'data-confirm' => Yii::t('yii', 'Are You Sure??')
						]);
					},
				],
			],
        ],
    ]); ?>
</div>