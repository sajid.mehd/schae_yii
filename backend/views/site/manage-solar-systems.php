<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\PsPosts;
/* @var $this yii\web\View */
/* @var $searchModel app\models\hr\search\HrPersonalinfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = "Manage Solar System";
$this->params['breadcrumbs'][] = $this->title;
if (isset($this->context)) {
    $this->context->pageHeading = "CMS";
    $this->context->pageCaption = "Manage Solar System";
}
?>
<div class="hr-faq-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Create', ['solar-systems'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'header' => 'Title',
                'value' => function($data) {
                  return (decodePost($data,'title') != NULL) ? decodePost($data,'title'): "-";
                 },
            ],
			 [
                'attribute' => 'detail',
                'header' => 'Detail',
                'format' =>'html',
                'value' => function($data) {
                  return (decodePost($data,'detail') != NULL) ? (decodePost($data,'detail')): "-";
                 },
            ],
			
            ['class' => 'yii\grid\ActionColumn',
				'template'=>'{update}{delete}',
				'buttons'=>[
					'update'=>function($url,$model,$key){
						$url = ['site/solar-systems','id'=>$model->id];
						return HTML::a('<span class="fa fa-fw fa-edit fa-lg"></span>',$url,[
							'title'=>Yii::t('yii','update Application'),
						]);
					},
					'delete'=>function($url,$model,$key){
						$url = ['site/delete','id'=>$key];
						return Html::a('<span class="fa fa-fw fa-remove text-red fa-lg"></span>',$url,[
							'title'=>Yii::t('yii','delete Application'),
                                                    'data-confirm' => Yii::t('yii', 'Are You Sure??')
						]);
					},
				],
			],
        ],
    ]); ?>
</div>