<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;
use app\components\EpatController;
use yii\web\View;
use backend\components\multiupload\MultiUploadWidget;
use backend\components\multiupload\MultiUploadWidgetDocument;
//use backend\models\PsPosts;
/* @var $this yii\web\View */
/* @var $model app\models\hr\HrDefallowance */
/* @var $form yii\widgets\ActiveForm */
if (isset($this->context)) {
    $this->context->pageHeading = "CMS";
    $this->context->pageCaption = "Add Downloads";
}
?>
<div class="hr-basicinfo-form">
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => 'form-horizantal form-control myform',
                    'role' => 'from',
                    'style' => 'border:none'
                ]
    ]);
    ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title">Downloads</h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            
                            <div class="col-sm-12">
                                <div class="row form-group">
                                    <label class="col-xs-2">Title:</label>
                                    <div class="col-xs-5">
                                        <?php
                                        $product = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'title') :
                                                NULL;
                                        echo Html::textInput('posted[title]', $product, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>
                                    </div> 
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-12">
                                        <?php
                                        if (isset($attachmentArrayDoc)) {
                                            echo MultiUploadWidgetDocument::widget([
                                                'form' => $form,
                                                'options' => [
                                                    'attachmentArray' => $attachmentArrayDoc
                                                ]
                                            ]);
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
