<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;
use app\components\EpatController;
use app\models\PsPosts;

/* @var $this yii\web\View */

/* @var $model app\models\hr\HrDefallowance */

/* @var $form yii\widgets\ActiveForm */





if (isset($this->context)) {

    $this->context->pageHeading = "CMS";

    $this->context->pageCaption = "Add Video Link";
}
?>

<div class="hr-basicinfo-form">
     <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title">Video Link</h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-sm-12">


                                <div class="row form-group">

                                    <label class="col-xs-2">Video title:</label>

                                    <div class="col-xs-8">
                                       
                                        <?php
                                        $product = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'title') :
                                                NULL;



                                        echo Html::textInput('posted[title]', $product, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>

                                    </div>

                                </div>
                                 <div class="row form-group">

                                    <label class="col-xs-2">Video Description:</label>

                                    <div class="col-xs-8">
                                        <?php
                                        $product = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'description') :
                                                NULL;



                                        echo Html::textInput('posted[description]', $product, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>

                                    </div>

                                </div>
                                 <div class="row form-group">

                                    <label class="col-xs-2">Video link:</label>

                                    <div class="col-xs-8">
                                        <?php
                                        $product = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'link') :
                                                NULL;



                                        echo Html::textarea('posted[link]', $product, ['class' => 'input-sm col-sm-12','placeholder'=>'Write only embed code of video here']
                                        );
                                        ?>

                                    </div>

                                </div>
                                 
                                <div class="form-group row">
                                    <div class="col-xs-10">
                                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
