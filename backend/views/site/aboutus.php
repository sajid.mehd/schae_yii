<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;
use app\components\EpatController;
use app\models\PsPosts;
use yii\web\View;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model app\models\hr\HrDefallowance */
/* @var $form yii\widgets\ActiveForm */
$this->title = "Add About Us";
$this->params['breadcrumbs'][] = $this->title;
if (isset($this->context)) {
    $this->context->pageHeading = "CMS";
    $this->context->pageCaption = "About Us";
}
?>
<div class="aboutus-form">
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => 'form-horizantal form-control myform',
                    'role' => 'from',
                    'style' => 'border:none'
                ]
    ]);
    ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title">About Us</h4>
                </div>
                   
                <div class="widget-body">
                    <div class="widget-main">
                        <div class=row>
                            <div class="col-sm-12">
                                <div class="row form-group">
                                    <label class="col-xs-2">Title:</label>
                                    <div class="col-xs-5">
                                        <?php
                                        $about = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'title') :
                                                NULL;
                                        echo Html::textInput('posted[title]', $about, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-xs-2">Details:</label>
                                    <div class="col-xs-10">
                                        <div class="wysiwyg-editor" id="editor1"></div>
                                        <?php
                                        $details = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'detail') :
                                                NULL;
                                        echo Html::hiddenInput('detail', $details, ['class' => 'col-sm-12 detail_clx']
                                        );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
