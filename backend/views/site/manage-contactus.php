<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\hr\search\HrPersonalinfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = "Manage Contact Us";
$this->params['breadcrumbs'][] = $this->title;
if (isset($this->context)) {
    $this->context->pageHeading = "CMS";
    $this->context->pageCaption = "Manage Contact Us";
}
?>
<div class="hr-faq-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Create', ['contactus'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			
            [
                'attribute' => 'address',
                'header' => 'Address',
                'value' => function($data) {
                  return (decodePost($data,'address') != NULL) ? decodePost($data,'address'): "-";
                 },
            ],
                          [
                'attribute' => 'email',
                'header' => 'Email',
                'value' => function($data) {
                  return (decodePost($data,'email') != NULL) ? decodePost($data,'email'): "-";
                 },
            ],
                          [
                'attribute' => 'cont_person1',
                'header' => 'Contact Person',
                'value' => function($data) {
                  return (decodePost($data,'cont_person1') != NULL) ? decodePost($data,'cont_person1'): "-";
                 },
            ],
                          [
                'attribute' => 'Phone no',
                'header' => 'phone1',
                'value' => function($data) {
                  return (decodePost($data,'phone1') != NULL) ? decodePost($data,'phone1'): "-";
                 },
            ],
			 /*[
                'attribute' => 'google-map',
                'header' => 'Google Map Address',
                'value' => function($data) {
                  return (decodePost($data,'google-map') != NULL) ? decodePost($data,'google-map'): "-";
                 },
            ],*/
			
            ['class' => 'yii\grid\ActionColumn',
				'template'=>'{update}{delete}',
				'buttons'=>[
					'update'=>function($url,$model,$key){
						$url = ['site/contactus','id'=>$model->id];
						return HTML::a('<span class="fa fa-fw fa-edit fa-lg"></span>',$url,[
							'title'=>Yii::t('yii','update contact'),
						]);
					},
					'delete'=>function($url,$model,$key){
						$url = ['site/delete','id'=>$key];
						return Html::a('<span class="fa fa-fw fa-remove text-red fa-lg"></span>',$url,[
							'title'=>Yii::t('yii','delete contact'),
                                                    'data-confirm' => Yii::t('yii', 'Are You Sure??')
						]);
					},
				],
			],
        ],
    ]); ?>
</div>