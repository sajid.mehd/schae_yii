<?php ?><?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;
use app\components\EpatController;
use app\models\PsPosts;
/* @var $this yii\web\View */
/* @var $model app\models\hr\HrDefallowance */
/* @var $form yii\widgets\ActiveForm */
if (isset($this->context)) {
    $this->context->pageHeading = "CMS";
    $this->context->pageCaption = "Add ContactUs";
}
?>
<div class="hr-basicinfo-form">
     <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title">Contact Us</h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row form-group">
                                    <label class="col-xs-2">Area:</label>
                                    <div class="col-xs-10">
                                        <?php
                                        $area = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'area') :
                                                NULL;
                                        echo Html::textInput('posted[area]', $area, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-xs-2">Contact Person1:</label>
                                    <div class="col-xs-4">
                                        <?php
                                        $cont_person1 = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'cont_person1') :
                                                NULL;
                                        echo Html::textInput('posted[cont_person1]', $cont_person1, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>
                                    </div>
                                    <label class="col-xs-1">Phone1:</label>
                                    <div class="col-xs-5">
                                        <?php
                                        $phone1 = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'phone1') :
                                                NULL;
                                        echo Html::textInput('posted[phone1]', $phone1, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-xs-2">Contact Person2:</label>
                                    <div class="col-xs-4">
                                        <?php
                                        $cont_person2 = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'cont_person2') :
                                                NULL;
                                        echo Html::textInput('posted[cont_person2]', $cont_person2, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>
                                    </div>
                                    <label class="col-xs-1">Phone2:</label>
                                    <div class="col-xs-5">
                                        <?php
                                        $phone2 = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'phone2') :
                                                NULL;
                                        echo Html::textInput('posted[phone2]', $phone2, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-xs-2">Email:</label>
                                    <div class="col-xs-10">
                                        <?php
                                        $email = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'email') :
                                                NULL;
                                        echo Html::textInput('posted[email]', $email, ['class' => 'input-sm col-sm-12']
                                        );
                                        ?>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <label class="col-xs-2">Address:</label>
                                    <div class="col-xs-10">
                                       
                                        <?php
                                        $address = (isset($_REQUEST['id'])) ?
                                                decodePost($model, 'address') :
                                                NULL;
                                        echo Html::textarea('posted[address]', $address, ['class' => 'input-sm col-sm-12' , 'rows'=> 4]
                                        );
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
<?php
$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';
    $script = <<<JS
        $('body').on('change', '.category_clx', function(){
            id = $(this).val();
            console.log(id);
            var reqid = '{$id}';
            $.ajax({
                url:'ajax-call',
                dataType:'html',
                data:{event_categoryid:id,id:reqid},
                type:'GET',
                success:function(data){
                    $('#subcategory').html(data);
                    $('select').chosen();
                },
                error: function(){
                    console.log('Error Occur');
                }

            });
        });
        $(function(){
            $('.category_clx').trigger('change');

        });
JS;
    $this->registerJs($script,yii\web\View::POS_END);
?>