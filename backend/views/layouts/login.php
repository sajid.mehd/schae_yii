<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */

use backend\themes;


@backend\themes\EpatThemeAsset::register($this);
@backend\assets\AppAsset::register($this);
// die();
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body class="login-layout light-login">
        <?php $this->beginBody() ?>
        <div class="main-container">
            <div class="main-content">
                <?= $content ?>
            </div><!-- /.main-content -->
        </div><!-- /.main-container -->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage(); ?>