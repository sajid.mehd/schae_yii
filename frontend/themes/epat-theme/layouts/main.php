<?php
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use frontend\themes;
frontend\assets\AppAsset::register($this);
?>
<?php
$this->beginPage();
?>
 
<html lang="en">
    <head>
        <title>ATS</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link rel="shortcut icon" href="<?= Yii::$app->request->baseUrl ?>/frontend/web/favicon.png" type="image/x-icon" /> -->
        <link rel="shortcut icon" href="<?= Yii::$app->request->baseUrl ?>/frontend/web/images/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="<?= $this->theme->baseurl?>/frontend/web/css/bootstrap.css">
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="css/font-awesome.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link rel="stylesheet" href="css/lightslider.css"/>
        <link rel="stylesheet" href="css/lightgallery.css">
        <link rel="stylesheet" href="<?php echo $this->theme->baseUrl ?>/css/style.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
    </head>
    <body>
        
        <?php $this->beginBody() ?>
        <?php echo $this->render('section/top_header.php'); ?>
        <?php echo $this->render('section/header-navigation.php'); ?>
        
        <?php  
            $action = Yii::$app->controller->action->id;
            if($action == 'index')
            {
               echo $this->render('section/main-slider.php');
            }
        ?>  
        <?php echo $content; ?>
        <?php echo $this->render('section/footer.php'); ?>  
    </div> <!-- End Container -->
	</section>

    <?php
       
        $this->registerJsFile($this->theme->baseUrl . '/js/mixitup.js', [
            'depends' => [
                'yii\web\YiiAsset',
                'yii\bootstrap\BootstrapPluginAsset'
            ], $this::POS_END
        ]);
        $this->registerJsFile($this->theme->baseUrl . '/js/lightslider.js', [
            'depends' => [
                'yii\web\YiiAsset',
                'yii\bootstrap\BootstrapPluginAsset'
            ], $this::POS_END
        ]);
        $this->registerJsFile($this->theme->baseUrl . '/js/lightgallery.js', [
            'depends' => [
                'yii\web\YiiAsset',
                'yii\bootstrap\BootstrapPluginAsset'
            ], $this::POS_END
        ]);
        $this->registerJsFile($this->theme->baseUrl . '/js/lg-fullscreen.js', [
            'depends' => [
                'yii\web\YiiAsset',
                'yii\bootstrap\BootstrapPluginAsset'
            ], $this::POS_END
        ]);
        $this->registerJsFile($this->theme->baseUrl . '/js/lg-thumbnail.js', [
            'depends' => [
                'yii\web\YiiAsset',
                'yii\bootstrap\BootstrapPluginAsset'
            ], $this::POS_END
        ]);
        $this->registerJsFile($this->theme->baseUrl . '/js/navScript.js', [
            'depends' => [
                'yii\web\YiiAsset',
                'yii\bootstrap\BootstrapPluginAsset'
            ], $this::POS_END
        ]);
        $this->registerJsFile($this->theme->baseUrl . '/js/external.js', [
            'depends' => [
                'yii\web\YiiAsset',
                'yii\bootstrap\BootstrapPluginAsset'
            ], $this::POS_END
        ]);
        // Start drop down menu ....
$script=<<<JS
    
JS;
$this->registerJs($script, $this::POS_END);
?>
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>

