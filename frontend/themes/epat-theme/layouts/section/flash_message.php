<!-- Flash Messages -->
<div class="row flash-message">
    <?php
    if (Yii::$app->session->hasFlash('success')) {
        $caption = "<strong>";
        $caption .= "<i class='ace-icon fa fa-check'></i> ";
        $caption .= "Success! ";
        $caption .= "</strong>";
        $bg = "alert alert-success";
        $flashMessage = '<small>' . Yii::$app->session->getFlash('success') . '</small>';
    } elseif (Yii::$app->session->hasFlash('error')) {
        $caption = "<strong>";
        $caption .= "<i class='ace-icon fa fa-times'></i> ";
        $caption .= "Alert! ";
        $caption .= "</strong>";
        $bg = "alert alert-danger";
        $flashMessage = '<small>' . Yii::$app->session->getFlash('error') . '</small>';
    } elseif (Yii::$app->session->hasFlash('warning')) {
        $caption = "<strong>";
        //$caption .= "<i class='ace-icon fa fa-times'></i> ";
        $caption .= "Warning! ";
        $caption .= "</strong>";
        $bg = "alert alert-warning";
        $flashMessage = '<small>' . Yii::$app->session->getFlash('warning') . '</small>';
    } elseif (Yii::$app->session->hasFlash('info')) {
        $caption = "<strong>";
        //$caption .= "<i class='ace-icon fa fa-times'></i> ";
        $caption .= "Information! ";
        $caption .= "</strong>";
        $bg = "alert alert-info";
        $flashMessage = '<small>' . Yii::$app->session->getFlash('info') . '</small>';
    }
    if (isset($flashMessage) && $flashMessage != "") {
        ?>
        <div class="col-xs-12">
            <div class="<?= $bg ?>">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                <?= $caption ?>
                <?= $flashMessage ?>  
                <br>
            </div>
        </div>

        <?php
    }
    ?>
</div>
<!-- ./ END of Flash Messages -->