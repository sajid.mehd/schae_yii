<!-- ************************************* -->
<!-- ************* Top Header ************ -->
<!-- ************************************* -->

<section class="top_header">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
                <div class="top_header_icon">
                    <i class="fa fa-map-marker"></i>
                </div>
                <div class="top_header_text">
                    <span>Plot # 69, Street # 3, Main Street # 01, Industrial Area, Sector I-10/3, Punjab, Islamabad , Zip Code 44000, Pakistan</span>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                <div class="top_header_icon">
                    <i class="fa fa-envelope-o"></i>
                </div>
                <div class="top_header_text">
                    <span style="margin-top: 12px; display: block;">info@atsengg.com</span>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="footer_social">
                    <ul>
                        <li><a target="_blank" href="https://www.facebook.com/atsengg/"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="https://twitter.com/ATSEngineering"><i class="fa fa-twitter"></i></a></li>  
                        <li><a target="_blank" href="https://plus.google.com/111079297529934752257"><i class="fa fa-google-plus"></i></a></li> 
                        <li><a target="_blank" href="https://www.linkedin.com/company/ats-engineering-sales-&-services"><i class="fa fa-linkedin"></i></a></li> 
                        <li><a target="_blank" href="https://www.youtube.com/user/ATSEngineering"><i class="fa fa-youtube"></i></a></li>
                        <li><a target="_blank" href="https://vimeo.com/user47404846"><i class="fa fa-vimeo"></i></a></li> 
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                <div class="header_contact_no pull-right">
                    <div class="top_header_icon">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="top_header_text">
                        <span><b>+92 51 4440973</b></span><br>
                        <span><b>+92 51 4441920</b></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





