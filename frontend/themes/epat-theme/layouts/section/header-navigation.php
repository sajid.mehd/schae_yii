<?php  
	use backend\models\PsPosts;
	use yii\helpers\Url;
?>
<section class="navigation">
	<div class="container menu-container clearfix" data-spy="affix" data-offset-top="197">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-12 ">
				<a href="<?= Url::to(['site/index']);?>">
					<div class="logo text-center">
						<img src="<?= $this->theme->baseUrl?>/images/logo.png">
					</div>
				</a>
			</div>
			<div class="col-lg-10 col-md-10">
		        <button class="nav_menu_toggler_icon"><span class="fa fa-bars"></span></button>
		        <nav class="manu clearfix" >
		            <ul>
		                <li><a style="font-size: 1.2em; padding:15px 0;" href="<?= Url::to(['site/index']);?>"><i class="fa fa-home"></i></a></li>
		                <li><a href="<?= Url::to(['site/profile']);?>">Profile</a></li>
		                <li><a href="<?= Url::to(['site/solar-systems']);?>">Solar Systems</a>
		                    <ul>
		                    	<?php  
		                    	$solar_systems = PsPosts::getPost('solarsystems');
		                    	foreach ($solar_systems as $key => $ssys):
	                    		$ssid = $ssys['id'];
	                    		$sstitle = $ssys['post_title'];
	                    		?>
		                        <li><a href="<?= Url::to(['site/solar-systems-detail','id'=>$ssid]);?>"><?= $sstitle; ?></a></li>
								<?php endforeach ?>
		                    </ul>
		                </li>
		                <li><a href="<?= Url::to(['site/fabrications']);?>">Fabrication(Ferrotek)</a>
		                    <ul>
		                        <li><a href="#">Fabrication</a>
		                            <ul>
		                            	<?php  
				                    	$fabrications = PsPosts::getPost('febrications');
				                    	foreach ($fabrications as $key => $fab):
			                    		$f_id = $fab['id'];
			                    		$f_title = $fab['post_title'];
			                    		?>
										<li><a href="<?= Url::to(['site/fabrication-detail','id'=>$f_id]);?>"><?= $f_title?></a></li>
										<?php endforeach; ?>
		                            </ul>
		                        </li>
		                        <li class="hidden-sm">
		                            <div class="menu_feb_img">
		                            	<img src="images/menu_fb.jpg">
		                            </div>
		                        </li>
		                        <!-- <li><a href="#">Something</a>
		                            <ul>
		                                <li><a href="#">Sub something</a></li>
		                                <li><a href="#">Sub something</a></li>
		                                <li><a href="#">Sub something</a></li>
		                                <li><a href="#">Sub something</a></li>
		                            </ul>
		                        </li> -->
		                    </ul>
		                </li>
		                <li><a href="<?= Url::to(['site/clients-testimonials']); ?>">Clients & Testimonials</a></li>
		                <li><a href="<?= Url::to(['site/quotation']);?>">Quotation</a></li>
		                <li><a href="#">Downloads</a>
		                    <ul>
		                        <li><a href="https://drive.google.com/drive/folders/0B2EUK23EAB9rOUlwbVJHcE5YdVk">Product Litrature and Data Sheets</a></li>
		                        <li><a href="<?= $this->theme->baseUrl?>/frontend/web/attachments/ats/brochure.pdf">ATS Brochure</a></li>
		                        <li><a href="<?= Url::to(['site/ats-cal']);?>">ATS Calculator</a></li>
		                    </ul>
		                </li>
		                <li><a href="#">Support</a>
		                    <ul>
		                        <li><a href="<?= Url::to(['site/become-a-reseller']); ?>">Become a Reseller</a></li>
		                        <li><a href="<?= Url::to(['site/contact']);?>">Contact Us</a></li>
		                        <li><a href="<?= Url::to(['site/faq']);?>">FAQs</a></li>
		                        <li><a href="<?= Url::to(['site/ask-for-dealership']);?>">Ask for Dealership</a></li>
		                        <!-- <li><a href="">Service Areas</a></li> -->
		                    </ul>
		                </li>
		            </ul>
		        </nav>
		    </div>
	    </div>
	</div>
</section>


