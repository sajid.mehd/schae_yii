<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="home_applications">
	<div class="container">
		<div class="row">
			<?php  
				$applications = PsPosts::getPost('applications');
				// dd($applications);
				foreach ($applications as $key => $app):
					$app_id = $app['id'];
					$app_content = decodeDetails($app['post_content']);
					$app_title = $app_content['title'];
					$app_detail = limitedWords(strip_tags($app_content['detail']),100);
					$app_attachment = AppAttachments::getAllAttachments($app_id);
                    $file_path = $app_attachment[0]['file_path'];
			?>
			<div class="col-md-4 col-sm-6">
				<div class="home_apps 
					<?php 
						if($key == 0){echo('n_metring');}
						if($key == 1){echo('offgrid');}
						if($key == 2){echo('pumping_system');}
						if($key == 3){echo('solar_fabrication');}
						if($key == 4){echo('ferrotek_fabrication');}
						if($key == 5){echo('industrial_fans');}
					?>
					" id="<?= ($key == 0 ? 'netmeter' : '')?>">
					<a href="<?= Url::to(['site/applications-detail','id'=>$app_id]);?>">
						<div class="app_icon">
							<img id="<?= ($key == 0 ? 'nmeter' : '')?>" src="<?= $this->theme->baseUrl.'/'.$file_path;?>">
						</div>
						<?php if($key == 0): ?>
						<div class="aedb">
							<img src="<?= $this->theme->baseUrl.'/images/aedb.png' ?>">
						</div>
						<?php endif; ?>
						<div class="app_text">
							<h3><?= $app_title;?></h3>
							<p>
								<?php if($key == 0){ echo(limitedWords($app_detail,48));}?>
								<?php if($key == 1){ echo(limitedWords($app_detail,30));}?>
								<?php if($key == 2){ echo(limitedWords($app_detail,30));}?>
								<?php if($key == 3){ echo(limitedWords($app_detail,30));}?>
								<?php if($key == 4){ echo(limitedWords($app_detail,30));}?>
								<?php if($key == 5){ echo(limitedWords($app_detail,30));}?>
							</p>
							<span>Read More <i class="fa fa-angle-double-right"></i></span>
						</div>
					</a>
				</div>
			</div>
			<?php endforeach; ?>			
		</div>
	</div>	
</section>