<?php  
	use backend\models\PsPosts;
	use yii\helpers\Url;
?>


<!-- ********************************************* -->
<!-- *************  Home Contact Info ************ -->
<!-- ********************************************* -->


<section class="home_contact_sec">
	<div class="container">
		<div class="row">
			<ul id="addresses">
			<?php  
				// foreach ($contacts as $key => $value):
				// 	$title = $value['post_title'];
				// 	$content = decodeDetails($value['post_content']);
				// 	$contact_person1 = $content['cont_person1'];
				// 	$phone1 = $content['phone1'];
				// 	$contact_person2 = $content['cont_person2'];
				// 	$phone2 = $content['phone2'];
				// 	$email = $content['email'];
				// 	$address = $content['address'];
				
			?>
				<li>
					<div class="home_contact_info">
						<h3>HeadOffice</h3>
						<ul>
							<li class="icons">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
								<a>Plot # 69, Street # 3, Main Street # 01, Industrial Area, Sector I‐10/3, Islamabad, Pakistan</a>
								</li>
								<div class="clearfix"></div>
								<li class="icons">
									<i class="fa fa-phone " aria-hidden="true"></i>
									<a href="tel:">+92 51 4440973</a>
								</li>
								<div class="clearfix"></div>
								<li class="icons">
									<i class="fa fa-envelope-o" aria-hidden="true"></i>
									<a href="mailto:">info@atsengg.com</a>
								</li>
								<li>.</li>
								<div class="clearfix"></div>
							</ul>
					</div>
				</li>
				<li>
					<div class="home_contact_info">
						<h3>Chakwal</h3>
						<ul>
							<li class="icons">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
								<a>Plot # 69, Street # 3, Main Street # 01, Industrial Area, Sector I‐10/3, Islamabad, Pakistan</a>
								</li>
								<div class="clearfix"></div>
								<li class="icons">
									<i class="fa fa-phone " aria-hidden="true"></i>
									<a href="tel:">+92 51 4440973</a>
								</li>
								<div class="clearfix"></div>
								<li class="icons">
									<i class="fa fa-envelope-o" aria-hidden="true"></i>
									<a href="mailto:">info@atsengg.com</a>
								</li>
								<li>.</li>
								<div class="clearfix"></div>
							</ul>
					</div>
				</li>
				<li>
					<div class="home_contact_info">
						<h3>Islamabad</h3>
						<ul>
							<li class="icons">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
								<a>Plot # 69, Street # 3, Main Street # 01, Industrial Area, Sector I‐10/3, Islamabad, Pakistan</a>
								</li>
								<div class="clearfix"></div>
								<li class="icons">
									<i class="fa fa-phone " aria-hidden="true"></i>
									<a href="tel:">+92 51 4440973</a>
								</li>
								<div class="clearfix"></div>
								<li class="icons">
									<i class="fa fa-envelope-o" aria-hidden="true"></i>
									<a href="mailto:">info@atsengg.com</a>
								</li>
								<li>.</li>
								<div class="clearfix"></div>
							</ul>
					</div>
				</li>
				<li>
					<div class="home_contact_info">
						<h3>Gilgit</h3>
						<ul>
							<li class="icons">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
								<a>Plot # 69, Street # 3, Main Street # 01, Industrial Area, Sector I‐10/3, Islamabad, Pakistan</a>
								</li>
								<div class="clearfix"></div>
								<li class="icons">
									<i class="fa fa-phone " aria-hidden="true"></i>
									<a href="tel:">+92 51 4440973</a>
								</li>
								<div class="clearfix"></div>
								<li class="icons">
									<i class="fa fa-envelope-o" aria-hidden="true"></i>
									<a href="mailto:">info@atsengg.com</a>
								</li>
								<li>.</li>
								<div class="clearfix"></div>
							</ul>
					</div>
				</li>
			<?php //endforeach ?>
		</ul>
		</div>
	</div>
</section>
 




	<!-- ********************************************* -->
	<!-- ***************  Footer Starts ************** -->
	<!-- ********************************************* -->

	<!-- ============== Footer Top =============== -->

	<section class="section footer-top">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h3>Need a Solar Solution ?</h3> 
				</div>
				<div class="col-md-8">
					<p id="call-us">Call us now at <span><a href="tel:+92514440973">+92 51 4440973</a></span>OR drop us an eMail at <span><a href="mailto:info@atsengg.com">info@atsengg.com</a></span></p>
				</div>
			</div>
		</div>
	</section>

	<!-- ================= Main Footer ================ -->

    <section class="footer_sec">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-4">
					<div class="about_info">
						<img src="images/logo.png">
						<?php  
						$about = PsPosts::getPost('about-us');
						$ab_content = decodeDetails($about[0]['post_content']);
						$ab_detail = limitedWords(strip_tags($ab_content['detail'],'<br>'),120); 
						?>
						<p><?= $ab_detail;?></p>
						<a href="<?= Url::to(['site/profile']);?>">Read More <i class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-4">
					<div class="footer_links">
						<h3>Fabricatons</h3>	
						<ul>
							<?php  
	                    	$fabrications = PsPosts::getPost('febrications');
	                    	foreach ($fabrications as $key => $fab):
                    		$f_id = $fab['id'];
                    		$f_title = $fab['post_title'];
                    		?>
							<li><a href="<?= Url::to(['site/fabrication-detail','id'=>$f_id]);?>"><?= $f_title?></a></li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>	
				<div class="col-md-3 col-sm-4">
					<div class="footer_links">
						<h3>Working Areas</h3>	
						<ul>
							<?php  
							$applications = PsPosts::getPost('applications');
							foreach ($applications as $key => $app):
							$app_id = $app['id'];
							$app_content = decodeDetails($app['post_content']);
							$app_title = $app_content['title'];
							?>
							<li><a href="<?= Url::to(['site/applications-detail','id'=>$app_id]);?>"><?= $app_title;?></a></li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<div class="col-md-3 hidden-sm hidden-xs">
					<div class="words_cloud">
						<img src="images/words-cloud.jpg">
					</div>
				</div>

			</div>
		</div>
	</section>

	<!-- ================= Opprotunities Section ================= -->

	<section class="sec_drop_cv">
		<div class="container">
			<div class="row">
				<div class="col-md-12 ">
					<div class="drop_cv">
						<div class="row">
							<div class="col-md-5 col-sm-5">
								<div class="opportunity">
									<h4>An Opprotunity <i class="fa fa-caret-right"></i></h4>
								</div>
							</div>
							<div class="col-md-7 col-sm-7">
								<div class="cv_sec">
									<p>For Jobs & Internships <a href="">Drop Your CV</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- ============== Footer Boottom =============== -->

	<section class="footer_bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-8">
					<div class="footer_main_links">
						<ul>
							<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
							<li><a href="<?= Url::to(['site/profile']);?>">Profile</a></li>
							<li><a href="<?= Url::to(['site/solar-systems']);?>">Solar Systems</a></li>
							<li><a href="<?= Url::to(['site/fabrications']);?>">Fabricatons</a></li>
							<li><a href="<?= Url::to(['site/clients-testimonials']);?>">Clients & Testimonials</a></li>
							<li><a href="<?= Url::to(['site/quotation']);?>">Quotations</a></li>
							<li><a href="<?= Url::to(['site/contact']);?>">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="footer_social">
						<ul>
							<li><a target="_blank" href="https://www.facebook.com/atsengg/"><i class="fa fa-facebook"></i></a></li>
							<li><a target="_blank" href="https://twitter.com/ATSEngineering"><i class="fa fa-twitter"></i></a></li>  
							<li><a target="_blank" href="https://plus.google.com/111079297529934752257"><i class="fa fa-google-plus"></i></a></li> 
							<li><a target="_blank" href="https://www.linkedin.com/company/ats-engineering-sales-&-services"><i class="fa fa-linkedin"></i></a></li> 
							<li><a target="_blank" href="https://www.youtube.com/user/ATSEngineering"><i class="fa fa-youtube"></i></a></li>
							<li><a target="_blank" href="https://vimeo.com/user47404846"><i class="fa fa-vimeo"></i></a></li> 
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- ********************************************* -->
	<!-- ****************  Footer Ends *************** -->
	<!-- ********************************************* -->


	<!-- ********************************************* -->
	<!-- **********  Copyright Footer Starts ********* -->
	<!-- ********************************************* -->

	<section class="copyright_sec">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="copyright">
						<p>Copyrights &copy; 2017-2018. All rights reserved by ATS Engineering</p>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="des_dev ">
						<span>by</span><a target="_blank" href="http:\\www.epatronus.com"><img src="images/epatronus.png"></a>
					</div>
				</div>

			</div>
		</div>
	</section>

	<!-- ********************************************* -->
	<!-- ***********  Copyright Footer Ends ********** -->
	<!-- ********************************************* -->