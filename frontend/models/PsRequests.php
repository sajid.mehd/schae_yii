<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "ps_requests".
 *
 * @property integer $id
 * @property string $name
 * @property string $city
 * @property integer $request_type
 * @property string $contactno
 * @property string $email
 * @property string $address
 * @property integer $interest
 * @property string $business_name
 * @property string $business_address
 * @property string $details
 */
class PsRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city', 'contactno', 'email', 'interest', 'business_name', 'details'], 'required'],
            [['request_type', 'interest'], 'integer'],
            [['details'], 'string'],
            [['name', 'city', 'contactno', 'email', 'business_name'], 'string', 'max' => 150],
            [['address', 'business_address'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'city' => 'City',
            'request_type' => 'Request Type',
            'contactno' => 'Contactno',
            'email' => 'Email',
            'address' => 'Address',
            'interest' => 'Interest',
            'business_name' => 'Business Name',
            'business_address' => 'Business Address',
            'details' => 'Details',
        ];
    }
}
