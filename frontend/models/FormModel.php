<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class FormModel extends Model {

    public $name;
    public $first_name;
    public $last_name;
    public $street;
    public $town;
    public $zip;
    public $age;
    public $country;
    public $area;
    public $gender;
    public $current_address;
    public $permenent_address;
    public $city;
    public $province;
    public $telephone;
    public $mobile;
    public $email;
    public $company;
    public $department;
    public $fax;
    public $query;
    public $company_name;
    public $company_url;
    public $captcha;
    public $subject;
    public $hidden1;
    public $business;
    public $solution;
    public $address1;
    public $address2;
    public $attach_cv;
    public $qualification;
    public $cover_note;
    public $file;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // name, email, subject and body are required
            [['area', 'gender' ,'current_address', 'city', 'province', 'mobile','captcha'], 'required'],
            ['captcha', 'captcha'],
            // email has to be a valid email address
            ['email', 'email'],
            [['name','business','address1','address2','solution','current_address', 'hidden1', 'subject','city', 'telephone','street','town','zip','country', 'company','first_name','last_name','department','query','company_name','company_url','attach_cv','qualification','file','cover_note'], 'string'],
            [['age', 'mobile', 'telephone', 'province','fax'], 'integer'],
            [['area'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'name' => 'Name',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'age' => 'Age',
            'area' => 'Section',
            'gender' => 'Gender',
            'current_address' => 'Current Address',
            'permenent_address' => 'Permenent Address',
            'city' => 'City',
            'province' => 'Province',
            'telephone' => 'Telephone no',
            'mobile' => 'Mobile no',
            'email' => 'Email',
            'company' => 'Company',
            'department' => 'Department',
            'fax' => 'Fax',
            'query'=>'Your Query',
            'company_name'=> 'Company name',
            'company_url'=>'Company url'
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email) {
        return Yii::$app->mailer->compose()
                        ->setTo($email)
                        ->setFrom([$this->email => $this->name])
                        ->setSubject($this->subject)
                        ->setTextBody($this->body)
                        ->send();
    }

}
