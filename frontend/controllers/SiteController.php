<?php 

namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\PsPosts;
use yii\web\Session;
use yii\helpers\Json;
use frontend\models\FormModel;
use backend\models\search\MembersSearch;
use backend\models\TempShowResultWinter2016;
use backend\models\search\TempShowResultWinter2016Search;
use backend\models\TempShowResultSummer2017;
use backend\models\search\TempShowResultSummer2017Search;
use backend\models\TempShowResultWinter2017;
use backend\models\search\TempShowResultWinter2017Search;
use backend\models\ProtectedPagesUsers;
use yii\db\Query;
use backend\models\AppAttachments;
use yii\web\UploadedFile;

/**
 * Site controller 
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
        'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout', 'signup'],
        'rules' => [
        [
        'actions' => ['signup'],
        'allow' => true,
        'roles' => ['?'],
        ],
        [
        'actions' => ['logout'],
        'allow' => true,
        'roles' => ['@'],
        ],
        ],
        ],
        'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
        'logout' => ['post'],
        ],
        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
        'error' => [
        'class' => 'yii\web\ErrorAction',
        ],
        'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
                // 'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
        'fixedVerifyCode' => null,
        ],
        ];
    }

    public function getVerifyCode($regenerate = false)
    {
        if ($this->fixedVerifyCode !== null) {
            return $this->fixedVerifyCode;
        }

        $session = Yii::$app->getSession();
        $session->open();
        $name = $this->getSessionKey();
        if ($session[$name] === null || $regenerate) {
            $session[$name] = $this->generateVerifyCode();
            $session[$name . 'count'] = 1;
        }

        return $session[$name];
    }
    
    public function actionAtsCal() {
        return $this->render('ats_cal');
    }
    

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
                ]);
        } 
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionIndex(){
        $model = new FormModel();
        if ($model->load(Yii::$app->request->post())) {
            // dd($_REQUEST);
            $hiddenVal = $_REQUEST['FormModel']['hidden1'];
            // dd($hiddenVal);
            if ($hiddenVal == 'home_quote') {
                $sender_email = $model->email;
                $data = $this->renderPartial('partials/quote_home_output', [
                    'model' => $model,
                    'formData' => $_REQUEST], true);
                // pre($data);
                // die();
                // $auto_reply = $this->renderPartial('_contact_form_receiver');

                $message = Yii::$app->mailer->compose(); 
                $message->setFrom($model->email)
                ->setTo('info@atsengg.com')
                ->setSubject('ATS Engineering - Get a Quote')
                ->setHtmlBody($data)
                ->send();

                $message = Yii::$app->mailer->compose();
                $message->setFrom('info@atsengg.com')
                ->setTo($model->email)
                ->setSubject('Re:ATS Engineering - Get a Quote')
                ->setHtmlBody("We Will Contact You Soon")
                ->send();
            }
            if ($hiddenVal == 'home_net_mtr') {
                $sender_email = $model->email;
                $data = $this->renderPartial('partials/apply-for-netmetring', [
                    'model' => $model,
                    'formData' => $_REQUEST], true);
                // pre($data);
                // die();
                // $auto_reply = $this->renderPartial('_contact_form_receiver');

                $message = Yii::$app->mailer->compose(); 
                $message->setFrom($model->email)
                ->setTo('info@atsengg.com')
                ->setSubject('ATS Engineering - Apply For Net Metring')
                ->setHtmlBody($data)
                ->send();

                $message = Yii::$app->mailer->compose();
                $message->setFrom('info@atsengg.com')
                ->setTo($model->email)
                ->setSubject('Re:ATS Engineering - Apply For Net Metring')
                ->setHtmlBody("We Will Contact You Soon")
                ->send();
            }
            if ($hiddenVal == 'home_contact') {
                $sender_email = $model->email;
                $data = $this->renderPartial('partials/contact-home', [
                    'model' => $model,
                    'formData' => $_REQUEST], true);
                // pre($data);
                // die();
                // $auto_reply = $this->renderPartial('_contact_form_receiver');

                $message = Yii::$app->mailer->compose(); 
                $message->setFrom($model->email)
                ->setTo('info@atsengg.com')
                ->setSubject('ATS Engineering - Contact Us')
                ->setHtmlBody($data)
                ->send();

                $message = Yii::$app->mailer->compose();
                $message->setFrom('info@atsengg.com')
                ->setTo($model->email)
                ->setSubject('Re:ATS Engineering - Contact Us')
                ->setHtmlBody("We Will Contact You Soon")
                ->send();
            }
            
            Yii::$app->session->setFlash('success', 'Congratulations! Your message has been successfully Sent.');
        }
        
        return $this->render('index', ['model' => new FormModel(),]);
    }

    public function actionProfile() {
        return $this->render('profile');
    }

    public function actionSolarSystems() {
        return $this->render('solar-systems');
    }

    public function actionFabrications() {
        return $this->render('fabrications');
    }
    
    // public function actionSolarSystemsDetail() {
    //     return $this->render('solar-systems-detail');
    // }
    public function actionSolarSystemsDetail($id = NULL) {
        if (isset($id) && $id != NULL) {
            $solarsystem = PsPosts::getOnePost($id);
        }
        return $this->render('solar-systems-detail', ['solarsystem' => $solarsystem]); 
    }

    public function actionClientsTestimonials() {
        return $this->render('clients-testimonials');
    }

    public function actionBecomeAReseller() {
        $model = new FormModel();
        if ($model->load(Yii::$app->request->post())) {
            $sender_email = $model->email;
            // dd($_REQUEST);
            $data = $this->renderPartial('partials/quotation-request', [
                'model' => $model,
                'formData' => $_REQUEST], true);
            // pre($data);
            // die();
            // $auto_reply = $this->renderPartial('_contact_form_receiver');

            $message = Yii::$app->mailer->compose(); 
            $message->setFrom($model->email)
            ->setTo('info@atsengg.com')
            ->setSubject('ATS Engineering - Become A Reseller')
            ->setHtmlBody($data)
            ->send();

            $message = Yii::$app->mailer->compose();
            $message->setFrom('info@atsengg.com')
            ->setTo($model->email)
            ->setSubject('Re:ATS Engineering - Become A Reseller')
            ->setHtmlBody("We Will Contact You Soon")
            ->send();
            Yii::$app->session->setFlash('success', 'Congratulations! Your message has been successfully Sent.');
        }
        return $this->render('become-a-reseller', ['model' => new FormModel(),]);
    }

    public function actionQuotation() {
        $model = new FormModel();
        if ($model->load(Yii::$app->request->post())) {
            $sender_email = $model->email;
            // dd($_REQUEST);
            $data = $this->renderPartial('partials/quotation-request', [
                'model' => $model,
                'formData' => $_REQUEST], true);
            // pre($data);
            // die();
            // $auto_reply = $this->renderPartial('_contact_form_receiver');

            $message = Yii::$app->mailer->compose(); 
            $message->setFrom($model->email)
            ->setTo('info@atsengg.com')
            ->setSubject('ATS Engineering - Request For Quotation')
            ->setHtmlBody($data)
            ->send();

            $message = Yii::$app->mailer->compose();
            $message->setFrom('info@atsengg.com')
            ->setTo($model->email)
            ->setSubject('Re:ATS Engineering - Request For Quotation')
            ->setHtmlBody("We Will Contact You Soon")
            ->send();
            Yii::$app->session->setFlash('success', 'Congratulations! Your message has been successfully Sent.');
        }
        return $this->render('quotation', ['model' => new FormModel(),]);
    }

    public function actionContact() {
        $model = new FormModel();
        if ($model->load(Yii::$app->request->post())) {
            
            $data = $this->renderPartial('contactus-page', [
                'model' => $model,
                'formData' => $_REQUEST], true);

            // $auto_reply = $this->renderPartial('_contact_form_receiver');
            $email_from = $_REQUEST['FormModel']['email'];
            $message = Yii::$app->mailer->compose();
            $message->setFrom($model->email)
            ->setTo('info@atsengg.com')
            ->setSubject('ATS  Engineering- Contact us Query')
            ->setHtmlBody($data)
            ->send();

            $message = Yii::$app->mailer->compose();
            $message->setFrom('info@atsengg.com')
            ->setTo($model->email)
            ->setSubject('RE : ATS Engineering - Contact us Query')
            ->setHtmlBody("we will contact you soon")
            ->send();
            
            Yii::$app->session->setFlash('success', 'Congratulations! Your message has been successfully Sent.');
        }
        
        return $this->render('contact', ['model' => new FormModel()]);
    }

    public function actionFaq() {
        return $this->render('faq');
    }

    public function actionAskForDealership() {
        $model = new FormModel();
        if ($model->load(Yii::$app->request->post())) {
            $sender_email = $model->email;
            // dd($_REQUEST);
            $data = $this->renderPartial('partials/quotation-request', [
                'model' => $model,
                'formData' => $_REQUEST], true);
            // pre($data);
            // die();
            // $auto_reply = $this->renderPartial('_contact_form_receiver');

            $message = Yii::$app->mailer->compose(); 
            $message->setFrom($model->email)
            ->setTo('info@atsengg.com')
            ->setSubject('ATS Engineering - Ask For Dealership')
            ->setHtmlBody($data)
            ->send();

            $message = Yii::$app->mailer->compose();
            $message->setFrom('info@atsengg.com')
            ->setTo($model->email)
            ->setSubject('Re:ATS Engineering - Ask For Dealership')
            ->setHtmlBody("We Will Contact You Soon")
            ->send();
            Yii::$app->session->setFlash('success', 'Congratulations! Your message has been successfully Sent.');
        }
        return $this->render('ask-for-dealership', ['model' => new FormModel()]);
    }

    public function actionDropYourCv() {
        $model = new FormModel();
        if ($model->load(Yii::$app->request->post())) {
            $model->attach_cv = UploadedFile::getInstance($model,'attach_cv');
                if($model->attach_cv){
                    $model->attach_cv->saveAs('attachments/cv/'.$model->attach_cv->baseName.'.'.$model->attach_cv->extension);
                    $model->attach_cv = 'attachments/cv/'.$model->attach_cv->baseName.'.'.$model->attach_cv->extension;
                }
               
            $sender_email = $model->email;
            // dd($_REQUEST);
            $data = $this->renderPartial('partials/drop-your-cv', [
                'model' => $model,
                'formData' => $_REQUEST], true);
            // pre($data);
            // die();
            // $auto_reply = $this->renderPartial('_contact_form_receiver');

            $message = Yii::$app->mailer->compose(); 
            $message->setFrom($model->email)
            ->setTo('info@atsengg.com')
            ->setSubject('ATS Engineering - Drop Your CV')
            ->attach($model->attach_cv)
            ->setHtmlBody($data)
            ->send();

            $message = Yii::$app->mailer->compose();
            $message->setFrom('info@atsengg.com')
            ->setTo($model->email)
            ->setSubject('Re:ATS Engineering - Drop Your CV')
            ->attach($model->attach_cv)
            ->setHtmlBody("We Will Contact You Soon")
            ->send();
            Yii::$app->session->setFlash('success', 'Congratulations! Your message has been successfully Sent.');
        }
        return $this->render('drop-your-cv', ['model' => new FormModel()]);
    }

// =======================================================================







    public function actionAbout() {
        return $this->render('about');
    }

    public function actionNews() {
        return $this->render('news');
    }

    public function actionNewsDetail($id = NULL) {
        if (isset($id) && $id != NULL) {
            $news_detail = PsPosts::getOnePost($id);
        }
        return $this->render('news-detail', ['newsdetail' => $news_detail]); 
    }

    public function actionFabricationDetail($id = NULL) {
        if (isset($id) && $id != NULL) {
            $fabrication_detail = PsPosts::getOnePost($id);
        }
        return $this->render('fabrication-detail', ['fabricationdetail' => $fabrication_detail]); 
    }

    public function actionProjects($proj_id = NULL){
        // dd($proj_id);
        return $this->render('projects', ['proj_id'=>$proj_id]);
    }
    public function actionProjectsDetail($id = NULL){
        // dd($_REQUEST);
        if(isset($id) && $id != NULL){
            $project = PsPosts::getOnePost($id);
            $proj_id = $project['main_area'];
        }
        return $this->render('projects-detail', ['project' => $project,'proj_id'=>$proj_id]);
    }

    public function actionApplications(){
        return $this->render('applications');
    }
    public function actionApplicationsDetail($id = NULL){
        if(isset($id) && $id != NULL){
            $applications = PsPosts::getOnePost($id);
            // dd($applications);
        }
        return $this->render('applications-detail', ['applications' => $applications]);
    }

    public function actionProducts() {
        return $this->render('products');
    }

    public function actionProductsDetail($id = NULL) {
        // dd($id);
        if (isset($id) && $id != NULL) {
            $product = PsPosts::getOnePost($id);
            // pre($product);
            // die();
        }
        return $this->render('products-detail', ['product' => $product]); //products-detail
    }

    public function actionDownloads($id = NULL){
        // dd($id);
        return $this->render('downloads',['id'=>$id]);
    }


    

// ============================================================= //
// ======================= Ajax Calls   ======================== //
// ============================================================= //

    public function actionAjaxCall(){

        // ================  Pdojects Conditions Start ================ //

        if(isset($_REQUEST['data_id']) && $_REQUEST['data_id']!=""){
            
            $data_id = $_REQUEST['data_id'];
            $model = PsPosts::find()
               ->select('id,post_content,post_title, post_type, city, recordstatus')
               ->where('post_type=:p_type AND city=:cty AND recordstatus=:rec',[
                ':p_type' => 'projects',
                ':cty' => $data_id,
                ':rec' =>1
                ])
               ->asArray()
               ->all();
            $html = $this->renderPartial('partials/proj_city_wise',['model' => $model, 'data_id' => $data_id ],true);
        }


        else if(isset($_REQUEST['n_w_wide_id']) && $_REQUEST['n_w_wide_id']!=""){
            
            $n_w_wide_id = $_REQUEST['n_w_wide_id'];
            // dd($n_w_wide_id);
            $model = PsPosts::find()
               ->select('id,post_content,post_title, post_type, main_area, recordstatus')
               ->where('post_type=:p_type AND main_area=:marea AND recordstatus=:rec',[
                ':p_type' => 'projects',
                ':marea' => $n_w_wide_id,
                ':rec' =>1
                ])
               ->asArray()
               ->all();
            $html = $this->renderPartial('partials/proj_country_wise',['model' => $model, 'data_id' => $data_id ],true);
        }

        else if(isset($_REQUEST['country_id']) && $_REQUEST['country_id']!=""){
            
            $country_id = $_REQUEST['country_id'];
            // dd($country_id);
            $model = PsPosts::find()
               ->select('id,post_content,post_title, post_type, main_area, recordstatus')
               ->where('post_type=:p_type AND country=:count AND recordstatus=:rec',[
                ':p_type' => 'projects',
                ':count' => $country_id,
                ':rec' =>1
                ])
               ->asArray()
               ->all();
            $html = $this->renderPartial('partials/proj_country_wise',['model' => $model, 'data_id' => $data_id ],true);
        }
        else if(isset($_REQUEST['fabr_id']) && $_REQUEST['fabr_id']!=""){
            
            $fabr_id = $_REQUEST['fabr_id'];
            // dd($fabr_id);
            $model = PsPosts::find()
               ->select('id,post_content,post_title, post_type, recordstatus')
               ->where('id=:id AND post_type=:p_type AND recordstatus=:rec',[
                ':p_type' => 'febrications',
                ':id' => $fabr_id,
                ':rec' =>1
                ])
               ->asArray()
               ->one();
            // dd($model);
            $html = $this->renderPartial('partials/febr_detail',['model' => $model],true);
        }


        else if(isset($_REQUEST['proj_id']) && $_REQUEST['proj_id']!=""){
            
            $proj_id = $_REQUEST['proj_id'];
            // dd($proj_id);
            $model = PsPosts::getOnePost($proj_id);
            $html = $this->renderPartial('partials/projects_detail',['model' => $model],true);
        }

        else if(isset($_REQUEST['faq_id']) && $_REQUEST['faq_id']!=""){
            
            $faq_id = $_REQUEST['faq_id'];
            // dd($faq_id);
            $model = PsPosts::getOnePost($faq_id);
            // dd($model);
            $html = $this->renderPartial('partials/faq_ajax',['model' => $model],true);
        }

        // ================  Pdojects Conditions End ================ //


        // ================  Downloads Start ================ //

        else if(isset($_REQUEST['download_id']) && $_REQUEST['download_id']!=""){
            
            $download_id = $_REQUEST['download_id'];
            // dd($download_id);
            $model = AppAttachments::getAllDocuments($download_id);
            $html = $this->renderPartial('partials/part_downloads',['model' => $model, 'data_id' => $data_id ],true);
        }


        echo $html;
    }
}
