<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());

//$baseUrl = '@web/frontend/../..';




return [
    'id' => 'app-frontend',
    'name' => 'ICPAP :: ',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
     
    'components' => [
        'homeUrl' => '/',
         'request' => [
            'baseUrl' => $baseUrl,
        ],
		 'view' => [
	        'theme' => [
	            'pathMap' => ['@app/views' => '@app/themes/epat-theme'],
	             'baseUrl' => $baseUrl,
	           //  'baseUrl' => '@frontend/web',
	        ],
	    ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
             'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            // Disable index.php
            'showScriptName' => false,
            
            'rules' => array(
                    '' => 'site/index',
                    '<action>' => 'site/<action>',
                    '<controller:\w+>/<id:\d+>' => '<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.icpap.com.pk',
                'username' => 'account2sendemailviaweb@icpap.com.pk',
                'password' => '!60p7RwIom31',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
    ],
    'params' => $params,
];
