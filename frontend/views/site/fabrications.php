<?php  
	use backend\models\PsPosts;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Fabrications
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Fabrications</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>


<section class="solar_systems inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Fabrications</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<?php  
				$solar_systems = PsPosts::getPost('febrications');
				foreach ($solar_systems as $key => $ssys):
					$ssid = $ssys['id'];
					$sscontent = decodeDetails($ssys['post_content']);
					$sstitle = $sscontent['title'];
					$ssdetail = limitedWords(strip_tags($sscontent['detail'],'<br>'),180);
				// dd($solar_systems);
			?>
			<div class="col-md-4">
				<a href="<?= Url::to(['site/fabrication-detail','id'=>$ssid]);?>">
					<div class="solar_system_box">
						<h3>
							<?= $sstitle;?>
						</h3>
						<p><?= $ssdetail; ?></p>
						<span>Read More <i class="fa fa-angle-double-right"></i></span>
					</div>
				</a>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

