<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						FAQ's
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>FAQ's</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<section class="faqs inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>FAQ's</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php include('partials/support_sidebar.php'); ?>
			</div>
			<div class="col-md-9 col-sm-9">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

					<?php
					$faq = PsPosts::getPost('faq');

					foreach ($faq as $key=> $faqs):
						$faqcontent=  decodeDetails($faqs['post_content']);

						$ques=  isset($faqcontent['question'])?$faqcontent['question']:'';
						$ans=  isset($faqcontent['answer'])?$faqcontent['answer']:'';

						?>    

						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne<?=$key?>">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?=$key?>" aria-expanded="true" aria-controls="collapseOne<?=$key?>">
										<?=$ques?>
									</a>
								</h4>
							</div>
							<div id="collapseOne<?=$key?>" class="panel-collapse collapse<?=($key==0)?'in':''?>" role="tabpanel" aria-labelledby="headingOne<?=$key?>">
								<div class="panel-body">
									<?=$ans?>
								</div>
							</div>

						</div>
					<?php endforeach; ?>

				</div>
			</div>
		</div>
	</div>
</section>