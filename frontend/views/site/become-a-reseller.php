<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use yii\helpers\Html;
?>


<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Become a Reseller
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Become a Reseller</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<section class="become_a_reseller inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Become a Reseller</h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<?php include('partials/support_sidebar.php'); ?>
			</div>

			<div class="col-md-9"> 
				<div class="reseller_form quote">
					<?php $form = ActiveForm::begin(); ?>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-user"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'name')->textInput(['placeholder' => 'Full Name'])->label(false); ?>
									</div>							
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-map-marker"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'city')->textInput(['placeholder' => 'City'])->label(false); ?>
									</div>							
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-phone"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'telephone')->textInput(['placeholder' => 'Contact No'])->label(false); ?>
									</div>							
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-envelope"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false); ?>
									</div>							
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-line-chart"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'business')->textInput(['placeholder' => 'Business Name'])->label(false); ?>
									</div>							
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-lightbulb-o"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'solution[]')->dropDownList(
							            ['a' => 'Solution One', 'b' => 'Solution Two', 'c' => 'Solution Three']
									    )->label(false); ?>
									</div>							
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-map-marker"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'address1')->textarea(['placeholder' => 'Home Address'])->label(false); ?>
									</div>							
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-map-marker"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'address2')->textarea(['placeholder' => 'Business Address'])->label(false); ?>
									</div>							
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-commenting-o"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'query')->textarea(['placeholder' => 'Description'])->label(false); ?>
									</div>							
								</div>
							</div>
						</div>


						<div class="row">
							<div class="col-md-12">
								<?= Html::submitInput('Submit',['class'=>'form-controll']) ?>
							</div>
						</div>

					<?php ActiveForm::end();?>
				</div>
			</div>
		</div>
	</div>
</section>