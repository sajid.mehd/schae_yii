<?php  
	use backend\models\PsPosts;
	use yii\helpers\Url;
	use backend\models\AppAttachments;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-7">
				<div class="top_sec_heading">
					<h1>Downloads</h1>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-5">
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li> / Downloads</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Downloads</h1>
				</div>
			</div>
			<div class="col-md-3">
				<div class="downloads_sidebar">
					<ul>
						<?php  
							$download_links	= PsPosts::getPost('downloads');
							// dd($download_links);
							foreach ($download_links as $key => $value):
							$down_id = $value['id'];
							$title = $value['post_title'];
						?>
						<li><a href="javascript:void(0)" data-id="<?=$down_id?>" class="link_class <?= ($down_id == $id ? 'down_active' : ''); ?>"><?= $title; ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
			<div class="col-md-9" id="downloads">
				<div class="downloads_list">
					<ul>
						<?php  

							$downloads = AppAttachments::getAllDocuments($id);
							// dd($downloads);
							foreach ($downloads as $key => $down):
								$file_name = $down['file_name'];
								$file_path = $down['file_path'];
						?>
						<li>
							<a target="_blank" href="<?= $this->theme->baseUrl.'/'.$file_path?>">
								<?= $file_name; ?><span><img src="images/download.png"></span>
							</a>
						</li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
	// $img_url = $this->theme->baseUrl.'/frontend/web/images/loader.gif';
	// $img = '<img src="'.$img_url.'" alt="Loading...." />';
	$url = Url::to(['site/ajax-call']);
    $script = <<<JS
        $('body').on('click', '.link_class', function(){
            id = $(this).attr('data-id');
            $('ul li a.link_class').removeClass('down_active');
			$(this).addClass('down_active');
            $.ajax({
                url:'{$url}',
                dataType:'html',
                data:{download_id:id},
                type:'GET',
                success:function(data){
                    $('#downloads').html(data);

                },
                error: function(){
                    console.log('Error Occur');
                }

            });
        });

        

JS;
    $this->registerJs($script,yii\web\View::POS_END);
?>