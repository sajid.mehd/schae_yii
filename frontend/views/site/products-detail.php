<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Products
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Products</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<!-- ********************************************* -->
<!-- ***************  Our Projects *************** -->
<!-- ********************************************* -->

<?php
	$id = $product['id'];
	$content = decodeDetails($product['post_content']);
	$detail = $content['detail'];
	$imgs = AppAttachments::getAllAttachments($id);
	$file_path = $imgs[0]['file_path'];
?>
<section class="inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="main_heading">
					<h1><?= $product['post_title']?></h1>
				</div>
			</div>
			
			<div class="col-md-8">
				<div class="project_description">
					<?= $detail; ?>
				</div>
			</div>
			<div class="col-md-4">
				<div class="prod_main_img">
					<figure class="imghvr-zoom-in"><img src="<?= $this->theme->baseUrl.'/'.$file_path?>">
			            <figcaption></figcaption>
			        </figure>
				</div>
				<?php
					$docs = AppAttachments::getAllDocuments($id);
					 if (isset($docs) && $docs != NULL): 
				?>
				<div class="download_files">
					<h3>Supporting Documents</h3>
					<ul>
						<?php  
							foreach ($docs as $key => $value):
							$file_name = $value['file_name'];
							$file_path = $value['file_path'];
						?>

						<li>
							<a href="<?= $this->theme->baseUrl.'/'.$file_path?>"><?= $file_name; ?> 
								<span class="down_icon"><i class="fa fa-download"></i></span>
							</a>
						</li>
						<?php endforeach ?>
					</ul>
				</div>
				<?php endif ?>
				
			</div>
		</div>
	</div>
</section>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="main_heading">
				<h1>Gallery</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul id="lightgallery" class="list-unstyled row">
				<?php  
				$single_news = PsPosts::getOnePost($_REQUEST['id']);
				$n_id = $single_news['id'];
				$news_img = AppAttachments::getAllAttachments($n_id);
				foreach ($news_img as $key => $value):
					$file_path = $value['file_path'];
					?>
					<li class="col-xs-6 col-sm-4 col-md-3 no_padding"  data-src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>" data-sub-html="">
						<a href="">
							<figure class="imghvr-zoom-in"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
					            <figcaption>
					            	<i class="fa fa-search"></i>
					            </figcaption>
					        </figure>
					    </a>
					</li>
				<?php endforeach ?>
			</ul>
		</div>
	</div>
</div>