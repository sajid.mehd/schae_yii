<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Projects
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Projects</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<!-- ********************************************* -->
<!-- ***************  Our Projects *************** -->
<!-- ********************************************* -->
<?php  
	// dd($project);
	$id = $project['id'];
	$content = decodeDetails($project['post_content']);
	// dd($content);
	$details = $content['detail'];
	$title = $content['title'];
	$imgs = AppAttachments::getAllAttachments($id);
	$file_path = $imgs[0]['file_path'];
?>
<section class=" inner_page">
	<div class="container">
			<div class="col-md-10 col-sm-10">
				<div class="main_heading">
					<h1><?= $title; ?></h1>
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="readmore">
					<a href="<?= Url::to(['site/projects']);?>">Back</a>
				</div>
			</div>
			<div class="col-md-12">
				<div class="desc">
					<div class="main_img">
						<img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
					</div>
					<h3></h3>
					<p><?= $details; ?></p>
				</div>
			</div>
		</div>
		
	</div>
</section>

			


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="main_heading">
				<h1>Related Images</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul id="lightgallery" class="list-unstyled row">
				<?php 				
				$all_imgs = AppAttachments::getAllAttachments($id);
				foreach ($all_imgs as $key => $value):
					$file_path = $value['file_path'];
					?>
					<li class="col-xs-6 col-sm-4 col-md-3 no_padding" data-src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>" data-sub-html="">
						<a href="">
							<figure class="imghvr-zoom-in"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
					            <figcaption>
					            	<i class="fa fa-search"></i>
					            </figcaption>
					        </figure>
					    </a>
					</li>
					<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>


<?php 
	// $img_url = $this->theme->baseUrl.'/frontend/web/images/loader.gif';
	// $img = '<img src="'.$img_url.'" alt="Loading...." />';
	$url = Url::to(['site/ajax-call']);
    $script = <<<JS
        $('body').on('click', '.link_class', function(){
            id = $(this).attr('data-id');

            $.ajax({
                url:'{$url}',
                dataType:'html',
                data:{data_id:id},
                type:'GET',
                success:function(data){
                	// console.log(data);
                    $('#all_cities').html(data);
                    // $('select').chosen();
                },
                error: function(){
                    console.log('Error Occur');
                }

            });
        });
         $('body').on('click', '.nation_world_wide', function(){
            id = $(this).attr('data-id');

            $.ajax({
                url:'{$url}',
                dataType:'html',
                data:{n_w_wide_id:id},
                type:'GET',
                success:function(data){
                	// console.log(data);
                    $('#all_cities').html(data);
                    // $('select').chosen();
                },
                error: function(){
                    console.log('Error Occur');
                }

            });
        });

        $('body').on('click', '.count_wise', function(){
            id = $(this).attr('data-id');
			// alert('hello');
            $.ajax({
                url:'{$url}',
                dataType:'html',
                data:{country_id:id},
                type:'GET',
                success:function(data){
                	// console.log(data);
                    $('#all_cities').html(data);
                    // $('select').chosen();
                },
                error: function(){
                    console.log('Error Occur');
                }

            });
        });

JS;
    $this->registerJs($script,yii\web\View::POS_END);
?>
