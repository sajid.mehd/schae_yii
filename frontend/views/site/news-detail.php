<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						News & Events
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>News & Events</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<section class="inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>News & Events</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<?php  
				$single_news = PsPosts::getOnePost($_REQUEST['id']);
				$n_id = $single_news['id'];
				$n_title = $single_news['post_title'];

				$date =$single_news['event_date'];
				$time = strtotime($date);
				$news_date_format = date('d M Y',$time);
				
				$content = decodeDetails($single_news['post_content']);
				$n_detail = $content['detail'];
				$news_img = AppAttachments::getAllAttachments($n_id);
				$file_path = $news_img[0]['file_path'];
				// dd($news_img);
			?>
			<div class="col-md-8 col-sm-8">
				<div class="page_heading">
					<h2 style="color:#ffba00;"><?= $n_title; ?></h2>
				</div>
				<span class="date"><?= $news_date_format; ?></span>
				<div class="news_page_img">
					<img src="<?= $this->theme->baseUrl.'/'.$file_path?>"> 
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="other_news">
					<h3>Other News</h3>
					<ul>
						<?php  
							$news_events = PsPosts::getPostWithOrderLimit('news_events',10,'DESC');
							
							foreach ($news_events as $key => $value):
								$id = $value['id'];
								$title = $value['post_title'];
							
						?>
						<li><a href="<?= Url::to(['site/news-detail','id'=>$id]);?>"><?= $title; ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				
				<p><?= $n_detail;?></p>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="main_heading">
				<h1>Gallery</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul id="lightgallery" class="list-unstyled row">
				<?php  
				$single_news = PsPosts::getOnePost($_REQUEST['id']);
				$n_id = $single_news['id'];
				$news_img = AppAttachments::getAllAttachments($n_id);
				foreach ($news_img as $key => $value):
					$file_path = $value['file_path'];
					?>
					<li class="col-xs-6 col-sm-4 col-md-3 no_padding" data-responsive="i<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?> 375, <?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?> 480, <?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?> 800" data-src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>" data-sub-html="">
						<a href="">
							<figure class="imghvr-zoom-in"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
					            <figcaption>
					            	<i class="fa fa-search"></i>
					            </figcaption>
					        </figure>
					    </a>
					</li>
				<?php endforeach ?>
			</ul>
		</div>
	</div>
</div>