<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Solar Systems
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Solar Systems</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<?php  
	// dd($solarsystem);
	$id = $solarsystem['id'];
	$content = decodeDetails($solarsystem['post_content']);
	$details = $content['detail'];
	$imgs = AppAttachments::getAllAttachments($id);
	$file_path = $imgs[0]['file_path'];
?>
<section class="solar_systems inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1><?= $solarsystem['post_title']?></h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="solar_system_sidebar">
					<ul>
						<?php  
							$solar_systems = PsPosts::getPost('solarsystems');
							foreach ($solar_systems as $key => $ssys):
								$ssid = $ssys['id'];
								$sstitle = $ssys['post_title'];
						?>
						<li><a href="<?= Url::to(['site/solar-systems-detail','id'=>$ssid]);?>"><?= $sstitle; ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>

			<div class="col-md-9">
				<div class="desc">
					<div class="main_img">
						<img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
					</div>
					<p><?= $details; ?></p>
				</div>

				<!-- <div class="row">
					<div class="col-md-12">
						<div class="doc_attachment">
							<ul>
								<li>
									<a href="">Document Attachment One 
										<span><i class="fa fa-download"></i></span>
									</a>
								</li>
								<li>
									<a href="">Document Attachment One 
										<span><i class="fa fa-download"></i></span>
									</a>
								</li>
								<li>
									<a href="">Document Attachment One 
										<span><i class="fa fa-download"></i></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div> -->
			</div>
		</div>
		
	</div>
</section>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="main_heading">
				<h1>Related Images</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul id="lightgallery" class="list-unstyled row">
				<?php 				
				$all_imgs = AppAttachments::getAllAttachments($id);
				foreach ($all_imgs as $key => $value):
					$file_path = $value['file_path'];
					?>
					<li class="col-xs-6 col-sm-4 col-md-3 no_padding" data-src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>" data-sub-html="">
						<a href="">
							<figure class="imghvr-zoom-in"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
					            <figcaption>
					            	<i class="fa fa-search"></i>
					            </figcaption>
					        </figure>
					    </a>
					</li>
					<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>