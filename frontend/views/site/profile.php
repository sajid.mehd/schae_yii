<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Profile
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Profile</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<section class="faqs inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>About ATS</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="about_us">
					<p>Established in 2005, ATS Engineering is a Sales & Services Company currently in the business of Solar Energy Solutions, Fabrication, Industrial Fans & Water Heating Solutions all at domestic, commercial & industrial level. ATS is an ISO 9001:2008 certified company.</p>

					<p>ATS purely focuses on selling the highest quality products & best after sales services. Fair and ethical business is hallmark of our corporate strategy. Customers’ confidence earned with hard efforts of last many years is our most prestigious asset and we always go an extra mile to protect this asset. These combinations are making ATS as trusted and reliable Sales & Services Company nationwide in its areas of business.</p>

					<p>ATS’s team has the passion to make Pakistan a prosperous nation using modern Alternate Energy Solutions and protect the environment by putting in our part to reduce pollution. We strongly believe that our country and society can become a great contributor to the knowledge-based economy of the 21st century by leveraging the talent and intellect of the young generation.</p>
				</div>
			</div>
			<!-- <div class="col-md-12">
				<div class="main_heading">
					<h1>Corporate Mission</h1>
				</div>
			</div>
			<div class="corporate_mission">
				<p>With our unanimous efforts in precision engineering through our professional services, and by closely observing our production line, We, ATS Engieering, are devoted to provide you all kinds of fabrication solutions and reliable products</p>
			</div> -->
		</div>
	</div>
</section>

<section class="vision_mission_philopshy">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 no_padding">
				<div class="mission">
					<div class="row">
						<div class="col-md-1">
							<div class="landing-icon">
								<i style="color:#FFBA00;" class="fa fa-flag-o" aria-hidden="true"></i>
							</div>
						</div>

						<div class="col-md-10">
							<div class="mission-inner-text">
								<h3 style="color: #FFBA00;">Corporate Mission</h3>
								<p>With our unanimous efforts in precision engineering through our professional services, and by closely observing our production line, We, ATS Engieering, are devoted to provide you all kinds of fabrication solutions and reliable products</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="history">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>History</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="history-highlights">
					<div class="hist-inner">
						<div class="hist_year">
							<h4>2008</h4>
						</div>
						<div class="highlights">
							<p>ATS get registered with government and started providing Alternate Energy Solution</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="hist-inner">
						<div class="hist_year">
							<h4>2010</h4>
						</div>
						<div class="highlights">
							<p>ATS Successfully completes many prrojects of SPS and SHS</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="hist-inner">
						<div class="hist_year">
							<h4>2013</h4>
						</div>
						<div class="highlights">
							<p>ATS becomes a certified firm with ISO 9001-2008</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="hist-inner">
						<div class="hist_year">
							<h4>2016</h4>
						</div>
						<div class="highlights">
							<p>ATS becomes a certified firm with ISO 9001-2015 also work with NGO's (GIZ, MDM)</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="hist-inner">
						<div class="hist_year">
							<h4>2018</h4>
						</div>
						<div class="highlights">
							<p>ATS successfully moved up from C4 to C% of PEC.</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="hist-inner">
						<div class="hist_year">
							<h4>2018</h4>
						</div>
						<div class="highlights">
							<p>FERROTEK brand is launched by ATS as its fabrication brand in Pakistan.</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="hist-inner">
						<div class="hist_year">
							<h4>2018</h4>
						</div>
						<div class="highlights">
							<p>ATS Become an AEDB certified vendor for Net Metering. </p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="services_we_offer">
	<div class="container">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Services We Offer</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-4">
				<div class="service">
					<h3>Solar On-Grid System (Net Metering)</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<div class="service">
					<h3>Solar Hybrid Systeme</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<div class="service">
					<h3>Solar Pumping Systems</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<div class="service">
					<h3>Solar Street Lights</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<div class="service">
					<h3>Solar Geyzers</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<div class="service">
					<h3>Solar Panels</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<div class="service">
					<h3>Solar Pump Inverters</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<div class="service">
					<h3>Solar On-Grid inverters</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<div class="service">
					<h3>Solar Hybrid inverters</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<div class="service">
					<h3>Batteries</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="fabrication_profile">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Fabrication</h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="fabrication_profile_box">
					<h3>Solar Manual Trackers for Pumps</h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="fabrication_profile_box">
					<h3>Solar Auto Trackers</h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="fabrication_profile_box">
					<h3>Fix PV Frames</h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="fabrication_profile_box">
					<h3>Street Light Poles</h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="fabrication_profile_box">
					<h3>Shed Type Structures</h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="fabrication_profile_box">
					<h3>Distribution Boxes</h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="fabrication_profile_box">
					<h3>Residential Containers</h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="fabrication_profile_box">
					<h3>Oil & Water Storage Tanks</h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="fabrication_profile_box">
					<h3>Industrial Fans</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="why_ats">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Why ATS</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="why_box">
					<div class="why_icon">
						<i class="fa fa-paper-plane-o" aria-hidden="true"></i>
					</div>
					<div class="why_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="why_box">
					<div class="why_icon">
						<i class="fa fa-registered" aria-hidden="true"></i>
					</div>
					<div class="why_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="why_box">
					<div class="why_icon">
						<i class="fa fa-rocket" aria-hidden="true"></i>
					</div>
					<div class="why_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="why_box">
					<div class="why_icon">
						<i class="fa fa-line-chart" aria-hidden="true"></i>
					</div>
					<div class="why_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="why_box">
					<div class="why_icon">
						<i class="fa fa-diamond" aria-hidden="true"></i>
					</div>
					<div class="why_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="why_box">
					<div class="why_icon">
						<i class="fa fa-recycle" aria-hidden="true"></i>
					</div>
					<div class="why_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- <section class="how_we_work">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>How We Work</h1>
				</div>
			</div>
		</div>
	</div>
</section> -->

<section class="industry_we_serve">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Industry We Serve</h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="industry_box">
					<div class="ind_inner">
						<i class="fa fa-graduation-cap"></i>
						<h4>Educational Institutes</h4>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="industry_box">
					<div class="ind_inner">
						<i class="fa fa-building-o"></i>
						<h4>Commercial buildings</h4>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="industry_box">
					<div class="ind_inner">
						<i class="fa fa-industry"></i>
						<h4>Industry</h4>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="industry_box">
					<div class="ind_inner">
						<i class="fa fa-users"></i>
						<h4>Domestic Customers</h4>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="industry_box">
					<div class="ind_inner">
						<i class="fa fa-hospital-o"></i>
						<h4>Hospitals</h4>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="industry_box">
					<div class="ind_inner">
						<i class="fa fa-cutlery"></i>
						<h4>Hotels & Restaurants</h4>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="industry_box">
					<div class="ind_inner">
						<i class="fa fa-university"></i>
						<h4>NGOs</h4>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="industry_box">
					<div class="ind_inner">
						<i class="fa fa-truck"></i>
						<h4>Poultry</h4>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="industry_box">
					<div class="ind_inner">
						<i class="fa fa-pagelines" aria-hidden="true"></i>
						<h4>Agriculture </h4>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<!-- ********************************************* -->
<!-- ***************  Our Clients *************** -->
<!-- ********************************************* -->

<section class="our_clients">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Our Clients</h1>
				</div>
			</div>
		</div>

		<div class="row">
			<?php  
			$clients = PsPosts::getPost('clients','','');
			foreach ($clients as $key => $value):
				$id = $value['id'];
				$client_img = AppAttachments::getAllAttachments($id);
				$file_path = $client_img[0]['file_path'];
				?>
				<a href="">
					<div class="client col-md-2 col-sm-4 col-xs-6">
						<figure class="imghvr-zoom-in-clients"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
							<figcaption>
								<div class="border_inside"></div>
							</figcaption>
						</figure> 
					</div>
				</a>
			<?php endforeach ?>

		</div>
	</div>
</section>