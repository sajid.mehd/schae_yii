<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Clients & Testimonials
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Clients & Testimonials</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<section class="page_clients inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Clients</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<?php  
			$clients = PsPosts::getPost('clients','','');
			foreach ($clients as $key => $value):
				$id = $value['id'];
				$client_img = AppAttachments::getAllAttachments($id);
				$file_path = $client_img[0]['file_path'];
				?>
				<a href="">
					<div class="client col-md-2 col-sm-4 col-xs-6">
						<figure class="imghvr-zoom-in-clients"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
							<figcaption>
								<div class="border_inside"></div>
							</figcaption>
						</figure> 
					</div>
				</a>
			<?php endforeach ?>

		</div>
	</div>
</section>

<!-- =============  Testimonials  ============= -->

<section class="testimonials">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Testimonials</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul id="testimonials">
					<?php  
						$testimonials = PsPosts::getPost('testimonial');
						foreach ($testimonials as $key => $test):
						$t_id = $test['id'];
						$t_content = decodeDetails($test['post_content']);
						$t_person_name = $t_content['title'];
						$t_testimonial = $t_content['detail']; 
					?>
					<li>
						<div class="test_single">
							<img src="images/projects/4.jpg">
							<h4><?= $t_person_name; ?></h4>
							<blockquote><?= $t_testimonial; ?></blockquote>
						</div>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</section>