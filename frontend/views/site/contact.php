<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
	use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use frontend\models\ContactForm;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Contact Us
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Contact Us</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<!-- ********************************************* -->
<!-- ************  Home Contact Us ************* -->
<!-- ********************************************* -->

<section class="inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Contact Us</h1>
				</div>
			</div>
		</div>


		<div class="row contacts">
			<div class="col-md-4">
				<div class="cont_box">
					<div class="cont_page_icon">
						<i class="fa fa-map-marker"></i>
					</div>
					<div class="cont_page_text">
						<p>Plot # 69, Street # 3, Main Street # 01, Industrial Area, Sector I‐10/3, Islamabad, Pakistan</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="cont_box">
					<div class="cont_page_icon">
						<i class="fa fa-phone"></i>
					</div>
					<div class="cont_page_text">
						<p>+92 51 4440973, +92 51 4441920</p>
						<p>+92 333 5128664, +92 321 5178412</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="cont_box">
					<div class="cont_page_icon">
						<i class="fa fa-envelope"></i>
					</div>
					<div class="cont_page_text">
						<p>info@atsengg.com</p>
					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-8">
				<div class="contact_form ">
					<?php $form = ActiveForm::begin(); ?>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-user"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'name')->textInput(['placeholder' => 'Full Name'])->label(false); ?>
									</div>							
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-map-marker"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'city')->textInput(['placeholder' => 'City'])->label(false); ?>
									</div>							
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-phone"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'telephone')->textInput(['placeholder' => 'Contact No'])->label(false); ?>
									</div>							
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-envelope"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false); ?>
									</div>							
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-map-marker"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'address1')->textarea(['placeholder' => 'Address'])->label(false); ?>
									</div>							
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="input-with-icon">
									<div class="input-icon"><i class="fa fa-commenting-o"></i></div>
									<div class="form-group">
										<?= $form->field($model, 'query')->textarea(['placeholder' => 'Description'])->label(false); ?>
									</div>							
								</div>
							</div>
						</div>


						<div class="row">
							<div class="col-md-12">
								<?= Html::submitInput('Submit',['class'=>'form-controll']) ?>
							</div>
						</div>

					<?php ActiveForm::end();?>
				</div>
			</div>
			<div class="col-md-4">
				<div class="contact_social">
					<h3>Follow Us On</h3>
					<ul>
						<li><a href="https://www.facebook.com/atsengg/"><i class="fa fa-facebook"></i> facebook</a></li>
						<li><a href="https://twitter.com/ATSEngineering"><i class="fa fa-twitter"></i> twitter</a></li>
						<li><a href="https://plus.google.com/111079297529934752257"><i class="fa fa-google-plus"></i> GooglePlus</a></li>
						<li><a href="https://www.linkedin.com/company/ats-engineering-sales-&-services"><i class="fa fa-linkedin"></i> LinkedIn</a></li>
						<li><a href="https://www.youtube.com/user/ATSEngineering"><i class="fa fa-youtube"></i> Youtube</a></li>
						<li><a href="https://vimeo.com/user47404846"><i class="fa fa-vimeo"></i> Vimeo</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>	
</section>

<section class="map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5585.435437108541!2d73.03452054551866!3d33.65405810295489!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38df95a038fda40d%3A0x86e43ca49a1cf590!2sATS+Engineering+Sales+%26+Services!5e0!3m2!1sen!2s!4v1519995793327" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>