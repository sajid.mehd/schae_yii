<?php  
use backend\models\PsPosts;
use backend\models\AppAttachments;
use yii\helpers\Url;
?>


<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Projects
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Projects</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<!-- ********************************************* -->
<!-- ***************  Our Projects *************** -->
<!-- ********************************************* -->
<?php //dd($id); ?>
<section class=" inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Our Projects</h1>
				</div>
			</div>
			<!-- <div class="col-md-3"> -->
				<?php //include('partials/sidebar_projects.php'); ?>
				<!-- </div> -->

				<div class="row">
					<?php  
					$projects = PsPosts::getPost('projects');
					foreach ($projects as $key => $proj):
						$id = $proj['id'];
						$title = $proj['post_title'];
						$cont_det = decodeDetails($proj['post_content']);
						$detail = limitedWords(strip_tags($cont_det['detail'],'<br>'),100);
						$proj_img = AppAttachments::getAllAttachments($id);
						$file_path = $proj_img[0]['file_path'];
						// }
						// dd($projects);
						?>
						<a href="<?= Url::to(['site/projects-detail','id'=>$id])?>">
							<div class="projects col-md-3 col-sm-6 col-xs-12 ">
								<figure class="imghvr-zoom-in_news"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
									<figcaption>
										<h3><?= $title; ?></h3>
										<p><?= (!empty($detail) ? $detail : 'Detail Will Be Uploaded Soon')?></p>
									</figcaption>
								</figure> 
							</div>
						</a>
					<?php endforeach; ?>
				</div>

			</div>
		</div>
	</section>



<?php 
	// $img_url = $this->theme->baseUrl.'/frontend/web/images/loader.gif';
	// $img = '<img src="'.$img_url.'" alt="Loading...." />';
	$url = Url::to(['site/ajax-call']);
    $script = <<<JS
        $('body').on('click', '.link_class', function(){
            id = $(this).attr('data-id');

            $.ajax({
                url:'{$url}',
                dataType:'html',
                data:{data_id:id},
                type:'GET',
                success:function(data){
                	// console.log(data);
                    $('#all_cities').html(data);
                    // $('select').chosen();
                },
                error: function(){
                    console.log('Error Occur');
                }

            });
        });
        $('body').on('click', '.nation_world_wide', function(){
            id = $(this).attr('data-id');

            $.ajax({
                url:'{$url}',
                dataType:'html',
                data:{n_w_wide_id:id},
                type:'GET',
                success:function(data){
                	// console.log(data);
                    $('#all_cities').html(data);
                    // $('select').chosen();
                },
                error: function(){
                    console.log('Error Occur');
                }

            });
        });

        $('body').on('click', '.count_wise', function(){
            id = $(this).attr('data-id');
            $.ajax({
                url:'{$url}',
                dataType:'html',
                data:{country_id:id},
                type:'GET',
                success:function(data){
                	// console.log(data);
                    $('#all_cities').html(data);
                    // $('select').chosen();
                },
                error: function(){
                    console.log('Error Occur');
                }

            });
        });

        $('body').on('click', '.proj_detail', function(){
            id = $(this).attr('data-id');
            $.ajax({
                url:'{$url}',
                dataType:'html',
                data:{proj_id:id},
                type:'GET',
                success:function(data){
                	// console.log(data);
                    $('#all_cities').html(data);
                    // $('select').chosen();
                },
                error: function(){
                    console.log('Error Occur');
                }

            });
        });

JS;
    $this->registerJs($script,yii\web\View::POS_END);
?>
