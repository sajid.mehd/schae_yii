<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>


<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Applications
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Applications</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<!-- ********************************************* -->
<!-- ************  Home Applications ************* -->
<!-- ********************************************* -->
<?php  
	// dd($applications);
	$id = $applications['id'];
	$content = decodeDetails($applications['post_content']);
	$detail = $content['detail'];
	$attachment = AppAttachments::getAllAttachments($id);
	$file_path = $attachment[1]['file_path'];
?>
<section class="inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1><?= $applications['post_title']?></h1>
				</div>
			</div>
			
			<div class="col-md-8">
				<div class="application_detail">
					<p><?= $detail ?></p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="application_image">
					<img src="<?=$this->theme->baseUrl.'/'.$file_path; ?>">
				</div>
			</div>
	</div>	
</section>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="main_heading">
				<h1>Related Images</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul id="lightgallery" class="list-unstyled row">
				<?php  
					$all_attachment = AppAttachments::getAllAttachments($id);
					foreach ($all_attachment as $key => $att):
					$file_path = $att['file_path'];
				?>
				<li class="col-xs-6 col-sm-4 col-md-3 no_padding" data-src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>" data-sub-html="">
					<a href="">
						<figure class="imghvr-zoom-in"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
				            <figcaption>
				            	<i class="fa fa-search"></i>
				            </figcaption>
				        </figure>
				    </a>
				</li>
				<?php endforeach; ?>
				
			</ul>
		</div>
	</div>
</div>