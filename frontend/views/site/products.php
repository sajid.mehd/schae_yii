<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>


<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_page_top_heading">
					<h1>
						Products
					</h1>	
				</div>	
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li><i class="fa fa-angle-right"></i>Products</li>
					</ul>
				</div>			
			</div>
		</div>
	</div>
</section>

<!-- ********************************************* -->
<!-- ***************  Our Projects *************** -->
<!-- ********************************************* -->

<section class="inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Our Products</h1>
				</div>
			</div>
			<div class="col-md-12">
				<div class="filters">
					<button class="filter" data-filter=".all">All</button>
					<?php  
                        $categories = PsPosts::getPost('product_categories');
                        foreach ($categories as $key => $cat):
                        $cat_id = $cat['id'];
                        $cat_title = $cat['post_title'];
                        $cat_class = strtolower(str_replace(' ','',$cat_title));
                        // dd($categories);
                    ?>
					<button class="filter" data-filter=".<?= $cat_class; ?>"><?= $cat_title;?></button>
					<?php endforeach; ?>
				</div>
			</div>
			
			<div id="Container">
				<div class="effects">
					<?php  
                    $products = PsPosts::getPost('products');
                            // dd($products);
                    foreach ($products as $key => $product):
                        $prod_id = $product['id'];
                        $content = decodeDetails($product['post_content']);
                        $prod_title = $content['title'];
                        $cat = $product['post_parent'];
                        // dd(str_replace('_','',getCategoryTitle($cat)));
                        $prod_detail = limitedWords(strip_tags($content['detail'],'<br>'),100);
                        $category_class = strtolower(str_replace('_','',getCategoryTitle($cat)));

                        $prod_img = AppAttachments::getAllAttachments($prod_id);
                        $file_path = $prod_img[0]['file_path'];

                        // dd($prod_img);
                            // pre($category_class);S
                            // die();
                        ?>
					<a href="<?= Url::to(['site/products-detail', 'id' => $prod_id]);?>">
						<div class="projects col-md-4 col-sm-6 col-xs-12 mix all <?= $category_class; ?>">
					        <figure class="imghvr-zoom-in"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
					            <figcaption>
					              <h3<?= $prod_title; ?></h3>
					              <p><?= $prod_detail; ?></p>
					            </figcaption>
					        </figure> 
				    	</div>
				    </a>
				    <?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>
