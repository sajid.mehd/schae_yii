<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="top_sec_heading">
					<h1>Applications</h1>
				</div>
			</div>
			<div class="col-md-6">
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li> / Applications</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ********************************************* -->
<!-- ************  Home Applications ************* -->
<!-- ********************************************* -->

<section class="home_applications inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1 style="text-align: left;">Our Applications</h1>
				</div>
			</div>
		</div>
		<div class="row">

			<?php  
				$applications = PsPosts::getPost('applications');
			?>
			<?php  
				foreach ($applications as $key => $app):
					
					$id = $app['id'];
					$title = $app['post_title'];
					$content = decodeDetails($app['post_content']);
					$detail = $content['detail'];
					$file_path = $app['file_attachment'];
					// $file_path = $attachment[0]['file_path'];
			?>
			<div class="col-md-4 col-sm-6 no_padding apps">
				<div class="home_apps">
					<a href="<?= Url::to(['site/applications-detail','id'=>$id]);?>">
						<div class="app_icon">
							<img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
						</div>
						<div class="app_text">
							<h3><?= $title?></h3>
							<p><?= strip_tags(limitedWords($detail, 130),'<br><ul><li>'); ?></p>
						</div>
					</a>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>