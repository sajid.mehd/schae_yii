<?php  
    use backend\models\PsPosts;
    use backend\models\AppAttachments;
    use yii\helpers\Url;
?>

<section class="inner_page_top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner_page_top_heading">
                    <h1>
                        Become a Reseller
                    </h1>   
                </div>  
                <div class="breadcrumb_top">
                    <ul>
                        <li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
                        <li><i class="fa fa-angle-right"></i>Become a Reseller</li>
                    </ul>
                </div>          
            </div>
        </div>
    </div>
</section>

<section class="become_a_reseller inner_page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main_heading">
                    <h1>ATS Calculator</h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?php include('partials/support_sidebar.php'); ?>
            </div>

            <div class="col-md-9">
                <div class="reseller_form quote">
                    <?= $this->render('partials/_cal_code')?>
                </div>
            </div>
        </div>
    </div>
</section>


