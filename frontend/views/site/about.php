<?php  
	use backend\models\PsPosts;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-7">
				<div class="top_sec_heading">
					<h1>About Us</h1>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-5">
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li> / About Us</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="inner_page">
	<div class="container">
		<div class="row">
			
			<div class="col-md-5">
				<div class="about_left_img">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="welcome-frame-1">
								<img src="images/about-above.jpg">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="welcome-frame-2">
								<img src="images/about-below.jpg">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="main_heading">
					<h1>Welcome to jntech</h1>
				</div>
				<div class="about_text">
					<?php  
						$about = PsPosts::getPost('about-us');
						$content = decodeDetails($about[0]['post_content']);
						$detail = $content['detail']; 
					?>
					<?= $detail,1100; ?>
				</div>
			</div>
		</div>
	</div>
</section>