<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="inner_page_top">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-7">
				<div class="top_sec_heading">
					<h1>Company News</h1>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-5">
				<div class="breadcrumb_top">
					<ul>
						<li><a href="<?= Url::to(['site/index']);?>">Home</a></li>
						<li> / Company News</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="news_events_home inner_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>News & Events</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<?php  
				$news_events = PsPosts::getPostWithOrderLimit('news_events','','DESC');
				
				foreach ($news_events as $key => $value) {
					$id = $value['id'];
					$content = decodeDetails($value['post_content']);
					$detail = limitedWords($content['detail'],140);
					$title = $value['post_title'];

					$date = $value['event_date'];
					$time = strtotime($date);
					$news_date_format = date('d-M-Y',$time);

					$news_img = AppAttachments::getAllAttachments($id);
					$file_path = $news_img[0]['file_path'];
				
			?>
			<div class="col-md-4 col-sm-6">
				<div class="news_box">
					<a href="<?= Url::to(['site/news-detail','id'=>$id]);?>">
						<div class="news_img">
							<img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>"> 
						</div>
					</a>
					<div class="overlay_orange"></div>
					<div class="news_icon">
						<i class="fa fa-search"></i>
					</div>
					<div class="news_content">
						<h2><?= $title; ?></h2>
						<div class="news_date">
							<p><strong><?= $news_date_format;?></strong></p>
						</div>
						<p><?= $detail; ?></p>
						<a href="<?= Url::to(['site/news-detail','id'=>$id]);?>">Read More <i class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

