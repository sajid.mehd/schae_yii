<?php
	use yii\helpers\Url;
?>
<div class="support_sidebar">
	<ul>
		<li><a href="<?= Url::to(['site/become-a-reseller']);?>">Become a Reseller</a></li>
		<li><a href="<?= Url::to(['site/contact']);?>">Contact Us</a></li>
		<li><a href="<?= Url::to(['site/faq']);?>">FAQs</a></li>
		<li><a href="<?= Url::to(['site/ask-for-dealership']);?>">Ask for Dealership</a></li>
	</ul>
</div>