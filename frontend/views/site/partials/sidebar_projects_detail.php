<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<div class="main_heading">
	<div class="projects_tabs">
		<!-- Nav tabs -->
		

		<ul class="nav nav-tabs" role="tablist">
			<?php  
				$main_area_list = PsPosts::getPost('main_areas');
				foreach ($main_area_list as $key => $value):
					$main_area = $value['post_title'];
			?>
			<li role="presentation" class="<?= ($key == 0) ? 'active' : '' ?>">
					<a href="#<?= $value['id']; ?>" aria-controls="projects" role="tab" data-toggle="tab"><?= $main_area; ?></a></li>
			<?php endforeach ?>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
			
			<?php 
			$main_area_list = PsPosts::getPost('main_areas');
			foreach ($main_area_list as $key => $value): 
				$id = $value['id'];
			?>
				
			<div  role="tabpanel" class="n_w_wide tab-pane <?= ($key == 0) ? 'active' : ''?>" id="<?= $id; ?>">
				<nav>

				    <ul class="nav provinces">
				    	<?php  
					// dd($id);
					$provinces_list = PsPosts::getPostWithParentOrder('provinces',$id);
					foreach ($provinces_list as $key => $prov) {
						$prov_id = $prov['id'];
						
						$content = decodeDetails($prov['post_content']);
						$province = $content['province'];
						?>
						<li><a href="javascript:void(0)" id="btn-1" data-toggle="collapse" data-target="#submenu<?= $key?>" aria-expanded="false"><?= $province ?></a>
							<ul class="nav collapse" id="submenu<?= $key?>" role="menu" aria-labelledby="btn-1">
								<?php 
								$cities = PsPosts::getCityAgainstProvince('cities',$prov_id);
								foreach ($cities as $key => $cty):
									$cty_id = $cty['id'];
									$city = $cty['post_title'];
							?>
							<li><a href="javascript:void(0)" id="btn-1" data-toggle="collapse" data-target="#submenu<?= $cty_id.'level2'?>" aria-expanded="false"><?= $city ?></a>
								<ul class="nav collapse" id="submenu<?= $cty_id.'level2'?>" role="menu" aria-labelledby="btn-1">
									<?php 
									$projects = PsPosts::getProjectsAgainstCity('projects',$cty_id);
									foreach ($projects as $key => $proj):
										$proj_id = $proj['id'];
										$Proj_title = $proj['post_title'];
										?>
										<li><a href="<?= Url::to(['projects-detail','id'=>$proj_id]);?>"><?= $Proj_title; ?></a></li>
									<?php endforeach ?>
								</ul>
							</li>
								<?php endforeach ?>
							</ul>
						</li>
						<?php } ?>
					</ul>
				</nav>						
			</div>
			<?php endforeach ?>
		</div>
	</div>
</div>


