<?php  
	use backend\models\AppAttachments;
?>

<div class="downloads_list">
	<ul>
		<?php  

		// $downloads = AppAttachments::getAllDocuments($id);
							// dd($downloads);
		foreach ($model as $key => $down):
			$file_name = $down['file_name'];
			$file_path = $down['file_path'];
			?>
			<li>
				<a target="_blank" href="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
					<?= $file_name; ?><span><img src="images/download.png"></span>
				</a>
			</li>
		<?php endforeach ?>
	</ul>
</div>