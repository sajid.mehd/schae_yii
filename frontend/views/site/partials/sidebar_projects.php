<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<div class="main_heading">
	<div class="projects_tabs">
		<!-- Nav tabs -->
		

		<ul class="nav nav-tabs" role="tablist">
			<?php  
				$main_area_list = PsPosts::getPost('main_areas');
				foreach ($main_area_list as $key => $value):
					$main_area = $value['post_title'];
					$m_id = $value['id'];

					if (isset($proj_id)){ ?>
					<li role="presentation" class="<?= ($m_id == $proj_id) ? 'active' : '' ?>">
					<a class="nation_world_wide" data-id="<?= $value['id']; ?>" href="#<?= $value['id']; ?>" aria-controls="projects" role="tab" data-toggle="tab"><?= $main_area; ?></a></li>
					<?php }else{ ?>
					<li role="presentation" class="<?= ($key == 0) ? 'active' : '' ?>">
					<a class="nation_world_wide" data-id="<?= $value['id']; ?>" href="#<?= $value['id']; ?>" aria-controls="projects" role="tab" data-toggle="tab"><?= $main_area; ?></a></li>
					<?php } ?>
			<?php endforeach ?>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
			
			<?php 
			$main_area_list = PsPosts::getPost('main_areas');
			foreach ($main_area_list as $key => $value): 
				$id = $value['id'];

				// dd($id);
			?>
			<?php if (isset($proj_id)){ ?>
				<div  role="tabpanel" class="n_w_wide tab-pane <?= ($id == $proj_id) ? 'active' : ''?>" id="<?= $id; ?>">

			<?php }else{ ?>
			<div  role="tabpanel" class="n_w_wide tab-pane <?= ($key == 0) ? 'active' : ''?>" id="<?= $id; ?>">
				<?php } ?>
				<nav>

				    <ul class="nav provinces">
			    	<?php  
					// dd($id);
			    	if ($id == 8){
					$provinces_list = PsPosts::getPostWithParentOrder('provinces',$id);

					foreach ($provinces_list as $key => $prov) {
						$prov_id = $prov['id'];
						
						$content = decodeDetails($prov['post_content']);
						$province = $content['province'];
						?>
						<li><a href="javascript:void(0)" id="btn-1" data-toggle="collapse" data-target="#submenu<?= $key?>" aria-expanded="false"><?= $province ?></a>
							<ul class="nav collapse" id="submenu<?= $key?>" role="menu" aria-labelledby="btn-1">
								<?php 
								$cities = PsPosts::getCityAgainstProvince('cities',$prov_id);
								// dd($cities);
								foreach ($cities as $key => $cty):
									$cty_id = $cty['id'];
									$city = $cty['post_title'];
									// dd($cty_id);
							?>
							<li><a href="javascript:void(0)" data-id="<?=$cty_id?>" class="link_class"><?= $city; ?></a></li>
							
								<?php endforeach ?>
							</ul>
						</li>

						<?php } }else{ ?>
						

						<!-- ========= List of countries -->
						<?php
						$countries_list = PsPosts::getPostWithParentOrder('countries',$id);
						// dd($countries_list);
						foreach ($countries_list as $key => $count) {
						$country_id = $count['id'];
						// dd($country_id);
						$content = decodeDetails($count['post_content']);
						$country = $content['country'];
						?>
						<li>
							<a href="javascript:void(0)" data-id="<?=$country_id?>" class="count_wise"><?= $country; ?></a>
						</li>
						<?php } }?>
					</ul>
				</nav>						
			</div>
			<?php endforeach ?>
		</div>
	</div>
</div>