<?php  
	use yii\helpers\Url;
?>

<div class="solar_system_sidebar">
	<ul>
		<li><a href="<?= Url::to(['site/solar-systems-detail']);?>">Solar On-Grid System (Net Metering)</a></li>
		<li><a href="#">Solar Hybrid System</a></li>
		<li><a href="#">Solar Pumping Systems</a></li>
		<li><a href="#">Solar Street Lights</a></li>
		<li><a href="#">Solar Geyzers</a></li>
		<li><a href="#">Solar Panels</a></li>
		<li><a href="#">Solar Pump Inverters</a></li>
		<li><a href="#">Solar On-Grid inverters</a></li>
		<li><a href="#">Solar Hybrid inverters</a></li>
		<li><a href="#">Batteries</a></li>
	</ul>
</div>