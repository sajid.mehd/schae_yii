
<div class="project_details">
	<?php  
		// dd($model);
		$proj_title = $model['post_title'];
		$proj_desc = decodeDetails($model['post_content']);
		$proj_detail = $proj_desc['detail'];
		?>

		<h2><?= $proj_title; ?></h2>
		<div class="back">
			<a href="">Back</a>
		</div>
	
	<div class="proj_desc">
		<p><?= $proj_detail; ?></p>
	</div>
</div>