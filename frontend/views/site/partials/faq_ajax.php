<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= $model['post_title']?></h4>
      </div>
      <div class="modal-body">
        <?php  
          $content = decodeDetails($model['post_content']);
          $ans = $content['answer'];
          // dd($ans);
        ?>
        <p><?= $ans; ?></p>
      </div>
    </div>
  </div>
</div>