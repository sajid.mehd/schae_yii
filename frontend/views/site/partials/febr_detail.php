<?php  
	use backend\models\PsPosts;
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>

<section class="solar_systems first">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1><?= $model['post_title'];?></h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="solar_system_sidebar">
					<ul>
						<?php  
							$fabri = PsPosts::getPost('febrications');
							foreach ($fabri as $key => $fabr):
							$f_id = $fabr['id'];
							$f_title = $fabr['post_title'];
						?>
						<li><a class="f_id" data-id="<?=$f_id;?>"><?= $f_title; ?></a></li>
						<?php endforeach; ?>
						
					</ul>
				</div>
			</div>

			<div class="col-md-9">
				<div class="desc">
					<div class="main_img">
						<?php  
							$id = $model['id'];
							$f_content = decodeDetails($model['post_content']);
							$f_detail = $f_content['detail'];
							$feb_img = AppAttachments::getAllAttachments($id);
							$file_path = $feb_img[0]['file_path'];
						?>
						<img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
					</div>
					<p><?= $f_detail; ?></p>
				</div>
			</div>
		</div>
		
	</div>
</section>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="main_heading">
				<h1>Related Images</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul id="lightgallery" class="list-unstyled row">
				<?php  
				
				$all_img = AppAttachments::getAllAttachments($id);
				foreach ($all_img as $key => $value):
				$file_path = $value['file_path'];
				?>
				<li class="col-xs-6 col-sm-4 col-md-3 no_padding" data-src="<?= $this->theme->baseUrl.'/'.$file_path;?>" data-sub-html="">
					<a href="">
						<figure class="imghvr-zoom-in"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
				            <figcaption>
				            	<i class="fa fa-search"></i>
				            </figcaption>
				        </figure>
				    </a>
				</li>
			<?php endforeach; ?>
		</div>
	</div>
</div>

