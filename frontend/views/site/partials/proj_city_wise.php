<?php  
	// dd($model);
	use backend\models\AppAttachments;
	use yii\helpers\Url;
?>
<?php  
foreach ($model as $key => $value){
	$id = $value['id'];
	// dd($);
	$content = decodeDetails($value['post_content']);
	$title = $content['title'];
	$detail = $content['detail'];
	$attachment = AppAttachments::getAllAttachments($id);
	$file_path = $attachment[0]['file_path'];
	// dd($file_path);
?>
<div class="col-md-4 col-sm-6 no_padding">
	<a href="javascript:void(0)" data-id="<?= $id ?>" class="proj_detail">
		<div class="proj_box">
			<div class="proj_img">

				<img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>" alt="">
			</div>
			<div class="item-content">
				<h4>
					<?= $title; ?>
				</h4>

				<p><?= strip_tags(limitedWords($detail,90)); ?></p>
			</div>
			<!-- <div class="proj_icon">
				<i class="fa fa-user"></i>
			</div> -->
		</div>
	</a>
</div>
<?php } ?>