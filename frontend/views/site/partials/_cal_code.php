<?php

use yii;
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\models\PsPosts;
use backend\models\AppAttachments;
?>
<style>

    .titleparag {
       
        color: #000000;
        float: left;
        font-size: 20px;
        font-weight: bold;
        margin: 19px 7px 0 300px;
        padding-right: 14px;
        text-shadow: 2px 0 #7C7C7C;
        width: auto;
    }


    .qwrapper{
        float:left;
        border:none !important; 
        padding:3px 3px 8px;
    }


    table {
        float: left;
        margin: 0 0 0 -4px;
    }

    .one-row{
        float: left;
        width: 16%;
    }
    .form-sec{
        border-bottom: 1px solid #CCCCCC;
        float: left;
        height: auto;
        padding: 5px 0;
        width: 100%;
    }

    .lbl-left-small {
        color: #020264;
        font-family: atsFont;
        font-size: 13px;
        font-weight: bold;
        margin: 10px 0 0;
        padding: 0 1%;
        width: 17%;
    }


    .lblinvrt {
        color: #020264;
        font-family: atsFont;
        font-size: 13px;
        font-weight: bold;
        margin: 10px 0 0 116px;
        padding: 0 1%;
        width: 17%;
    }

    .lblbsize {
        color: #020264;
        font-family: atsFont;
        font-size: 13px;
        font-weight: bold;
        margin: 10px 0 0 63px;
        padding: 0 1%;
        width: 17%;
    }
    .lblhours{
        color: #020264;
        font-family: atsFont;
        font-size: 13px;
        font-weight: bold;
        margin: 0 0 0 91px;
        width: 20%;

    }

    .reset{
       // background-color: #FF7F27 !important;
        border: medium none;
        color: #000000;
        float: left;
        text-align: center;
        padding: 0 0 0 0;
        font-weight: bold;
        //height: 28px;
       // margin: 17px 0 0;
       // width: 80px;
        cursor: pointer;
    }



    .calc_tothours {
        height: 27px;
        margin: 0;
        padding-left: 5px;
        text-align: left;
        width: 49px;
    }


    /*	.city_clx{
                    
                    height: 27px;
                padding: 4px 0 0 5px;
                width: 123px;
            }*/

    .calc_type{
        height: 27px;
        padding: 5px 0 0 6px;
        width: 178px;
        margin:7px 0 6px;
    }


    .calc_quantity.calc_all, .calc_wattage.calc_all, .calc_hours.calc_all, .calc_totpower, .calc_pvsize, .calc_chrgController,.calc_btrysizing, .calc_invertor, .calc_sum_totpower, .calc_sum_pvsize {
        height: 26px;
        padding-left: 6px;
        text-align: left;
        width: 62px;

    }


    #sm1{
        background: none repeat scroll 0 0 #2C68FC !important;
        color: #FFFFFF;
        height: 26px;
        padding-left: 0px;
        text-align: left;
        width: 49px;
        margin:6px 0 0 0;

    }

    #sm2{

        background: none repeat scroll 0 0 #2C68FC !important;
        color: #FFFFFF;
        height: 26px;
        padding-left: 0px;
        text-align: left;
        width: 49px;
        margin: 6px 0 0 0;

    }
    .calc_sysvoltage {
        height: 27px;
        margin: 2px 0 22px 16px;
        padding-left: 2px;
        text-align: left;
        width: 49px;
    }



    th {
        color: #020264;
        font-family: atsFont;
        font-size: 14px;
        padding: 0 0 0 5px;
    }



    td {
        color: #020264;
        font-family: atsFont;
        font-size: 16px;
        font-weight: bold;
        padding-left: 14px;
    }



    .citylbl {
        color: #FFFFFF;
        float: left;
        font-family: atsFont;
        font-size: 16px;
        font-weight: bold;
        margin: 0 3px 1px -136px;
        width: 89px;
    }

    .heading{
        background: none repeat scroll 0 0 #2C68FC;
        float: left;
        margin: -5px 0 0;
        width: 102%;
    }


    .leftblock {
        float: left;
        height: auto;
        width: 274px;
    }


    .lblafter{
        color: rgb(2, 2, 100); 
        font-family: atsFont; 
        font-size: 12px; 
        font-style: normal; 
        font-weight: bold;
    }
    .ref{
        
        color:#FFFFFF !important;
    }

</style>


<div class="content">
    <div class="main-content">


        <div class="rightblock">	

            <div style="float:left;">


               
                        <form class="myForm">

                            <div class="form-sec" style="height: auto; background-color: rgb(217, 219, 219); width: 99%; padding-right: 8px; margin: 0px 0 0 6px; border:8px solid #2C68FC"">

                                <?php
                                $CityArr = array(
                                    "5.3" => "Islamabad",
                                    "5.5000" => "Lahore",
                                    "5.50" => "Karachi",
                                    "5.9" => "Quetta",
                                    "4.9" => "Peshawar",
                                    "5.800" => "Multan",
                                    "5.500" => "Gawadar",
                                    "5.5" => "Hyderabad",
                                    "5.800" => "Nawabshah",
                                    "5.70" => "Sukkur",
                                    "5.80" => "Bahawalpur",
                                    "5.7" => "Faisalabad",
                                    "5.30" => "Gujrat",
                                    "6.0" => "1-6.0",
                                    "6.5" => "2-6.5",
                                    "7.0" => "3-7.0",
                                    "7.5" => "4-7.5",
                                );
                                ?>


                                <div class="heading"> 
                                    <div style="width: 11%; float: left; margin: 9px -61px 11px 143px;"><label class="citylbl" for="city">Select City</label></div>
                                    <div class="one-row" style="width: 23%; padding: 0px; margin: 0px 0px 16px -55px;">
                                       

                                        <select name="city" class="city_clx calc_other" >
                                            <?php
                                            foreach ($CityArr as $key => $cit) {
                                                $selected = "";
                                                if ($cit == "Islamabad") {
                                                    $selected = 'selected="selected"';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $cit; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="btnrow" style="width:100%;">
                                        
                                        <span class="reset" style="margin-left:10px; height: 35px;">
                                        <input  type="button" value="Reset Form" onClick="this.form.reset()" />
                                            <i class="ref fa fa-refresh fa-lg "></i>
                                        </span>

                                        <!--<a href="" value="Reset Form" onClick="this.form.reset()"></a>-->

                                    </div>



                                </div> 
                                <div class="one-row" style="width:100%;">
                                    <?php
                                    $arr = range(1, 15);
                                    $WattageArr = array(
                                        "" => "", "20" => "Fan-20W", "45" => "Fan-45W", "60" => "Fan-60W",
                                        "120" => "Fan-120W", "3" => "Energy Saver-3W", "7" => "Energy Saver-7W", "18" => "Energy Saver-18W",
                                        "20.0" => "Energy Saver-20W", "24" => "Energy Saver-24W", "25" => "Energy Saver-25W",
                                        "60" => "Energy Saver-60W", "120.0" => "Television-120W", "85" => "Television-85W",
                                        "140" => "Television-140W", "150" => "Computer-150W", "100" => "Monitor-100W"
                                    );
                                    ?>
                                    <table>
                                        <thead>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <th></th>	
                                                <th>TYPE</th>	
                                                <th>QTY</th>	
                                                <th>WATTS</th>	
                                                <th>HOURS</th>	
                                                <th>TOT.POW</th>	
                                                <th>PVSIZE</th>	
                                            </tr>


                                            <?php
                                            foreach ($arr as $key => $value) {
                                                ?>
                                                <tr>
                                                    <td> 
                                                        <?php echo $value; ?>	
                                                    </td>

                                                    <td>
                                                        <select name='calculate[type]<?php echo '[' . $value . ']'; ?>' class='calc_type' style=data-placeholder="Slecttype"/>
                                                        <?PHP
                                                        foreach ($WattageArr as $val => $text) {
                                                            ?>
                                                <option value="<?php echo $val; ?>">
                                                    <?php echo $text; ?>
                                                </option>
                                            <?PHP }
                                            ?>
                                            </select>
                                            </td>
                                            <td>
                                                <input name='calculate[quantity]<?php echo '[' . $value . ']'; ?>' class='calc_quantity calc_all' value="0"/>
                                            </td>
                                            <td>
                                                <input name='calculate[wattage]<?php echo '[' . $value . ']'; ?>' class='calc_wattage calc_all' value="0"/>
                                            </td>
                                            <td>
                                                <input name='calculate[hours]<?php echo '[' . $value . ']'; ?>' class='calc_hours calc_all' value="0"/>
                                            </td>
                                            <td>
                                                <input name='calculate[totpower]<?php echo '[' . $value . ']'; ?>' class='calc_totpower' readonly="" value="0"/>
                                            </td>
                                            <td>
                                                <input name='calculate[pvsize]<?php echo '[' . $value . ']'; ?>' class='calc_pvsize' readonly="" value="0"/>
                                            </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>	

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <input id="sm1" name='calculate[sum-totpower]<?php echo '[' . $val . ']'; ?>' class='calc_sum_totpower' readonly="" value="0"/>
                                                </td>
                                                <td>
                                                    <input id="sm2" name='calculate[sum-pvsize]<?php echo '[' . $val . ']'; ?>' class='calc_sum_pvsize' value="0"/>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div class="assemble">
                                    <div class="one-row" style="width: 100%; margin: 32px 0px 0px;">
                                        <label class="lbl-left-small"style="margin: 32px 0px 0px 6px;">System Voltage:</label>
                                        <input name='calculate[sysvoltage]' class='calc_sysvoltage' value="12"/>
                                        <label class="lblafter">Volts</label>

                                        <label class="lblinvrt">Invertor:</label>
                                        <input name='calculate[invertor]' class='calc_invertor' value="0" readonly=""/>
                                        <label class="lblafter">Watts</label>
                                    </div>
                                    <div class="one-row" style="width: 91%; margin: 0px 0px 0px 7px;">
                                        <label class="lbl-left-small">Charger Controller:</label>
                                        <input name='calculate[chrgController]' class='calc_chrgController' value="0" readonly=""/>
                                        <label class="lblafter">A/Volts</label>
                                        <label class="lblbsize">Battery Sizing:</label>
                                        <input name='calculate[btrysizing]' class='calc_btrysizing' value="0" readonly=""/>
                                        <label class="lblafter">Ah</label>

                                    </div>

                                    <div class="one-row" style="width: 91%; margin: 25px 0px 13px 7px;">
                                        <label class="lblhours">Hours:</label>
                                        <input name='calculate[hours]' class='calc_tothours' value="24"/>

                                    </div>
                                </div>
                            </div>
                        </form>	
                   


            </div>

        </div>


    </div>

</div>


<?php
$script = <<<JS
                    $('body').on('change keyup','.calc_all',function () {
                        var ths = $(this);
                        calcAll();
                    });
                    $('.calc_type').change(function () {
                        var ths = $(this);
                        var calctype = $(this).val();
                        ths.parents('tr').find('.calc_wattage').val(calctype);
                        calcAll();
                    });
                    $('.city_clx').change(function () {
                        calcAll();
                    });
                    function calcAll()
                    {
                        $('.calc_quantity').each(function () {
                            var quant = $(this).val();
                            var watt = $(this).parents('tr').find('.calc_wattage').val();
                            var hours = $(this).parents('tr').find('.calc_hours').val();
                            var cit = $('.city_clx').val();
                            if (watt != '' && hours != '' && quant != '')
                            {
                                var tot = watt * quant * hours;
                                if (cit != '')
                                {
                                    var pvsize = (tot * (1.3)) / cit;
                                    //					alert(1.2654.toFixed());	
                                    $(this).parents('tr').find('.calc_totpower').val(tot);
                                    $(this).parents('tr').find('.calc_pvsize').val(pvsize.toFixed(2));
                                }

                            }
                        });
                        var totpow = 0;
                        var totpvsize = 0;
                        $('.calc_totpower').each(function () {
                            var ths = $(this).val();
                            if (ths != '')
                            {
                                totpow = parseInt(totpow) + parseInt(ths);
                                $('.calc_sum_totpower').val(totpow);
                            }
                        });
                        $('.calc_pvsize').each(function () {
                            var ths = $(this).val();
                            if (ths != '')
                            {
                                totpvsize = parseFloat(totpvsize) + parseFloat(ths);
                                $('.calc_sum_pvsize').val(totpvsize.toFixed(2));
                            }
                        });
                        var sumpv = $('.calc_sum_pvsize').val();
                        if (sumpv != '')
                        {
                            var sysvolt = $('.calc_sysvoltage').val();
                            var res = parseFloat(sumpv) / sysvolt;
                            $('.calc_chrgController').val(res.toFixed(2));

                        }
                        var sumtotpow = $('.calc_sum_totpower').val();
                        var sysvolt = $('.calc_sysvoltage').val();
                        if (sumtotpow != '' && sysvolt != '')
                        {
                            var tothours = $('.calc_tothours').val();
                            var batrysize = (parseInt(sumtotpow) * parseInt(tothours)) / (0.6 * 0.8 * parseInt(sysvolt) * 24);
                            $('.calc_btrysizing').val(batrysize.toFixed(2));
                        }
                        var invertor = 0;
                        $('.calc_quantity').each(function () {
                            var quant = $(this).val();
                            if (quant != '')
                            {
                                var watt = $(this).parents('tr').find('.calc_wattage ').val();

                                invertor = invertor + (parseInt(watt) * parseInt(quant));

                            }
                            var totinvertor = invertor * 1.3;
                            $('.calc_invertor').val(totinvertor.toFixed(2));
                        });
                    }
                    // this is to calculate charge controller on system Voltage
                    $('body').on('blur keyup change','.calc_sysvoltage',function () {
                        var sysvolt = $(this).val();
                        var pvsize = $('.calc_sum_pvsize').val();
                        if (pvsize != '')
                        {
                            var res = pvsize / sysvolt;
                            $('.calc_chrgController').val(res.toFixed(2));
                        }
calcAll();


                    });
                    // this is to calculate battry sizing base on hours
                    $('.calc_tothours').blur(function () {
                        var sumtotpow = $('.calc_sum_totpower').val();
                        var sysvolt = $('.calc_sysvoltage').val();
                        if (sumtotpow != '' && sysvolt != '')
                        {
                            var tothours = $('.calc_tothours').val();
                            var batrysize = (parseInt(sumtotpow) * parseInt(tothours)) / (0.6 * 0.8 * parseInt(sysvolt) * 24);
                            $('.calc_btrysizing').val(batrysize.toFixed(2));
                        }
                    });
                    function myFunction()
                    {
                        document.getElementById("myForm").reset();
                    }
                        $(function () {

        $(".success-flash").animate({opacity: 1.0}, 8000).fadeOut("slow");
        $(".quotation-flash").animate({opacity: 1.0}, 8000).fadeOut("slow");
    });
JS;

$this->registerJs($script, $this::POS_END);
?>