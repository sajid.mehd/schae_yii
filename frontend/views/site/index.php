<?php  
use backend\models\PsPosts;
use backend\models\AppAttachments;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<!-- ********************************************* -->
<!-- **************  Home About Us *************** -->
<!-- ********************************************* -->


<section class="about_home">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Welcome to ATS Engineering</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7">
				<div class="about_desc">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="areas_of_work">
								<div class="work_icon">
									<img src="<?= $this->theme->baseUrl?>/images/areas-of-work/solar-services.png">
								</div>
								<div class="work_text">
									<h3>Solar Services</h3>
									<ul>
										<li>On / Grid </li>
										<li>Off / Grid </li>
										<li>Solar Pumping</li>
										<li>On / Off Grid Inverters</li>
										<li>PV Panels</li>
										<li>Batteries</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="areas_of_work">
								<div class="work_icon">
									<img src="<?= $this->theme->baseUrl?>/images/areas-of-work/1.png">
								</div>
								<div class="work_text">
									<h3>Fabrication Products</h3>
									<ul>
										<li>Ferrotek Manual Tracking System</li>
										<li>Ferrotek Auto Tracking System</li>
										<li>Ferrotek Rooftop Fixed Frame Type</li>
										<li>Ferrotek Rooftop Shed Frame Type</li>
										<li>Street Light Bulbs Tubular & Conical</li>
										<li>Residential Containers</li>
										<li>Battery Boxes & Racks</li>
										<li>Distribution Boxes</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="get_a_quote quote">
								<h3>Get a Quote</h3>
								<?php $form = ActiveForm::begin(); ?>
									<div class="row">
										<?= $form->field($model, 'hidden1')->hiddenInput(['value'=>'home_quote'])->label(false); ?>
										<div class="col-md-6 col-sm-6">
											<div class="input-with-icon">
												<div class="input-icon"><i class="fa fa-user"></i></div>
												<div class="form-group">
													<?= $form->field($model, 'name')->textInput(['placeholder' => 'Name'])->label(false); ?>
												</div>							
											</div>
										</div>
										<div class="col-md-6 col-sm-6">
											<div class="input-with-icon">
												<div class="input-icon"><i class="fa fa-envelope"></i></div>
												<div class="form-group">
													<?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false); ?>
												</div>							
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<div class="input-with-icon">
												<div class="input-icon"><i class="fa fa-phone"></i></div>
												<div class="form-group">
													<?= $form->field($model, 'telephone')->textInput(['placeholder' => 'Contact No'])->label(false); ?>
												</div>							
											</div>
										</div>
										<div class="col-md-6 col-sm-6">
											<div class="input-with-icon">
												<div class="input-icon"><i class="fa fa-tasks"></i></div>
												<div class="form-group">
													<?= $form->field($model, 'subject')->textInput(['placeholder' => 'Subject'])->label(false); ?>
												</div>							
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-12">
											<div class="input-with-icon">
												<div class="input-icon"><i class="fa fa-shopping-basket"></i></div>
												<div class="form-group">
													<?= $form->field($model, 'query')->textarea(['placeholder' => 'Query'])->label(false); ?>
												</div>							
											</div>
											
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<?= Html::submitInput('Submit',['class'=>'form-control']) ?>
										</div>
									</div>
									
								<?php ActiveForm::end(); ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="social_icons">
								<ul>
									<li><a target="_blank" href="https://www.facebook.com/atsengg/"><i class="fa fa-facebook"></i></a></li>
									<li><a target="_blank" href="https://twitter.com/ATSEngineering"><i class="fa fa-twitter"></i></a></li>  
									<li><a target="_blank" href="https://plus.google.com/111079297529934752257"><i class="fa fa-google-plus"></i></a></li> 
									<li><a target="_blank" href="https://www.linkedin.com/company/ats-engineering-sales-&-services"><i class="fa fa-linkedin"></i></a></li> 
									<li><a target="_blank" href="https://www.youtube.com/user/ATSEngineering"><i class="fa fa-youtube"></i></a></li>
									<li><a target="_blank" href="https://vimeo.com/user47404846"><i class="fa fa-vimeo"></i></a></li> 
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div> 
			<div class="col-md-5">
				<div class="right_about">
					<h3>About ATS Engineering</h3>
					<?php  
						$about = PsPosts::getPost('about-us');
						$ab_content = decodeDetails($about[0]['post_content']);
						$ab_detail = limitedWords(strip_tags($ab_content['detail'],'<br>'),500); 
					?>
					<p><?= $ab_detail;?></p>
					<a href="<?= Url::to(['site/profile']);?>"> Read More <i class="fa fa-angle-double-right"></i></a>
					<img src="images/projection.jpg">
				</div>
			</div>
		</div>
	</div>
</section> 


<!-- ********************************************* -->
<!-- ***************  Our Projects *************** -->
<!-- ********************************************* -->


<section class=" brands_we_deal">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_headingw">
					<h1>Ferrotek Fabrications</h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul id="lightSlider" class="light-slider">
					<?php  
					$brands = PsPosts::getPost('distributors');
					foreach ($brands as $key => $value):
						$id = $value['id'];
						$client_img = AppAttachments::getAllAttachments($id);
						$file_path = $client_img[0]['file_path'];
						?>
						<li>
							<div class="home_brand">
								<img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
							</div>
						</li>
					<?php endforeach ?>
				</ul>
			</div>
		</div>
	</div>
</section>


<!-- ********************************************* -->
<!-- ***************  Fabrications *************** -->
<!-- ********************************************* -->

<section class="our_projections">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<div class="left_projection">
					<h3>Fabrication</h3>
					<?php  
						$fabrication = PsPosts::getPost('febrications');
						// dd($fabrication);
						$feb_content = decodeDetails($fabrication[0]['post_content']);
						$detail = limitedWords(strip_tags($feb_content['detail'],'<br>'),220);
					?>
					<p><?= $detail;?></p>
					<a href="">
						<span>Read More <i class="fa fa-angle-double-right"></i></span>
					</a>
					<img src="images/Fabrication.jpg">
				</div>
			</div>

			<div class="col-md-7">
				<div class="fbr">
					<div class="main_heading">
						<h1 style="margin-top: -40px;">Fabrication</h1>
					</div>
					<div class="proj_desc">
						<div class="row">
							<?php  
								$all_fabrications = PsPosts::getPost('febrications');
								foreach (array_slice($all_fabrications,1) as $key => $fabr):
								$fabr_id = $fabr['id'];
								$fabr_title = $fabr['post_title'];
							?>
							<div class="col-lg-4 col-md-6 col-sm-6">
								<a href="<?= Url::to(['site/fabrication-detail','id'=>$fabr_id]);?>">
									<div class="feb_box">
										<h4><?= $fabr_title;?></h4>
									</div>
								</a>
							</div>

							<?php endforeach; ?>
							<div class="clearfix"></div>
							<div class="col-md-12">
								<div class="readmore pt2em">
									<a href="">More Fabrication</a>
								</div>
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ********************************************* -->
<!-- ***************  Net Metring *************** -->
<!-- ********************************************* -->

<section class="net_metring">
	<div class="container">
		<div class="col-md-6">
			<div class="netmetring_top">
				<h2>
					<span>Net Metring</span><br>
					Empowring People to Generate Power
				</h2>
				<p>Net metering (also known as net energy metering) is a solar incentive that allows you to store energy in the electric grid. When your solar panels produce excess power, that energy is sent to the grid and in exchange you can pull from the grid when your system is under-producing like during nighttime.</p>
			</div>
		</div>
		<div class="col-md-6">
			<div class="netmetring_img">
				<img src="images/netmeter-dg.png">
			</div>
		</div>
	</div>
</section>
<section class="netmetring_left query">
	<img src="<?= $this->theme->baseUrl?>/images/approved-by-nepra.png">
	<h2>Apply For Net Metring</h2>
	<?php $form = ActiveForm::begin(); ?>
		<div class="row ">
			<?= $form->field($model, 'hidden1')->hiddenInput(['value'=>'home_net_mtr'])->label(false); ?>
			<div class="col-md-6 col-sm-6">
				<div class="input-with-icon">
					<div class="input-icon"><i class="fa fa-user"></i></div>
					<div class="form-group">
						<?= $form->field($model, 'name')->textInput(['placeholder' => 'Name'])->label(false); ?>
					</div>							
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="input-with-icon">
					<div class="input-icon"><i class="fa fa-envelope"></i></div>
					<div class="form-group">
						<?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false); ?>
					</div>							
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="input-with-icon">
					<div class="input-icon"><i class="fa fa-phone"></i></div>
					<div class="form-group">
						<?= $form->field($model, 'telephone')->textInput(['placeholder' => 'Contact No'])->label(false); ?>
					</div>							
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-with-icon">
					<div class="input-icon"><i class="fa fa-map-marker"></i></div>
					<div class="form-group">
						<?= $form->field($model, 'area[]')->dropDownList(
							            ['a' => 'Area One', 'b' => 'Area Two', 'c' => 'Area Three']
							    )->label(false); ?>
					</div>							
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?= Html::submitInput('Submit',['class'=>'form-control']) ?>
			</div>
		</div>
		
	<?php ActiveForm::end(); ?>
</section>
<section class="netmetring_right">
	<h2>ATS Engineering Net metring</h2>
	<div class="net_metring_text">
		<?php  
			$n_metr = PsPosts::getPost('applications');
			// dd($n_metr);
			$nmtr_id = $n_metr[0]['id'];
			$nmtr_cont = decodeDetails($n_metr[0]['post_content']);
			// dd($nmtr_cont);
			$nmtr_detail = limitedWords(strip_tags($nmtr_cont['detail'],'<br>,<ul><li><b>'),300);

		?>
		<p><?= $nmtr_detail; ?></p>
		<a href="<?= Url::to(['site/applications-detail','id'=>$nmtr_id]);?>">
			<span>Read More <i class="fa fa-angle-double-right"></i></span>
		</a>
	</div>
</section>
<div class="clearfix"></div>




<!-- ********************************************* -->
<!-- ***************  Our Products *************** -->
<!-- ********************************************* -->


<section class="our_products">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="main_heading">
					<h1>Our Products</h1>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="view_all">
					<a href="<?= Url::to(['site/products']);?>">View All <i class="fa fa-angle-double-right"></i></a>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="filters">
					<!-- <button class="filter" data-filter=".all">All</button> -->
					<?php  
                        /*$sub_categories = PsPosts::getPost('product_categories');
                        foreach ($sub_categories as $key => $s_cat):
                        $s_cat_id = $s_cat['id'];
                        $s_cat_title = $s_cat['post_title'];
                        $s_cat_class = strtolower(str_replace(' ','',$s_cat_title));*/
                    ?>
					<!-- <button class="filter" data-filter=".<?php //$s_cat_class; ?>"><?php //$s_cat_title;?></button> -->
					<?php // endforeach; ?>
				</div>
			</div>
			
			<div id="Container">
				<div class="effects">
					<?php  
                    $products = PsPosts::getPost('products',6,'');
                            // dd($projects);
                    foreach ($products as $key => $product):
                        $prod_id = $product['id'];
                        $content = decodeDetails($product['post_content']);
                        $prod_title = $content['title'];
                        $prod_detail = limitedWords(strip_tags($content['detail'],'<br>'),70);
                        $category_class = strtolower(str_replace(' ','',getCategoryTitle($content['project_sub_category'])));

                        $prod_img = AppAttachments::getAllAttachments($prod_id);
                        $file_path = $prod_img[0]['file_path'];


                            // pre($category_class);
                            // die();
                        ?>
					<a href="<?= Url::to(['site/products-detail', 'id' => $prod_id]);?>">
						<div class="projects col-md-4 col-sm-6 col-xs-12 mix all <?= $category_class; ?>">
					        <figure class="imghvr-zoom-in"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
					            <figcaption>
					              <h3<?= $prod_title; ?></h3>
					              <p><?= $prod_detail; ?></p>
					            </figcaption>
					        </figure> 
				    	</div>
				    </a>
				    <?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- ********************************************* -->
<!-- ****************  ATS Videos **************** -->
<!-- ********************************************* -->
<section class="ats_videos">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_headingw">
					<h1>ATS Videos</h1>
				</div>
			</div>

			<div class="col-md-6">
				<div class="videos_list">
					<ul>
						<?php
                        $video=  PsPosts::getPost('video');
                        foreach ($video as $key => $eachvideo):
                            $content=  decodeDetails($eachvideo['post_content']);
                            $title=isset($content['title'])?$content['title']:'';
                            $des=  isset($content['description'])?$content['description']:'';
                            $link=isset($content['link'])?$content['link']:''; 
                        ?>
                        <li class="vedio"></i><?=$title?></li>   
                        <div class="hidden" >
                            <?=$link?>
                        </div>  
		                <?php endforeach; ?>
					</ul>
				</div>
			</div> 
			<div class="col-md-6">
				<div class="video_frame videos">
                    <iframe src="https://player.vimeo.com/video/150539331" width="640" height="320" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
			</div>
		</div>
	</div>
</section>


<!-- ********************************************* -->
<!-- ***************  Our Projects *************** -->
<!-- ********************************************* -->


<section class="our_projects">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">
				<div class="main_heading">
					<h1>Our Projects</h1>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="view_all">
					<a href="<?= Url::to(['site/projects'])?>">View All <i class="fa fa-angle-double-right"></i></a>
				</div>
			</div>
		</div>
			
		<div class="row">
			<?php  
				$projects = PsPosts::getPost('projects',8,'');
				foreach ($projects as $key => $proj):
					$id = $proj['id'];
					$title = $proj['post_title'];
					$cont_det = decodeDetails($proj['post_content']);
					$detail = limitedWords(strip_tags($cont_det['detail'],'<br>'),100);
					$proj_img = AppAttachments::getAllAttachments($id);
					$file_path = $proj_img[0]['file_path'];
				// }
				// dd($projects);
			?>
			<a href="<?= Url::to(['site/projects-detail','id'=>$id])?>">
				<div class="projects col-md-3 col-sm-6 col-xs-12 ">
			        <figure class="imghvr-zoom-in_news"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
			            <figcaption>
			              <h3><?= $title; ?></h3>
			              <p><?= (!empty($detail) ? $detail : 'Detail Will Be Uploaded Soon')?></p>
			            </figcaption>
			        </figure> 
		    	</div>
		    </a>
			<?php endforeach; ?>
		</div>
	</div>
</section>



<!-- ********************************************* -->
<!-- ***************  Our Clients *************** -->
<!-- ********************************************* -->

<section class="our_clients">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">
				<div class="main_heading">
					<h1>Our Clients</h1>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="view_all">
					<a href="<?= Url::to(['site/clients-testimonials'])?>">View All <i class="fa fa-angle-double-right"></i></a>
				</div>
			</div>
		</div>

		<div class="row">
			<?php  
			$clients = PsPosts::getPost('clients', 12,'');
			foreach ($clients as $key => $value):
				$id = $value['id'];
				$client_img = AppAttachments::getAllAttachments($id);
				$file_path = $client_img[0]['file_path'];
				?>
				<a href="">
					<div class="client col-md-2 col-sm-6 col-xs-12">
						<figure class="imghvr-zoom-in-clients"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
							<figcaption>
								<div class="border_inside"></div>
							</figcaption>
						</figure> 
					</div>
				</a>
			<?php endforeach ?>

		</div>
	</div>
</section>


<!-- ********************************************* -->
<!-- ***************  FAQs Section *************** -->
<!-- ********************************************* -->



<!-- Modal -->
<div id="ajax_model"></div>

<section class="faqs_title">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">
				<div class="main_heading">
					<h1>FAQs</h1>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="view_all">
					<a href="<?= Url::to(['site/faq']);?>">View All <i class="fa fa-angle-double-right"></i></a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="faqs_section">
	<div class="container">
		<div class="row">
			<?php
			$faq = PsPosts::getPost('faq',6,'');
			foreach ($faq as $key=> $faqs):
			$faq_id = $faqs['id'];
			$faqcontent=  decodeDetails($faqs['post_content']);

			$ques = isset($faqcontent['question'])?$faqcontent['question']:'';
			$ques = limitedWords($ques,40);
			$ans = isset($faqcontent['answer'])?$faqcontent['answer']:'';
			$ans = limitedWords(strip_tags($ans),100);

			?> 
			<a class="link_class" data-id="<?= $faq_id;?>">
				<div class="col-md-2 col-sm-4 col-xs-12">
					<div class="faq_box hvr-sweep-to-bottom">
						<p><?= $ques; ?></p>
						<div class="faq_img">
							<i class="fa fa-commenting"></i>
						</div>
						<div class="faq_text">
							<p><?= $ans;?></p>
							<span>Read More <i class="fa fa-angle-double-right"></i></span>
						</div>

					</div>
				</div>
			</a>
			<?php endforeach; ?>
		</div>
	</div>
</section>


<!-- ********************************************* -->
<!-- ***************  News & Events ************** -->
<!-- ********************************************* -->


<section class="news_events_home">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8">
				<div class="main_heading">
					<h1>Latest Company News</h1>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="view_all">
					<a href="">View All <i class="fa fa-angle-double-right"></i></a>
				</div>
			</div>
		</div>
		<div class="row">
			<?php  
				$news_events = PsPosts::getPostWithOrderLimit('news_events',3,'DESC');
				// dd($news_events); 
				foreach ($news_events as $key => $news):
				$nw_id = $news['id'];
				$nw_content = decodeDetails($news['post_content']);
				$nw_title = $nw_content['title'];
				$nw_detail = $nw_content['detail'];
				$nw_date_str = $news['event_date'];
				$nw_timestamp = strtotime($nw_date_str);
				$nw_date = date('d M Y', $nw_timestamp);
				$news_img = AppAttachments::getAllAttachments($nw_id);
				$file_path = $news_img[0]['file_path'];
				// dd($nw_date); 

			?>
			<div class="news_box">
				<a href="<?= url::to(['site/news-detail','id'=>$nw_id])?>">
					<div class="news col-md-4 col-sm-6 col-xs-12">
				        <figure class="imghvr-zoom-in_news"><img src="<?= (isset($file_path) ? $this->theme->baseUrl.'/'.$file_path : $this->theme->baseUrl.'/images/not-found.jpg')?>">
				            <figcaption>
				            	<i class="fa fa-search-plus"></i> 
				            </figcaption>
				      </figure>

				      <div class="news_content">
				      	<blockquote><?= $nw_title; ?></blockquote>
				      	<div class="news_date">
				      		<p><strong><?= $nw_date; ?></strong></p>
				      	</div>
				      	<p><?= $nw_detail; ?></p>
				      	<span>Read More <i class="fa fa-angle-double-right"></i></span>
				      </div> 
			    	</div>
			    </a>
			</div>
			<?php endforeach; ?>	
		</div>
	</div>
</section>


<!-- ********************************************* -->
<!-- ***************  Contact Home *************** -->
<!-- ********************************************* -->

<section class="contact_home">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 style="color: #fff; text-align: center; margin-bottom: 20px; font-weight: 300;">Contact Us</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 hidden-sm hidden-xs"></div>
			<div class="col-md-8">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="query">
                    	<div class="row">
                    		<?= $form->field($model, 'hidden1')->hiddenInput(['value'=>'home_contact'])->label(false); ?>
                    		<div class="col-md-6 col-sm-6 col-xs-12">
                    			<div class="input-with-icon">
                    				<div class="input-icon"><i class="fa fa-user"></i></div>
                    				<div class="form-group">
                    					<?= $form->field($model, 'name')->textInput(['placeholder' => 'Name'])->label(false); ?>
                    				</div>							
                    			</div>
                    		</div>
                    		<div class="col-md-6 col-sm-6 col-xs-12">
                    			<div class="input-with-icon">
                    				<div class="input-icon"><i class="fa fa-envelope"></i></div>
                    				<div class="form-group">
                    					<?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false); ?>
                    				</div>							
                    			</div>
                    		</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-6 col-sm-6 col-xs-12">
                    			<div class="input-with-icon">
                    				<div class="input-icon"><i class="fa fa-phone"></i></div>
                    				<div class="form-group">
                    					<?= $form->field($model, 'telephone')->textInput(['placeholder' => 'Contact No'])->label(false); ?>
                    				</div>							
                    			</div>
                    		</div>
                    		<div class="col-md-6 col-sm-6 col-xs-12">
                    			<div class="input-with-icon">
                    				<div class="input-icon"><i class="fa fa-tasks"></i></div>
                    				<div class="form-group">
                    					<?= $form->field($model, 'subject')->textInput(['placeholder' => 'Subject'])->label(false); ?>
                    				</div>							
                    			</div>
                    		</div>
                    	</div>

                    	<div class="row">
                    		<div class="col-md-12">
                    			<div class="input-with-icon">
                    				<div class="input-icon"><i class="fa fa-shopping-basket"></i></div>
                    				<div class="form-group">
                    					<?= $form->field($model, 'query')->textarea(['placeholder' => 'Query'])->label(false); ?>
                    				</div>							
                    			</div>

                    		</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-12">
                    			<?= Html::submitInput('Submit',['class'=>'form-control']) ?>
                    		</div>
                    	</div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
			<div class="col-md-2"></div>
		</div>
	</div>
</section>


<!-- ********************************************* -->
<!-- ***************  Testimonials *************** -->
<!-- ********************************************* -->


<section class="testimonials">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main_heading">
					<h1>Testimonials</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul id="testimonials">
					<?php  
						$testimonials = PsPosts::getPost('testimonial');
						foreach ($testimonials as $key => $test):
						$t_id = $test['id'];
						$t_content = decodeDetails($test['post_content']);
						$t_person_name = $t_content['title'];
						$t_testimonial = $t_content['detail']; 
					?>
					<li>
						<div class="test_single">
							<img src="images/projects/4.jpg">
							<h4><?= $t_person_name; ?></h4>
							<blockquote><?= $t_testimonial; ?></blockquote>
						</div>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</section>


<?php

	
	$url = Url::to(['site/ajax-call']);
    $script = <<<JS
        $('body').on('click', '.link_class', function(){
            id = $(this).attr('data-id');
			// alert(id);
            $.ajax({
                url:'{$url}',
                dataType:'html',
                data:{faq_id:id},
                type:'GET',
                success:function(data){
                    $('#ajax_model').html(data);
                    $("#myModal").modal('show');
                },
                error: function(){
                    console.log('Error Occur');
                }
            });
        });


        $(document).ready(function(){

		    var fvideo = $('.installation li').first().html();
		    if(fvideo!=undefined){
		        setTimeout(function(){
		            $('.installation li.vedio').first().trigger('click')
		        }, '3000');
		        console.log('defined');
		    }else{
		        console.log('undefined');
		    }

		    $('body').on('click', '.vedio', function(){
		        // alert('Hello');
		        // console.log('Video Frame');
		        var a = $(this).next('div.hidden').html();
		        console.log(a);
		        $('.videos').html('');
		        $('.videos').html(a);
		    });

		 
		});

JS;
    $this->registerJs($script,yii\web\View::POS_END);
?>

