<?php
namespace app\themes;

use yii\web\AssetBundle;

class AtsFrontendThemeAssets extends AssetBundle
{
	public $sourcePath = '@frontend/themes/ats2theme';
//	public $sourcePath = '@bower/backend-theme/assets';
	public $css = [
		'/css/bootstrap.css',
		'/css/bootstrap-responsive.css',
		'/font/stylesheet.css',
		'/css/prettyPhoto.css',
		'/css/jcarousel.css',
		'/css/style.css',
		'/color/default.css',
		'/bodybg/bg1.css',
	];
	
	public $js = [
		'/js/jquery.js',
		'/js/jquery.easing.1.3.js',
		'/js/bootstrap.js',
		'/js/jcarousel/jquery.jcarousel.min.js',
		'/js/prettyPhoto/jquery.prettyPhoto.js',
		'/js/google-code-prettify/prettify.js',
		'/js/hover/jquery-hover-effect.js',
		'/js/tweet/jquery.tweet.js',
		'/js/jquery.flexslider.js',
		'/js/jquery.nivo.slider.js',
		'/js/modernizr.custom.79639.js',
		'/js/jquery.ba-cond.min.js',
		'/js/jquery.slitslider.js',
		'/js/layerslider/layerslider.transitions.js',
		'/js/layerslider/layerslider.kreaturamedia.jquery.js',
		'/js/custom.js',
	];
	
}


?>	