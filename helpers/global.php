<?php
/**
 * This is the shortcut to DIRECTORY_SEPARATOR
 */
use backend\models\PsPosts;
use backend\models\UploadForm;
use yii\web\UploadedFile;
use yii\validators\FileValidator;
use backend\models\Members;
use backend\models\Cities;
use backend\models\Countries;
use yii\helpers\Url;
defined('DS') or define('DS',DIRECTORY_SEPARATOR);
 
/**
 * This is the shortcut to Yii::app()
 */
function app()
{
    return Yii::app();
}
 
/**
 * This is the shortcut to Yii::app()->clientScript
 */
function cs()
{
    // You could also call the client script instance via Yii::app()->clientScript
    // But this is faster
    return Yii::app()->getClientScript();
}
 
/**
 * This is the shortcut to Yii::app()->user.
 */
function user() 
{
    return Yii::app()->getUser();
}
 
/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route,$params=array(),$ampersand='&')
{
    return Yii::app()->createUrl($route,$params,$ampersand);
}
 
/**
 * This is the shortcut to CHtml::encode
 */
function h($text)
{
    return htmlspecialchars($text,ENT_QUOTES,Yii::app()->charset);
}
 
/**
 * This is the shortcut to CHtml::link()
 */
function l($text, $url = '#', $htmlOptions = array()) 
{
    return CHtml::link($text, $url, $htmlOptions);
}
/**
 * This is the shortcut to CHtml::checkbox()
 */
function checkbox($name, $checked=false, $htmlOptions = array()) 
{
	CHtml::$beforeRequiredLabel = '<span></span>';
	CHtml::$afterRequiredLabel = '';
	$checkBox = CHtml::checkBox($name,$checked,$htmlOptions);
	$tag = CHtml::tag('label',array(
						'for'=>$htmlOptions['id'],
						), 
						'<span></span>'
					);
	return array('checkbox'=>$checkBox,'tag'=>$tag);
}
 /**
 * This is the shortcut to CHtml::radiobutton()
 */
function radiobutton($name, $checked=false, $htmlOptions = array()) 
{
	CHtml::$beforeRequiredLabel = '<span></span>';
	CHtml::$afterRequiredLabel = '';
	$radiobutton = CHtml::radioButton($name,$checked,$htmlOptions);
	$tag = CHtml::tag('label',array(
						'for'=>$htmlOptions['id'],
						), 
						'<span></span>'
					);
	return array('radiobutton'=>$radiobutton,'tag'=>$tag);
}
 
/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t($message, $category = 'stay', $params = array(), $source = null, $language = null) 
{
    return Yii::t($category, $message, $params, $source, $language);
}
 
/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url=null) 
{
    static $baseUrl;
    if ($baseUrl===null)
        $baseUrl=Yii::app()->getRequest()->getBaseUrl();
    return $url===null ? $baseUrl : $baseUrl.'/'.ltrim($url,'/');
}
/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name, $default = null) 
{
	if(Yii::$app->params[$name] != NULL)
		return Yii::$app->params[$name];
	else
		return $default;
}
/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function have_branches() 
{
	$settings = param('settings');
	if($settings['branches']==FALSE)
		return FALSE;
	return TRUE;
}
/**
 * This is the shortcut to Controller::pre_r function
 * If the parameter is given, it will print the array in pre tags
 */
function pre($data) 
{
   echo "<pre>";
       print_r($data);
    echo "</pre>";
  //  return Controller::pre_r($data);
}
function dd($data) 
{
   echo "<pre>";
       print_r($data);
    echo "</pre>";
    die('==============================');
}
/**
 * This is the shortcut to Controller::normaldate function
 * If the parameter is given, it will convert the mysql date to d/m/y format
 */
function normaldate($dateString) 
{
    return Controller::normaldate($dateString);
}
function mysqldate($dateString){
	if($dateString!==null){
		$parts = explode('/', $dateString);
		$Y = $parts[2];
		$m = $parts[1];
		$d = $parts[0];
		return ( $Y. '-' . $m . '-' . $d);
	}else{
		return null;
	}
		
	}
/**
 * This will return the next key in case of an associateive Array
 */
function getNextItem($array, $key)
{
    $keys = array_keys($array);
    //2011-07-29: if $key was found, make sure it isn't the last item in the array
    if ( (false !== ($p = array_search($key, $keys))) && ($p < count($keys) - 1) )
    {
        return array('key' => $keys[++$p], 'value' => $array[$keys[$p]]);
    }
    else
    {
        return false;
    }
}
function setMessage($class,$message){
	$session = Yii::$app->session;
	echo $session->setFlash($class, $message);
	
}
function setError($message){
	setMessage('error',$message);
}
function setSuccess($message){
	setMessage('confirm',$message);
}
function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}
function convertDate($date, $format = 'Y-m-d H:i:s',$validateTime=FALSE)
{
    $d = strtotime($date);
	
	if($validateTime==TRUE){
		if(validateDate($date,'h:i A')==FALSE){
			return $date;
		}
	}
	return date($format, $d); 
}
function previous_date($refDate){
	
	$date1 = strtotime($refDate);
	$date2 = strtotime(date('Y-m-d'));
	if ($date1 < $date2)
	{
	   return true;
	}
	return FALSE;
}
//decodeDetails function return array
function decodeDetails($attribute){
	return json_decode($attribute,true);
}
function getCategoryTitle($id=null){
	$model = PsPosts::findOne($id);
	// pre($model);die();
	return (isset($model) && $model!=null)?str_replace(' ', '_',$model['post_title']):'-'; 
}
function getCityName($id=null){
	$model = Cities::findOne($id);
	return (isset($model) && $model!=null) ? $model['city_name'] : '-'; 
}
function getCountryName($id=null){
	$model = Countries::findOne($id);
	return (isset($model) && $model!=null) ? $model['country_name'] : '-'; 
}
//return attribute
function decodePost($model,$attribute){
    
	return json_decode($model->post_content)->$attribute;
}
function savePosts($model,$content,$title, $type){
	$model->post_title = $title;
	$model->post_type = $type;
	// $model->post_parent = $post_parent;
	$model->post_content = json_encode($content);
        $model->recordstatus=1;
 //        pre($_REQUEST);
	// pre($model->attributes);
	// die();
	$model->save(FALSE);
	
	return $model;
}
// function saveDirectory($membership_no , $member_name , $status , $country){
// 		// $model = new Members;
//         // pre($member_name);
//         // die('trigger');
//         $model->membershipnumber = $membership_no;
//         $model->membershipname = $member_name;
//         $model->status = $status;
//         $model->country = $country;
//         $model->save(false);
//     }
 
function pCategoryList($type){
	$model = PsPosts::find('post_type,recordstatus')->where('post_type=:ptype AND recordstatus=:rec',[':ptype'=>$type, ':rec' => 1])->all();
	
	if($model != NULL){
		foreach($model as $key=>$val)
		{
			$id = $val->id;
			$val = $val->post_title;
			
			$cat[$id] = $val;
		}
	}
	return $cat;
}
function singleUploadFile($name){
	$upload = new UploadForm();
	if(isset($_REQUEST['id'])){
			//$upload = $upload::findOne($_REQUEST['id']);
	}
    if (Yii::$app->request->isPost) {
        $upload->file = UploadedFile::getInstance($upload, $name);
	
        if ($upload->file && $upload->validate()) {  	     
        		 $upload->file->saveAs('uploads/'.$upload->file->baseName . '.' . $upload->file->extension);
				 
        }
    }
	return $upload;	
}
function limitedWords($string,$limit=20){
		 if (strlen($string) > $limit)
	      $string = substr($string, 0, strrpos(substr($string, 0, $limit), ' ')) . '...';
	      
	     return $string;
	}
?>