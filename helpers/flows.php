<?php
class Flows{
	/**
	 * Pending flows
	 * @param string $code Code that is defined in _hr_codes.inc
	 * @return array 
	 */
	public function pendingFlow($code)
	{
	    return WorkflowProcess::pending(param($code));
	}
}

?>